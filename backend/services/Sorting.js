const { camelCase, upperFirst } = require('lodash');
const models = require('../models');
const sequelize = require('sequelize');

class Sorting {
    static handle(sortBy, sortOrder, mode = 'association') {
        const order = [];
        const sortItems = sortBy.trim().split(' ');

        for (const sortItem of sortItems) {
            let orderItem;

            if (sortItem.startsWith('$') && sortItem.endsWith('$')) {
                const item = sortItem.replace('$', '').replace('$', '');

                orderItem = [sequelize.literal('`' + item + '`'), sortOrder];
            } else {
                if (sortItem.includes('.')) {
                    const tempItems = sortItem.split('.');
                    const column = tempItems.pop();
                    const associations = tempItems;

                    orderItem = [column, sortOrder];

                    if (mode === 'association') {
                        orderItem.unshift({
                            association: associations.join('.')
                        });
                    } else {
                        for (const association of associations.reverse()) {
                            const model = upperFirst(camelCase(association));

                            orderItem.unshift({
                                model: models[model],
                                as: association
                            });
                        }
                    }
                } else {
                    orderItem = [sortItem, sortOrder];
                }
            }

            order.push(orderItem);
        }

        return order;
    }
}

module.exports = Sorting;

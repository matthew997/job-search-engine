const PhoneService = require('./Phone');
const { Op } = require('sequelize');
const { User, Role } = require('../models');
const config = require('../config');
const UserRepository = require('../repositories/User');
const RoleRepository = require('../repositories/Role');

class UserService {
    static async login(phone, pin) {
        const user = await UserRepository.findOne({
            where: {
                phone,
                pin
            },
            include: [
                {
                    association: 'registration'
                }
            ]
        });

        return user;
    }

    static async getMe(id, role) {
        const include = [
            {
                association: 'roles'
            },
            {
                association: 'image'
            },
            {
                association: 'registration'
            }
        ];

        if (role) {
            include.push({ association: role });
        }

        const user = await UserRepository.findOne({
            where: {
                id
            },
            include,
            attributes: User.DISPLAYABLE_FIELDS,
            subQuery: false
        });

        if (!user) {
            return null;
        }

        return user;
    }

    static async findByPhone(phone, origin = null) {
        let roles = [];

        if (origin === 'cms') {
            roles = [Role.ADMIN];
        } else {
            roles = [Role.USER];
        }

        const user = await UserRepository.findOne({
            where: {
                phone,
                ['$roleUser.role.name$']: {
                    [Op.in]: roles
                }
            },
            include: [
                {
                    association: 'roleUser',
                    include: [
                        {
                            association: 'role'
                        }
                    ]
                }
            ],
            subQuery: false
        });

        return user;
    }

    static async sendPin(phone) {
        const pin = config.twilio.useFixedPin
            ? config.twilio.defaultPin
            : PhoneService.generatePin();

        await UserRepository.update(
            {
                pin,
                pin_generated_at: new Date()
            },
            {
                where: {
                    phone
                }
            }
        );

        if (phone.charAt(0) === '0') {
            const phoneNumber = phone.replace(phone.charAt(0), '972');

            return PhoneService.sendPin(phoneNumber, pin);
        }

        const phoneNumber = `+${phone}`;

        return PhoneService.sendPin(phoneNumber, pin);
    }

    static async generateNumericId(transaction) {
        const count = await this.count(
            {},
            {
                transaction,
                lock: true
            }
        );

        return count + 1;
    }
}

module.exports = UserService;

const MediaRepository = require('../repositories/Media');
const StoreMediaHandler = require('./StoreMediaHandler');

class StoreMediaFromFrontendHandler {

    static async handle(file, id) {
        const type = this.getMediaType(file);

        if (type) {
            const {
                filename,
                extension,
                name,
                date,
                meta = {}
            } = await StoreMediaHandler.handle(file, type);

            const storedMedia = await MediaRepository.create({
                name,
                alt: name,
                filename,
                extension,
                type,
                size: file.size,
                date,
                user_id: id,
                meta
            });

            return storedMedia;
        }

        return false;
    }

    static getMediaType(file) {
        if (!file.mimetype) {
            return null;
        }

        const [type] = file.mimetype.split('/');
        const allowedTypes = ['image', 'video'];

        if (allowedTypes.includes(type)) {
            return type;
        }

        return null;
    }
}

module.exports = StoreMediaFromFrontendHandler;

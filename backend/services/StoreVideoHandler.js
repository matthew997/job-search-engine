const fs = require('fs');
const crypto = require('crypto');

class StoreVideoHandler {
    static async handle(video) {
        try {
            const buffer = crypto.randomBytes(12);
            const hex = buffer.toString('hex');
            const extension = video.name.split('.').pop();

            const filename = `${hex}.${extension}`;
            const directory = `public/media/videos/users`;

            this.createDirectory(directory);

            const path = `${directory}/${filename}`;

            await video.mv(path);

            return filename;
        } catch (error) {
            console.error(error);
            return null;
        }
    }

    static createDirectory(directory) {
        try {
            fs.statSync(directory);
        } catch (e) {
            fs.mkdirSync(directory, { recursive: true });
        }
    }
}

module.exports = StoreVideoHandler;

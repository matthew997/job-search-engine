const BaseModelRepository = require('../repositories/BaseModelRepository');
const { Role } = require('../models');

class RoleService extends BaseModelRepository {
    static get model() {
        return Role;
    }
}

module.exports = RoleService;

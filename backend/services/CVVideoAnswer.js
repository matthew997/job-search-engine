const path = require('path');
const fs = require('fs');

const CVAnswerRepository = require('../repositories/CVVideoAnswer');

class CVVideoAnswerService {
    static get VIDEO_SAVE_PATH() {
        return './../public/cv-videos';
    }

    static getVideoPath(filename) {
        return path.join(__dirname, `${this.VIDEO_SAVE_PATH}/${filename}`);
    }

    static readVideoFile(path, encoding) {
        return new Promise((resolve, reject) => {
            fs.readFile(path, encoding, function (err, data) {
                if (err) {
                    reject(err);
                }

                resolve(data);
            });
        });
    }

    static getVideoFile(answer) {
        const videoPath = this.getVideoPath(answer.video);

        return this.readVideoFile(videoPath, 'base64');
    }

    static async storeUploadedVideo(answer) {
        try {
            const { video } = answer;
            const extension = video.name.split('.').pop();
            const filename = `cv_video_user_${answer.user_id}_question_${answer.question_id}.${extension}`;
            const directory = 'public/cv-videos';

            this.createDirectory(directory);

            const path = `${directory}/${filename}`;

            await video.mv(path);

            return filename;
        } catch (error) {
            console.error(error);
            return null;
        }
    }

    static createDirectory(directory) {
        try {
            fs.statSync(directory);
        } catch (e) {
            fs.mkdirSync(directory, { recursive: true });
        }
    }

    static writeVideo(answer) {
        const { video } = answer;
        const videoPath = this.createVideoPath(answer);
        const filename = videoPath.split('/').pop();
        const buffer = Buffer.from(video.blob, 'base64');

        fs.writeFile(videoPath, buffer, () => console.log('Video Saved'));

        return filename;
    }

    static async deleteAllVideosAndAnswersForUser(userId) {
        const answers = await CVAnswerRepository.findAllForUser(userId);
        const promises = [];

        answers.forEach(answer => {
            const promise = this.deleteVideoAndAnswer(answer);

            promises.push(promise);
        });

        return Promise.all(promises);
    }

    static async deleteVideoAndAnswer(answer) {
        const videoPath = this.getVideoPath(answer.video);

        fs.unlink(videoPath, err => {
            if (err) {
                throw err;
            }

            console.log(`${answer.video} has been deleted`);
        });

        await answer.destroy();
    }

    static createVideoPath(answer) {
        const filename = `cv_video_user_${answer.user_id}_question_${answer.question_id}.webm`;
        const videoPath = path.join(
            __dirname,
            `${this.VIDEO_SAVE_PATH}/${filename}`
        );

        return videoPath;
    }

    static async joinVideos(answers) {
        let joinedVideos;

        for (const answer of answers) {
            const blob = await this.getVideoFile(answer);

            joinedVideos += blob;
        }

        return joinedVideos;
    }
}

module.exports = CVVideoAnswerService;

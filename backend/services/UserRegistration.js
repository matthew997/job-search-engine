const UserRegistrationRepository = require('../repositories/UserRegistration');
const UserRepository = require('../repositories/User');

class UserRegistrationService {
    static async updateStepByUserId(userId, step, value) {
        const userRegistration = await UserRegistrationRepository.findOneByUserId(
            userId
        );

        if (!userRegistration) {
            return null;
        }

        await userRegistration.update({
            [step]: value
        });

        if (userRegistration.hasCompletedAllSteps()) {
            const user = await userRegistration.getUser();

            await user.update({
                registration_completed: true
            });
        }

        return userRegistration;
    }

    static async updateCompanyAcceptedTerms(userId) {
        const userRegistration = await UserRegistrationRepository.findOneByUserId(
            userId
        );

        if (!userRegistration) {
            return null;
        }

        await userRegistration.update({
            company_accept_terms: true
        });

        return userRegistration;
    }
}

module.exports = UserRegistrationService;

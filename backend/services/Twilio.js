const Twilio = require('twilio');
const config = require('../config');

const twilio = new Twilio(config.twilio.sid, config.twilio.token);

class TwilioService {
    static async sendMessage(to, body, from = null) {
        try {
            await twilio.messages.create({
                from: from || config.twilio.from,
                to,
                body
            });
        } catch (error) {
            return error.code;
        }

        return 0;
    }
}

TwilioService.errorCodeToMessage = {
    21211: 'This phone number is invalid.',
    21612: 'Twilio cannot route to this number.',
    21408: "Your account doesn't have the international permissions necessary to SMS this number.",
    21610: 'This number is blacklisted for your account.',
    21614: 'This number is incapable of receiving SMS messages.'
};

TwilioService.ERROR_RECEIVER_HAS_INVALID_PHONE_NUMBER = 21211;
TwilioService.ERROR_NO_INTERNATIONAL_PERMISSIONS = 21408;
TwilioService.ERROR_RECEIVER_IS_BLACKLISTED = 21610;
TwilioService.ERROR_RECEIVER_HAS_UNROUTABLE_NUMBER = 21612;
TwilioService.ERROR_RECEIVER_CANNOT_RECEIVE_AN_SMS = 21614;

TwilioService.FROM_NUMBER_VALID = '+15005550006';

TwilioService.TEST_RECIPIENT_OK = '+15005550022';
TwilioService.TEST_RECIPIENT_INVALID = '+15005550001';
TwilioService.TEST_RECIPIENT_UNROUTABLE = '+15005550002';
TwilioService.TEST_RECIPIENT_NO_PERMISSION = '+15005550003';
TwilioService.TEST_RECIPIENT_BLACKLISTED = '+15005550004';
TwilioService.TEST_RECIPIENT_CANNOT_RECEIVE_AN_SMS = '+15005550009';

module.exports = TwilioService;

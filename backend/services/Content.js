class ContentService {
    static validate(mainKey, value) {
        let rules = [];
        let errors = [];
        const imageObjectFields = [
            { key: 'id', type: 'string' },
            { key: 'date', type: 'string' },
            { key: 'filename', type: 'string' },
            { key: 'extension', type: 'string' },
            { key: 'alt', type: 'string' },
            {
                key: 'meta',
                type: 'object',
                subKeys: [
                    {
                        key: 'width',
                        type: 'number'
                    },
                    {
                        key: 'height',
                        type: 'number'
                    },
                    {
                        key: 'breakpoints',
                        type: 'object',
                        array: true
                    }
                ]
            },
            { key: 'size', type: 'string|number' },
            { key: 'name', type: 'string' },
            { key: 'type', type: 'string' }
        ];

        const meta = {
            key: 'meta',
            type: 'object',
            subKeys: [
                { key: 'title', type: 'string' },
                { key: 'description', type: 'string' }
            ]
        };

        switch (mainKey) {
            case 'header-component':
                rules = [
                    {
                        key: 'logo',
                        type: 'object',
                        subKeys: imageObjectFields
                    },
                    {
                        key: 'menuItems',
                        type: 'object',
                        array: true
                    },
                    { key: 'phoneNumber', type: 'string' }
                ];
                break;
            case 'customer-sign-page':
                rules = [
                    {
                        key: 'signInTab',
                        type: 'object',
                        subKeys: [
                            {
                                key: 'step1',
                                type: 'object',
                                array: true,
                                subkeys: [
                                    { key: 'contentText', type: 'string' },
                                    {
                                        key: 'termsAndConditionsText',
                                        type: 'string'
                                    },
                                    { key: 'errorText', type: 'string' },
                                    { key: 'submitButtonText', type: 'string' },
                                    {
                                        key: 'phoneNumberAttemptInfo',
                                        type: 'object',
                                        subkeys: [
                                            { key: 'text', type: 'string' },
                                            { key: 'ctaText', type: 'string' },
                                            { key: 'ctaLink', type: 'string' }
                                        ]
                                    }
                                ]
                            },
                            {
                                key: 'step2',
                                type: 'object',
                                array: true,
                                subkeys: [
                                    { key: 'contentText', type: 'string' },
                                    { key: 'explanationText', type: 'string' },
                                    { key: 'resendMessage', type: 'string' },
                                    { key: 'errorText', type: 'string' },
                                    { key: 'submitButtonText', type: 'string' }
                                ]
                            }
                        ]
                    },
                    {
                        key: 'signUpTab',
                        type: 'object',
                        subKeys: [
                            { key: 'contentText', type: 'string' },
                            { key: 'buttonText', type: 'string' }
                        ]
                    },
                    meta
                ];
                break;
            case 'signup-page':
                rules = [
                    { key: 'headline', type: 'string' },
                    {
                        key: 'personalData',
                        type: 'object',
                        subKeys: [{ key: 'termsConditions', type: 'string' }]
                    },
                    {
                        key: 'authentication',
                        type: 'object',
                        subKeys: [
                            { key: 'headline', type: 'string' },
                            { key: 'sendAgainText', type: 'string' },
                            { key: 'sendAgainLink', type: 'string' }
                        ]
                    },
                    {
                        key: 'goldUser',
                        type: 'object',
                        subKeys: [
                            { key: 'text', type: 'string' },
                            {
                                key: 'icon',
                                type: 'object',
                                subKeys: imageObjectFields
                            }
                        ]
                    },
                    meta
                ];
                break;
            case 'footer-component':
                rules = [
                    { key: 'sublogoText', type: 'string' },
                    { key: 'phoneNumber', type: 'string' },
                    { key: 'terms', type: 'string' },
                    { key: 'newsletter', type: 'string' },
                    {
                        key: 'links',
                        type: 'object',
                        subKeys: [
                            {
                                key: 'socialmedia',
                                type: 'object',
                                subKeys: [
                                    { key: 'first', type: 'string' },
                                    { key: 'second', type: 'string' },
                                    { key: 'third', type: 'string' }
                                ]
                            },
                            {
                                key: 'colFirst',
                                type: 'object',
                                array: true
                            },
                            {
                                key: 'colSecond',
                                type: 'object',
                                array: true
                            }
                        ]
                    },
                    {
                        key: 'logo',
                        type: 'object',
                        subKeys: imageObjectFields
                    },
                    {
                        key: 'socialmedia',
                        type: 'object',
                        subKeys: [
                            {
                                key: 'logoFirst',
                                type: 'object',
                                subKeys: imageObjectFields
                            },
                            {
                                key: 'logoSecond',
                                type: 'object',
                                subKeys: imageObjectFields
                            },
                            {
                                key: 'logoThird',
                                type: 'object',
                                subKeys: imageObjectFields
                            }
                        ]
                    }
                ];
                break;
            case 'home-page':
                rules = [
                    {
                        key: 'mainBanner',
                        type: 'object',
                        subKeys: [
                            {
                                key: 'image',
                                type: 'object',
                                subKeys: imageObjectFields
                            },
                            { key: 'headline', type: 'string' },
                            { key: 'subheadline', type: 'string' },
                            {
                                key: 'button',
                                type: 'object',
                                subKeys: [
                                    { key: 'text', type: 'string' },
                                    { key: 'link', type: 'string' }
                                ]
                            },
                            {
                                key: 'videoButton',
                                type: 'object',
                                subKeys: [
                                    { key: 'text', type: 'string' },
                                    { key: 'link', type: 'string' }
                                ]
                            }
                        ]
                    },
                    {
                        key: 'aboutProcess',
                        type: 'object',
                        subKeys: [
                            { key: 'headline', type: 'string' },
                            { key: 'subheadline', type: 'string' },
                            {
                                key: 'button',
                                type: 'object',
                                subKeys: [
                                    { key: 'text', type: 'string' },
                                    { key: 'link', type: 'string' }
                                ]
                            },
                            {
                                key: 'items',
                                type: 'object',
                                array: true
                            }
                        ]
                    },
                    {
                        key: 'hotJobs',
                        type: 'object',
                        subKeys: [
                            { key: 'headline', type: 'string' },
                            { key: 'noJobsText', type: 'string' }
                        ]
                    },
                    {
                        key: 'whoWeAre',
                        type: 'object',
                        subKeys: [
                            { key: 'headline', type: 'string' },
                            { key: 'text', type: 'string' },
                            {
                                key: 'video',
                                type: 'object',
                                subKeys: [{ key: 'source', type: 'string' }]
                            }
                        ]
                    },
                    {
                        key: 'customerReviews',
                        type: 'object',
                        subKeys: [
                            { key: 'headline', type: 'string' },
                            {
                                key: 'reviews',
                                type: 'object',
                                array: true
                            }
                        ]
                    },
                    meta
                ];
                break;
            case 'about-us-page':
                rules = [
                    { key: 'headline', type: 'string' },
                    { key: 'columnLeft', type: 'string' },
                    { key: 'columnRight', type: 'string' },
                    {
                        key: 'banner',
                        type: 'object',
                        subKeys: [
                            { key: 'text', type: 'string' },
                            { key: 'textBold', type: 'string' },
                            {
                                key: 'image',
                                type: 'object',
                                subKeys: imageObjectFields
                            }
                        ]
                    },
                    {
                        key: 'promotionalImages',
                        type: 'object',
                        array: true,
                        subKeys: imageObjectFields
                    },
                    meta
                ];
                break;
            case 'contact-us-page':
                rules = [{ key: 'subtitle', type: 'string' }, meta];
                break;
            case 'video-cv-done-page':
                rules = [
                    { key: 'title', type: 'string' },
                    { key: 'heading', type: 'string' },
                    { key: 'description', type: 'string' },
                    meta
                ];
                break;
            case 'business-registration-finished-page':
                rules = [
                    {
                        key: 'icon',
                        type: 'object',
                        subKeys: imageObjectFields
                    },
                    { key: 'title', type: 'string' },
                    { key: 'heading', type: 'string' },
                    { key: 'description', type: 'string' },
                    meta
                ];
                break;
            case 'business-registration-page':
                rules = [
                    {
                        key: 'personalInformation',
                        type: 'object',
                        array: true,
                        subkeys: [
                            {
                                key: 'informationText',
                                type: 'string'
                            }
                        ]
                    },
                    {
                        key: 'smsVerification',
                        type: 'object',
                        array: true,
                        subkeys: [
                            {
                                key: 'text',
                                type: 'string'
                            }
                        ]
                    },
                    meta
                ];
                break;
            case 'edit-company-leads-filter-page':
                rules = [{ key: 'text', type: 'string' }, meta];
                break;
            case 'personal-zone-page':
                rules = [
                    {
                        key: 'upperText',
                        type: 'string'
                    },
                    {
                        key: 'videoTabUpperText',
                        type: 'string'
                    },
                    meta
                ];
                break;
            case 'applications-page':
                rules = [
                    {
                        key: 'text',
                        type: 'string'
                    },
                    {
                        key: 'videoLink',
                        type: 'string'
                    },
                    meta
                ];
                break;
            case 'payment-page':
                rules = [
                    {
                        key: 'headline',
                        type: 'string'
                    },
                    {
                        key: 'text',
                        type: 'string'
                    },
                    {
                        key: 'monthlyPrice',
                        type: 'number'
                    },
                    {
                        key: 'pricePerLead',
                        type: 'number'
                    },
                    meta
                ];
                break;
            case 'privacy-policy-page':
                rules = [
                    {
                        key: 'title',
                        type: 'string'
                    },
                    {
                        key: 'content',
                        type: 'string'
                    },
                    meta
                ];
                break;
            case 'terms-of-use-page':
                rules = [
                    {
                        key: 'title',
                        type: 'string'
                    },
                    {
                        key: 'content',
                        type: 'string'
                    },
                    meta
                ];
                break;
            default:
                return ['Invalid key.'];
        }

        for (let k in value) {
            const index = rules.findIndex(rule => rule.key === k);

            if (index === -1) {
                errors.push('Not recognized key of value: ' + k);

                continue;
            }

            const propertyErrors = ContentService.validateProperty(
                k,
                value[k],
                rules[index]
            );

            errors = [...errors, ...propertyErrors];
        }

        rules.forEach(rule => {
            if (value[rule.key] === undefined) {
                errors.push('Missing key of value: ' + rule.key);
            }
        });

        return errors;
    }

    static validateProperty(propertyName, value, rule) {
        let errors = [];
        const availableTypes = rule.type.split('|');

        if (!availableTypes.includes(typeof value)) {
            errors.push(
                `Wrong type key of value: ${propertyName}. Should be: ${rule.type}`
            );
        }

        if (rule.required && !value) {
            errors.push('Key of value can not be empty: ' + propertyName);
        }

        if (rule.max && value.length > rule.max) {
            errors.push(
                `Key of value can not contain more than ${rule.max} chars: ${propertyName}`
            );
        }

        if (rule.type === 'object' && !rule.array) {
            for (let k in value) {
                const index = rule.subKeys.findIndex(rule => rule.key === k);

                if (index === -1) {
                    errors.push(
                        `Not recognized key of value: ${k} in ${propertyName}`
                    );

                    continue;
                }

                if (rule.required && !value) {
                    errors.push(
                        `Key of value can not be empty: ${k} in ${propertyName}`
                    );
                }

                const propertyErrors = ContentService.validateProperty(
                    k,
                    value[k],
                    rule.subKeys[index]
                );

                errors = [...errors, ...propertyErrors];
            }
        }

        return errors;
    }
}

module.exports = ContentService;

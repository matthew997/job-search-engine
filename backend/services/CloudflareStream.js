const fs = require('fs');
const axios = require('axios');
const tus = require('tus-js-client');

class CloudflareStream {
    constructor({ accountId, apiKey, email }) {
        this.accountId = accountId;
        this.apiKey = apiKey;
        this.email = email;

        this.authHeaders = {
            'X-Auth-Email': this.email,
            'X-Auth-Key': this.apiKey
        };

        this.API_URL = `https://api.cloudflare.com/client/v4/accounts/${this.accountId}/stream`;
    }

    upload(path) {
        return new Promise((resolve, reject) => {
            const file = fs.createReadStream(path);
            const size = fs.statSync(path).size;
            const upload = new tus.Upload(file, {
                endpoint: this.API_URL,
                headers: {
                    ...this.authHeaders
                },
                chunkSize: 5 * 1024 * 1024,
                uploadSize: size,
                onError(error) {
                    reject(error);
                },
                onSuccess() {
                    const mediaId = upload.url.split('/').pop();

                    resolve(mediaId);
                }
            });

            upload.start();
        });
    }

    delete(id) {
        return axios.delete(`${this.API_URL}/${id}`, {
            headers: {
                ...this.authHeaders
            }
        });
    }
}

module.exports = CloudflareStream;

class IsSlugProhibitedHandler {
    static handle(slug) {
        const prohibitedPrefixes = [
            'about',
            'applications',
            'company',
            'contact',
            'filters',
            'jobs',
            'login',
            'payment',
            'personal-zone',
            'preview',
            'recruiter-sign-up',
            'recruiter-zone',
            'signup',
            'videocv'
        ];

        for (const prefix of prohibitedPrefixes) {
            if (slug.startsWith(prefix)) {
                return true;
            }
        }

        return false;
    }
}

module.exports = IsSlugProhibitedHandler;

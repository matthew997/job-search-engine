const atob = require('atob');
const btoa = require('btoa');

class Tranzila {
    constructor(client) {
        this.client = client;
    }

    async getTransactionByIndex(index) {
        return this.client.getTransactionByIndex(index);
    }

    async getTransactionsByDate(from, to) {
        return this.client.getTransactionsByDate(from, to);
    }

    async verifyTransaction(transactionData) {
        const { index } = transactionData;

        try {
            const transaction = await this.client.getTransactionByIndex(index);

            if (transaction.response !== this.client.RESPONSE_PAID) {
                return false;
            }

            transaction.returnData = this.decodeReturnData(transaction.user7);

            return transaction;
        } catch (error) {
            console.error(error);
        }
    }

    async getSubscriptionPaymentUrl(user, sum) {
        const { id: userId, email } = user;

        const returnData = {
            user_id: userId,
            save_card: true,
            type: 'subscription-initial-payment'
        };

        const token = await this.client.getHandshakeToken(sum, {
            pdesc: this.encodeReturnData(returnData),
            email
        });

        const options = {
            sum,
            currency: this.client.CURRENCY_NIS,
            cred_type: '1',
            tranmode: 'AK',
            hidesum: '1',
            thtk: token,
            email
        };

        return this.client.getIframeUrl(options);
    }

    async getAddCardPaymentUrl(userId) {
        const returnData = {
            user_id: userId,
            save_card: true
        };

        const token = await this.client.getHandshakeToken(0.01, {
            pdesc: this.encodeReturnData(returnData)
        });

        const options = {
            sum: 0.01,
            currency: this.client.CURRENCY_NIS,
            cred_type: '1',
            tranmode: 'AK',
            hidesum: '1',
            thtk: token
        };

        return this.client.getIframeUrl(options);
    }

    pay(amount, card, options) {
        return this.client.payWithSavedCard(amount, card, options);
    }

    encodeReturnData(data) {
        return btoa(JSON.stringify(data));
    }

    decodeReturnData(data) {
        return JSON.parse(atob(data));
    }

    getCardBrand(type, issuer) {
        let brand = null;

        const types = {
            1: 'mastercard',
            2: 'visa'
        };

        const issuers = {
            3: 'dinersclub',
            4: 'americanexpress'
        };

        if (types[type]) {
            brand = types[type];
        }

        if (issuers[issuer]) {
            brand = issuers[issuer];
        }

        return brand;
    }
}

module.exports = Tranzila;

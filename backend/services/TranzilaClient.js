const qs = require('qs');
const axios = require('axios');

class TranzilaClient {
    constructor({ terminal, password, transactionPassword }) {
        this.terminal = terminal;
        this.password = password;
        this.transactionPassword = transactionPassword;

        this.HANDSHAKE_URL =
            'https://secure5.tranzila.com/cgi-bin/tranzila71dt.cgi';
        this.TRANSACTION_URL =
            'https://secure5.tranzila.com/cgi-bin/tranzila71u.cgi';
        this.GET_TRANSACTION_BY_INDEX_URL =
            'https://secure5.tranzila.com/cgi-bin/billing/tranzila_dates.cgi';
        this.GET_TRANSACTIONS_BY_DATE_URL =
            'https://secure5.tranzila.com/cgi-bin/billing/tranzila_dates.cgi';

        this.RESPONSE_PAID = '000';

        this.CURRENCY_NIS = 1;
        this.CURRENCY_USD = 2;
        this.CURRENCY_GBP = 826;
        this.CURRENCY_EUR = 978;
    }

    get IFRAME_URL() {
        return `https://direct.tranzila.com/${this.terminal}/iframenew.php`;
    }

    async getTransactionsByDate(from, to) {
        const options = {
            Fdate: from,
            Tdate: to,
            terminal: this.terminal,
            passw: this.password
        };

        try {
            const { data } = await axios.get(
                `${this.GET_TRANSACTIONS_BY_DATE_URL}?${qs.stringify(options)}`
            );

            const transactions = data
                .split('\n')
                .filter(transaction => transaction !== '')
                .map(transaction => this.parseTransaction(transaction));

            return transactions;
        } catch (error) {
            console.error(error);

            throw error;
        }
    }

    async getTransactionByIndex(index) {
        const options = {
            index,
            terminal: this.terminal,
            passw: this.password
        };

        try {
            const { data } = await axios.get(
                `${this.GET_TRANSACTION_BY_INDEX_URL}?${qs.stringify(options)}`
            );

            const transaction = this.parseTransaction(data);

            return transaction;
        } catch (error) {
            console.error(error);

            throw error;
        }
    }

    async getHandshakeToken(amount, overrideOptions) {
        const options = {
            op: '1',
            sum: amount,
            supplier: this.terminal,
            TranzilaPW: this.transactionPassword,
            currency: this.CURRENCY_NIS,
            ...overrideOptions
        };

        const { data } = await axios.get(
            `${this.HANDSHAKE_URL}?${qs.stringify(options)}`
        );

        const token = data.replace('thtk=', '');

        return token;
    }

    async payWithSavedCard(amount, card, overrideOptions) {
        const options = {
            sum: amount,
            expdate: card.exp.replace('/', ''),
            TranzilaTK: card.token,
            supplier: this.terminal,
            TranzilaPW: this.transactionPassword,
            currency: this.CURRENCY_NIS,
            cred_type: '1',
            ...overrideOptions
        };

        try {
            const { data } = await axios.get(
                `${this.TRANSACTION_URL}?${qs.stringify(options)}`
            );

            const transaction = this.parseTransaction(data);

            return transaction.Response === this.RESPONSE_PAID;
        } catch (error) {
            console.error(error);
            return false;
        }
    }

    parseTransaction(data) {
        const transaction = {};

        data.split('&').forEach(row => {
            const [key, value] = row.split('=');

            transaction[key] = value.replace('\n', '');
        });

        return transaction;
    }

    getIframeUrl(options) {
        return `${this.IFRAME_URL}?${qs.stringify(options)}`;
    }
}

module.exports = TranzilaClient;

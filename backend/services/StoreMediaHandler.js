const _ = require('lodash');
const crypto = require('crypto');
const fs = require('fs');
const moment = require('moment');
const sharp = require('sharp');

class StoreMediaHandler {
    static async handle(media, type) {
        const buffer = crypto.randomBytes(12);
        const id = buffer.toString('hex');
        const extension = media.name.split('.').pop();
        const filename = id;

        const typeToFolder = {
            image: 'images',
            video: 'videos'
        };

        const mediaTypeFolder = typeToFolder[type] || 'other';

        const date = moment().format('YYYY-MM');
        const directory = `public/media/${mediaTypeFolder}/${date}`;

        this.createDirectory(directory);

        let meta = {};
        const path = `${directory}/${filename}.${extension}`;

        media.mv(path);

        if (type === 'image') {
            const { width, height } = await sharp(media.data).toFile(
                `${directory}/${id}.webp`
            );

            const breakpoints = [2000, 1200, 640, 480, 320];
            const usedBreakpoints = [];

            for (const newWidth of breakpoints) {
                if (newWidth < width) {
                    usedBreakpoints.push(newWidth);

                    await sharp(media.data)
                        .resize(newWidth)
                        .toFile(`${directory}/${id}_${newWidth}.webp`);

                    await sharp(media.data)
                        .resize(newWidth)
                        .toFile(`${directory}/${id}_${newWidth}.${extension}`);
                }
            }

            meta = {
                breakpoints: usedBreakpoints,
                width,
                height
            };
        }

        return {
            filename,
            extension,
            name: media.name,
            meta,
            date
        };
    }

    static createDirectory(directory) {
        try {
            fs.statSync(directory);
        } catch (e) {
            fs.mkdirSync(directory, { recursive: true });
        }
    }
}

module.exports = StoreMediaHandler;

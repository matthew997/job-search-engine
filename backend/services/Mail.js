const { MailTemplate } = require('../models');
const MailTemplateService = require('./MailTemplate');
const MailPlainTextFromHTMLHandler = require('./MailPlainTextFromHTMLHandler');
const SettingRepository = require('../repositories/Setting');
const fs = require('fs');
const ms = require('ms');

class MailService {
    async triggerEvent(name, email, replacements, app, application_id) {
        const emailQueue = app.get('emailQueue');
        const applicationQueue = app.get('applicationQueue');
        const applied24hQueue = app.get('applied24hQueue');
        const applied48hQueue = app.get('applied48hQueue');

        let emailContent = await MailTemplate.findOne({
            where: {
                code: name
            }
        });

        emailContent = emailContent.toJSON();
        emailContent = MailTemplateService.prepareEmailContent(
            emailContent,
            replacements
        );

        const from = await SettingRepository.getEmailFrom();

        const mailLayout = fs.readFileSync(
            require.resolve('../assets/html/mailLayout.html'),
            'utf-8'
        );

        const emailData = {
            from,
            to: email,
            subject: emailContent.title,
            text: MailPlainTextFromHTMLHandler.handle(emailContent.content),
            html: mailLayout.replace('[content]', emailContent.content)
        };

        if (name === 'NEW_LEAD' || name === 'APPLICATION_CANDIDATE') {
            applicationQueue.add({
                email: emailData,
                application_id
            });
        } else if (name === 'APPLIED_24H') {
            applied24hQueue.add(
                {
                    email: emailData,
                    application_id
                },
                {
                    delay: ms('24 hrs')
                }
            );

            applied48hQueue.add(
                {
                    application_id,
                    replacements
                },
                {
                    delay: ms('48 hrs')
                }
            );
        } else {
            emailQueue.add({
                email: emailData
            });
        }
    }
}

module.exports = MailService;

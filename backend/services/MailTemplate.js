const BaseModelRepository = require('../repositories/BaseModelRepository');
const { MailTemplate } = require('../models');

class MailTemplateService extends BaseModelRepository {
    static get model() {
        return MailTemplate;
    }

    static get templates() {
        return {
            CONTACT: [
                {
                    code: '{{fullName}}',
                    description: "Customer's full name."
                },
                {
                    code: '{{email}}',
                    description: "Customer's email"
                },
                {
                    code: '{{title}}',
                    description: "Email title given by customer"
                },
                {
                    code: '{{message}}',
                    description: 'Message from customer'
                }
            ],
            NEWSLETTER: [
                {
                    code: '{{email}}',
                    description: "Customer's email"
                }
            ],

            APPLICATION_CANDIDATE: [
                {
                    code: '{{companyName}}',
                    description: "Name of the company"
                }
            ],
            NEW_LEAD: [
                {
                    code: '{{fullName}}',
                    description: "Candidate's full name"
                },
                {
                    code: '{{companyName}}',
                    description: "Name of the company"
                }
            ],
            CANDIDATE_STATUS_CHANGE: [
                {
                    code: '{{status}}',
                    description: "New status"
                },
                {
                    code: '{{companyName}}',
                    description: "Name of the company"
                }
            ],
            APPLIED_24H: [
                {
                    code: '{{fullName}}',
                    description: "Candidate's full name"
                },
                {
                    code: '{{companyName}}',
                    description: "Name of the company"
                }
            ],
            INVOICE: [],
            CHANGE_STATUS: [
                {
                    code: '{{status}}',
                    description: "New status"
                },
                {
                    code: '{{companyName}}',
                    description: "Name of the company"
                }
            ],
            RECRUITER_INACTIVE: []
        };
    }

    static getVariablesForTemplate(key) {
        return this.templates[key] || null;
    }

    static prepareEmailContent(emailContent, replacements) {
        for (const replacement of Object.entries(replacements)) {
            const [key, value] = replacement;

            if (emailContent.title) {
                emailContent.title = emailContent.title.split(`{{${key}}}`).join(value);
            }

            if (emailContent.content) {
                emailContent.content = emailContent.content.split(`{{${key}}}`).join(value);
            }
        }

        return emailContent;
    }
}

module.exports = MailTemplateService;

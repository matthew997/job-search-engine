const app = require('../index');
const pending72hQueue = app.get('pending72hQueue');
const inProcess72hQueue = app.get('inProcess72hQueue');
const ms = require('ms');

const UserRepository = require('../repositories/User');

class StatusService {
    static async checkPendingStatusAfter72h(application) {
        await pending72hQueue.add(
            {
                application
            },
            {
                delay: ms('72 hrs')
            }
        );
    }

    static async checkInProgressStatusAfter72Hours(application, replacements) {
        await inProcess72hQueue.add(
            {
                application,
                replacements
            },
            {
                delay: ms('72 hrs')
            }
        );
    }

    static async addUserOneCreditWhenStatusChanged(userId) {
        const user = await UserRepository.findById(userId);

        if (!user) {
            return;
        }

        await user.update(
            {
                application_credits: user.application_credits + 1,
                updated_at: new Date()
            },
            {
                fields: ['application_credits', 'updated_at']
            }
        );
    }
}

module.exports = StatusService;

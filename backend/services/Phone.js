const moment = require('moment');
const Twilio = require('twilio');
const SMS = require('./Twilio');
const config = require('../config');

class PhoneService {
    static normalizeNumber(phone) {
        return phone.replace(/[^+0-9]/g, '');
    }

    static generatePin() {
        const min = 1000;
        const max = Math.random() * 8999;

        return Math.floor(min + max);
    }

    static async sendPin(phone, pin) {
        const message = `Your verification code is: ${pin}`;

        let code;

        try {
            code = await SMS.sendMessage(phone, message);
        } catch (error) {
            console.error(error);
            throw error;
        }

        let errorMessage = null;

        if (code !== 0) {
            if (SMS.errorCodeToMessage.hasOwnProperty(code)) {
                errorMessage = SMS.errorCodeToMessage[code];
            } else {
                errorMessage = `Error sending an SMS (TW:${code})`;
            }
        }

        return errorMessage;
    }

    static canSendPin(date) {
        if (!date) {
            return true;
        }

        const now = moment();
        const lastSentTime = moment(date);
        const timeElapsed = now.diff(lastSentTime, 'seconds');

        return timeElapsed > config.twilio.pinRequestInterval;
    }

    static async validatePhoneNumberOnline(number) {
        if (config.twilio.isSandbox) {
            return;
        }

        const twilio = new Twilio(config.twilio.sid, config.twilio.token);

        await twilio.lookups.phoneNumbers(number).fetch();
    }
}

module.exports = PhoneService;

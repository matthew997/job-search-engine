const fs = require('fs');

class StoreMediaHandler {
    static async handle(media) {
        const directory = `public/media/${media.type}s/${media.date}`;

        const paths = [
            `${directory}/${media.filename}.${media.extension}`,
            `${directory}/${media.filename}.webp`
        ];

        if (media.meta.breakpoints) {
            media.meta.breakpoints.forEach(breakpoint => {
                const webpPath = `${directory}/${media.filename}_${breakpoint}.webp`;
                const oryginalPath = `${directory}/${media.filename}_${breakpoint}.${media.extension}`;

                paths.push(webpPath);
                paths.push(oryginalPath);
            });
        }

        for (const path of paths) {
            try {
                fs.unlinkSync(path);
            } catch (err) {
                console.error('Error during removing file.');
                console.error(err);
            }
        }
    }
}

module.exports = StoreMediaHandler;

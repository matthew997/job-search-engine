const UserRegistrationService = require('../services/UserRegistration');
const PaymentRepository = require('../repositories/Payment');
const UserRepository = require('../repositories/User');

class PaymentService {
    static async create(data) {
        const user = await UserRepository.findById(data.user_id, {
            include: [
                {
                    association: 'registration'
                }
            ]
        });

        if (user && user.registration && !user.registration.third_step) {
            await UserRegistrationService.updateStepByUserId(
                user.id,
                'third_step',
                true
            );
        }

        return PaymentRepository.create(data);
    }
}

module.exports = PaymentService;

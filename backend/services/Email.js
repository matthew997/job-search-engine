const nodemailer = require('nodemailer');
const config = require('../config');

class EmailService {
    constructor() {
        this.mailer = nodemailer.createTransport(config.email);
    }

    send(options) {
        this.mailer.sendMail(options);
    }
}

module.exports = EmailService;

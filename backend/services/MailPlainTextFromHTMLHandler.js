class MailPlainTextFromHTMLHandler {
    static handle(content) {
        const newLineRegex = /<\/div\s*\/?>/gm;
        const regex = /(<([^>]+)>)/gi;

        const contentWithoutBr = content.replace(newLineRegex, '\n');

        return contentWithoutBr.replace(regex, '');
    }
}

module.exports = MailPlainTextFromHTMLHandler;

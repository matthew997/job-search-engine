const UserRepository = require('../repositories/User');

async function AddUserSession(request, response, next) {
    if (request.session && request.session.users && request.headers) {
        const userId = request.session.users.find(
            id => id === request.headers.cluid
        );

        if (userId) {
            const user = await UserRepository.findById(userId);

            if (user) {
                request.loggedUser = user;
                request.isAdmin = await user.isAdmin();
            }
        }
    }

    return next();
}

module.exports = AddUserSession;

const HTTP = require('http-status-codes');

function AdminOnly(request, response, next) {
    if (request.isAdmin) {
        return next();
    }

    return response.sendStatus(HTTP.FORBIDDEN);
}

module.exports = AdminOnly;

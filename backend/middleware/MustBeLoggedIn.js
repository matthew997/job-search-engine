const HTTP = require('http-status-codes');
const UserRepository = require('../repositories/User');

async function MustBeLoggedIn(request, response, next) {
    if (request.session && request.session.users && request.headers) {
        const userId = request.session.users.find(
            id => id === request.headers.cluid
        );

        if (userId) {
            const user = await UserRepository.findById(userId);

            if (!user) {
                return response.sendStatus(HTTP.FORBIDDEN);
            }

            request.session.user = user;
        }
    }

    if (request.loggedUser && request.loggedUser.registration_completed) {
        return next();
    }

    return response.sendStatus(HTTP.UNAUTHORIZED);
}

module.exports = MustBeLoggedIn;

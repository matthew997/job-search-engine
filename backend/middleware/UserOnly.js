const HTTP = require('http-status-codes');
const UserRepository = require('../repositories/User');

async function UserOnly(request, response, next) {
    if (request.session && request.session.users) {
        const currentUserId = request.session.users.find(
            (id) => id === request.headers.cluid
        );

        const currentUser = await UserRepository.findById(currentUserId);

        if (!currentUser || !(await currentUser.isUser())) {
            return response.sendStatus(HTTP.FORBIDDEN);
        }

        request.loggedUser = currentUser;

        return next();
    }

    return response.sendStatus(HTTP.UNAUTHORIZED);
}

module.exports = UserOnly;

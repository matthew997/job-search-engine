const HTTP = require('http-status-codes');

function MustBeLoggedInIncomplete(request, response, next) {
    if (request.loggedUser) {
        return next();
    }

    return response.sendStatus(HTTP.UNAUTHORIZED);
}

module.exports = MustBeLoggedInIncomplete;

const SettingController = require('../controllers/SettingController');

const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const AdminOnly = require('../middleware/AdminOnly');

module.exports = app => {
    const adminMiddlewares = [MustBeLoggedIn, AdminOnly];
    const settingController = new SettingController();

    app.get('/api/settings', adminMiddlewares, settingController.index);
    app.get(
        '/api/settings/tag/:tag',
        adminMiddlewares,
        settingController.getByTag
    );
    app.get(
        '/api/settings/key/:key',
        adminMiddlewares,
        settingController.getByKey
    );
    app.post('/api/settings', adminMiddlewares, settingController.update);

    app.get('/api/settings/public', settingController.public);
    app.get('/api/settings/public/:key', settingController.publicKey);
};

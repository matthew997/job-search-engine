const ChangeController = require('../controllers/ChangeController');
const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const AdminOnly = require('../middleware/AdminOnly');

module.exports = app => {
    const di = app.get('di');
    const mailService = di.get('mailService');
    const changeController = new ChangeController(mailService);

    app.get(
        '/api/changes',
        [MustBeLoggedIn, AdminOnly],
        changeController.index
    );

    app.post(
        '/api/changes/bulk',
        [MustBeLoggedIn, AdminOnly],
        changeController.bulk
    );

    app.post(
        '/api/changes/:id/status',
        [MustBeLoggedIn, AdminOnly],
        changeController.setStatus
    );
};

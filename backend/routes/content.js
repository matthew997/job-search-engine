const ContentController = require('../controllers/ContentController');

const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const AdminOnly = require('../middleware/AdminOnly');

module.exports = app => {
    const contentController = new ContentController();

    app.get('/api/content', contentController.index);
    app.get('/api/content/:key', contentController.show);
    app.put(
        '/api/content/:key',
        [MustBeLoggedIn, AdminOnly],
        contentController.update
    );
};

const JobTypeController = require('../controllers/JobTypeController');
const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const AdminOnly = require('../middleware/AdminOnly');

module.exports = app => {
    const jobTypeController = new JobTypeController();
    const adminMiddlewares = [MustBeLoggedIn, AdminOnly];

    app.get('/api/job-types', jobTypeController.index);
    app.get('/api/job-types/:id', jobTypeController.show);

    app.post('/api/job-types', adminMiddlewares, jobTypeController.store);

    app.delete(
        '/api/job-types/:id',
        adminMiddlewares,
        jobTypeController.destroy
    );

    app.put('/api/job-types/:id', adminMiddlewares, jobTypeController.update);
};

const AuthController = require('../controllers/AuthController');

const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const MustBeLoggedInIncomplete = require('../middleware/MustBeLoggedInIncomplete');
const AdminOnly = require('../middleware/AdminOnly');

const Validate = require('../middleware/Validate');
const registerValidator = require('../validators/auth/register');
const loginValidator = require('../validators/auth/login');
const finishRegistrationValidator = require('../validators/auth/finishRegistration');
const phoneNumberValidator = require('../validators/phoneNumber');

module.exports = app => {
    const di = app.get('di');
    const mailService = di.get('mailService');
    const authController = new AuthController(mailService);

    app.post(
        '/api/auth/register',
        [phoneNumberValidator, registerValidator, Validate],
        authController.register
    );

    app.patch(
        '/api/auth/register',
        [phoneNumberValidator, registerValidator, Validate],
        authController.patchRegistered
    );

    app.post(
        '/api/auth/register-company',
        [phoneNumberValidator, registerValidator, Validate],
        authController.register
    );

    app.post(
        '/api/auth/login',
        [phoneNumberValidator, loginValidator, Validate],
        authController.login
    );

    app.post('/api/auth/logout', authController.logout);
    app.post('/api/auth/forgot-password', authController.passwordResetLink);
    app.post(
        '/api/auth/request-pin',
        [phoneNumberValidator, Validate],
        authController.requestPin
    );
    app.post('/api/auth/check-pin', authController.checkPin);
    app.post(
        '/api/auth/register/finish',
        [MustBeLoggedInIncomplete, finishRegistrationValidator, Validate],
        authController.userFinishRegistration
    );

    app.post('/api/auth/reset-password/:token', authController.passwordReset);
    app.get(
        '/api/auth/reset-password/:token',
        authController.checkPasswordReset
    );

    app.post(
        '/api/auth/add-user-session/:id',
        [MustBeLoggedIn, AdminOnly],
        authController.addUserSession
    );
};

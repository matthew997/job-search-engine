const DropdownController = require('../controllers/DropdownController');

const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const AdminOnly = require('../middleware/AdminOnly');

module.exports = app => {
    const dropdownController = new DropdownController();

    app.get('/api/dropdowns', dropdownController.index);
    app.get('/api/dropdowns/:name', dropdownController.show);

    app.post(
        '/api/dropdowns',
        [MustBeLoggedIn, AdminOnly],
        dropdownController.store
    );
    app.patch(
        '/api/dropdowns/:name',
        [MustBeLoggedIn, AdminOnly],
        dropdownController.update
    );
    app.delete(
        '/api/dropdowns/:name',
        [MustBeLoggedIn, AdminOnly],
        dropdownController.destroy
    );
    app.put(
        '/api/dropdowns/:name/options',
        [MustBeLoggedIn, AdminOnly],
        dropdownController.updateOptions
    );
};

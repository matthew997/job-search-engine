const MediaController = require('../controllers/MediaController');

const fileUpload = require('express-fileupload');
const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const AdminOnly = require('../middleware/AdminOnly');

module.exports = app => {
    const adminMiddlewares = [MustBeLoggedIn, AdminOnly];
    const videoUploadQueue = app.get('videoUploadQueue');
    const mediaController = new MediaController(videoUploadQueue);

    app.get('/api/media', mediaController.index);
    app.get('/api/media/:id', mediaController.show);
    app.delete('/api/media/:id', adminMiddlewares, mediaController.destroy);

    app.post(
        '/api/media',
        [
            adminMiddlewares,
            fileUpload({
                createParentPath: true
            })
        ],
        mediaController.store
    );

    app.patch('/api/media/:id', mediaController.update);
};

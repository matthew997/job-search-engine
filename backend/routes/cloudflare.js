const CloudflareController = require('../controllers/CloudflareController');

module.exports = app => {
    const cloudflareController = new CloudflareController();

    app.post('/api/cloudflare/webhook', cloudflareController.webhook);
};

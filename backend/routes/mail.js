const MailController = require('../controllers/MailController');

module.exports = app => {
    const di = app.get('di');
    const mailService = di.get('mailService');
    const mailController = new MailController(mailService);

    app.post('/api/mail/newsletter', mailController.newsletter);
    app.post('/api/mail/contact', mailController.contact);
};

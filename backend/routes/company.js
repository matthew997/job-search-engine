const fileUpload = require('express-fileupload');

const CompanyController = require('../controllers/CompanyController');
const CompanyFilterController = require('../controllers/CompanyFilterController');
const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const MustBeLoggedInIncomplete = require('../middleware/MustBeLoggedInIncomplete');
const AdminOnly = require('../middleware/AdminOnly');
const adminMiddlewares = [MustBeLoggedIn, AdminOnly];

module.exports = app => {
    const videoUploadQueue = app.get('videoUploadQueue');

    const companyController = new CompanyController(videoUploadQueue);
    const companyFilterController = new CompanyFilterController();

    app.get(
        '/api/companies',
        MustBeLoggedInIncomplete,
        companyController.index
    );

    app.get(
        '/api/companies/:id/applications',
        MustBeLoggedIn,
        companyController.getApplicationsForCompany
    );

    app.get(
        '/api/companies/:id/reviews',
        MustBeLoggedIn,
        companyController.getReviewsForCompany
    );

    app.get(
        '/api/companies/cms',
        adminMiddlewares,
        companyController.getCompaniesForCMS
    );

    app.get('/api/companies/filters', companyController.filters);
    app.post(
        '/api/companies',
        [
            MustBeLoggedInIncomplete,
            fileUpload({
                createParentPath: true
            })
        ],
        companyController.store
    ); // @TODO validation
    app.delete('/api/companies/:id', MustBeLoggedIn, companyController.destroy);
    app.patch(
        '/api/companies/:id',
        [
            MustBeLoggedIn,
            fileUpload({
                createParentPath: true
            })
        ],
        companyController.update
    );
    app.get('/api/companies/:id/locked', companyController.showLocked);
    app.post(
        '/api/companies/:id/set-active-status',
        companyController.setActiveStatus
    );
    app.post(
        '/api/companies/:id/add-free-leads',
        companyController.addFreeLeads
    );
    app.post('/api/companies/:id/add-free-days', companyController.addFreeDays);

    app.get(
        '/api/companies/:id/payment-tab',
        adminMiddlewares,
        companyController.paymentTab
    );

    app.get('/api/companies/:id', MustBeLoggedIn, companyController.show);

    app.post(
        '/api/companies/:companyId/filter',
        MustBeLoggedIn,
        companyFilterController.update
    );

    app.patch(
        '/api/companies/:id/set-homepage-status',
        adminMiddlewares,
        companyController.setHomepageStatus
    );

    app.post(
        '/api/companies/:userId/accepted-payment-terms',
        MustBeLoggedInIncomplete,
        companyController.setPaymentTerms
    );
};

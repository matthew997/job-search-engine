const UserController = require('../controllers/UserController');
const UserCompanyController = require('../controllers/UserCompanyController');

const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const MustBeLoggedInIncomplete = require('../middleware/MustBeLoggedInIncomplete');
const AdminOnly = require('../middleware/AdminOnly');
const Validate = require('../middleware/Validate');

const UpdateUserDetailsValidator = require('../validators/personal-zone/UserDetails');
const UpdateRecruiterDetailsValidator = require('../validators/personal-zone/RecruiterDetails');
const fileUpload = require('express-fileupload');
const updateCreditsValidator = require('../validators/users/updateCredits');
const storeValidator = require('../validators/users/store');

const adminMiddleware = [MustBeLoggedIn, AdminOnly];

module.exports = app => {
    const userController = new UserController();
    const userCompanyController = new UserCompanyController();

    app.get('/api/user/companies', MustBeLoggedIn, userCompanyController.index);

    app.get('/api/users', adminMiddleware, userController.index);

    app.post('/api/users', adminMiddleware, userController.store);

    app.delete('/api/users/:id', adminMiddleware, userController.destroy);

    app.post('/api/users/:id/restore', adminMiddleware, userController.restore);

    app.put(
        '/api/users/image',
        [MustBeLoggedIn, fileUpload({ createParentPath: true })],
        userController.updateImage
    );

    app.post('/api/users/bulk', adminMiddleware, userController.bulk);

    app.put('/api/users/:id', adminMiddleware, userController.update);
    app.get('/api/users/:id', adminMiddleware, userController.show);

    app.get('/api/me', MustBeLoggedInIncomplete, userController.me);

    app.post('/api/profile', adminMiddleware, userController.profile);

    app.post(
        '/api/profile-password',
        MustBeLoggedIn,
        userController.updatePassword
    );

    app.get(
        '/api/details',
        MustBeLoggedIn,
        userController.getProfileInformation
    );
    app.post(
        '/api/details/user',
        [MustBeLoggedIn, UpdateUserDetailsValidator, Validate],
        userController.updateUserDetails
    );
    app.post(
        '/api/details/recruiter',
        [MustBeLoggedIn, UpdateRecruiterDetailsValidator, Validate],
        userController.updateRecruiterDetails
    );

    app.patch(
        '/api/users/:id/credits',
        [adminMiddleware, updateCreditsValidator, Validate],
        userController.updateUserCredits
    );
};

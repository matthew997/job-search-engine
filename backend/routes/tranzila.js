const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const MustBeLoggedInIncomplete = require('../middleware/MustBeLoggedInIncomplete');
const TranzilaController = require('../controllers/TranzilaController');

module.exports = app => {
    const di = app.get('di');
    const tranzilaService = di.get('tranzilaService');
    const tranzilaController = new TranzilaController(tranzilaService);

    app.post('/api/tranzila/webhook', tranzilaController.webhook);

    app.get(
        '/api/tranzila/add-card-payment-url',
        MustBeLoggedInIncomplete,
        tranzilaController.getAddCardPaymentUrl
    );

    app.get(
        '/api/tranzila/subscription-payment-url',
        MustBeLoggedInIncomplete,
        tranzilaController.getSubscriptionPaymentUrl
    );
};

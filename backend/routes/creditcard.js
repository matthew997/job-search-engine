const CreditCardController = require('../controllers/CreditCardController');
const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');

module.exports = app => {
    const creditCardController = new CreditCardController();

    app.get('/api/credit-cards', MustBeLoggedIn, creditCardController.index);

    app.post(
        '/api/credit-cards/default',
        MustBeLoggedIn,
        creditCardController.setDefault
    );

    app.delete(
        '/api/credit-cards/:id',
        MustBeLoggedIn,
        creditCardController.destroy
    );
};

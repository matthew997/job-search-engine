const ReviewController = require('../controllers/ReviewController');
const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const AdminOnly = require('../middleware/AdminOnly');
const adminMiddlewares = [MustBeLoggedIn, AdminOnly];

module.exports = app => {
    const reviewController = new ReviewController();

    app.get('/api/reviews', adminMiddlewares, reviewController.index);
    app.patch('/api/reviews/:id', adminMiddlewares, reviewController.update);
    app.delete('/api/reviews/:id', adminMiddlewares, reviewController.destroy);
};

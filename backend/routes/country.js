const fileUpload = require('express-fileupload');
const CountryController = require('../controllers/CountryController');
const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const AdminOnly = require('../middleware/AdminOnly');

module.exports = app => {
    const countryController = new CountryController();
    const adminMiddlewares = [MustBeLoggedIn, AdminOnly];

    app.get('/api/countries', countryController.index);
    app.get('/api/countries/:id', countryController.show);
    app.post('/api/countries', adminMiddlewares, countryController.store);

    app.post(
        '/api/countries/:id/import-cities',
        [
            adminMiddlewares,
            fileUpload({
                createParentPath: true
            })
        ],
        countryController.importCities
    );

    app.post('/api/countries/:id', adminMiddlewares, countryController.update);
};


const MailTemplateController = require('../controllers/MailTemplateController');
const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const AdminOnly = require('../middleware/AdminOnly');
const middlewares = [MustBeLoggedIn, AdminOnly];

module.exports = app => {
    const mailTemplateController = new MailTemplateController();

    app.get('/api/mails', middlewares, mailTemplateController.index);
    app.get(
        '/api/mails/variables/:key',
        middlewares,
        mailTemplateController.variables
    );
    app.get('/api/mails/:id', middlewares, mailTemplateController.show);
    app.put(
        '/api/mails/:id',
        middlewares,
        mailTemplateController.update
    );
};

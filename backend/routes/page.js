const PageController = require('../controllers/PageController');
const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const AdminOnly = require('../middleware/AdminOnly');

module.exports = app => {
    const adminMiddlewares = [MustBeLoggedIn, AdminOnly];
    const pageController = new PageController();

    app.get('/api/pages', adminMiddlewares, pageController.index);
    app.post('/api/pages', adminMiddlewares, pageController.store);
    app.get('/api/pages/:idOrSlug', pageController.show);
    app.put('/api/pages/:id', adminMiddlewares, pageController.update);
    app.delete('/api/pages/:id', adminMiddlewares, pageController.destroy);
    app.patch('/api/pages/bulk', adminMiddlewares, pageController.bulk);
    app.patch('/api/pages/:id/status', adminMiddlewares, pageController.status);
};

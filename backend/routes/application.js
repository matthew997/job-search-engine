const ApplicationController = require('../controllers/ApplicationController');
const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');

module.exports = app => {
    const di = app.get('di');
    const mailService = di.get('mailService');
    const applicationController = new ApplicationController(mailService);

    app.get('/api/applications', MustBeLoggedIn, applicationController.index);
    app.get(
        '/api/applications/:id',
        MustBeLoggedIn,
        applicationController.show
    );
    app.post('/api/applications', MustBeLoggedIn, applicationController.store);

    app.post(
        '/api/applications/cancel',
        MustBeLoggedIn,
        applicationController.cancel
    );
    app.delete(
        '/api/applications/:id',
        MustBeLoggedIn,
        applicationController.destroy
    );
    app.post(
        '/api/applications/:id/update-note',
        MustBeLoggedIn,
        applicationController.updateNote
    );
    app.post(
        '/api/applications/:id/update-status',
        MustBeLoggedIn,
        applicationController.updateStatus
    );
    app.get(
        '/api/recruiter/applications',
        MustBeLoggedIn,
        applicationController.getApplicationsForRecruiter
    );
};

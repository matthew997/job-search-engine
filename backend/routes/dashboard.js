const DashboardController = require('../controllers/DashboardController');
const AdminOnly = require('../middleware/AdminOnly');

module.exports = app => {
    const dashboardController = new DashboardController();

    app.get('/api/dashboard/statistics', AdminOnly, dashboardController.statistics);
};

module.exports = app => {
    app.get('/', (req, res) => res.send({message: 'Use /api'}));
    app.get('/api', (req, res) => res.send({message: 'API is working'}));
};

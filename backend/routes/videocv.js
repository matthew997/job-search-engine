const fileUpload = require('express-fileupload');

const CVQuestionController = require('../controllers/CVQuestionController');
const CVAnswerController = require('../controllers/CVAnswerController');

const CVQuestionsValidate = require('../validators/videocv/CVQuestions');
const CVAnswersValidate = require('../validators/videocv/CVAnswers');

const MustBeLoggedIn = require('../middleware/MustBeLoggedIn');
const MustBeLoggedInIncomplete = require('../middleware/MustBeLoggedInIncomplete');
const Validate = require('../middleware/Validate');

module.exports = app => {
    const videoUploadQueue = app.get('videoUploadQueue');

    const cvAnswerController = new CVAnswerController(videoUploadQueue);
    const cvQuestionController = new CVQuestionController();

    app.get('/api/cv-video', MustBeLoggedIn, cvAnswerController.showAllForUser);
    app.get('/api/cv-video-answers', cvAnswerController.index);
    app.get('/api/cv-video-answers/:id', cvAnswerController.show);
    app.get('/api/cv-questions', cvQuestionController.index);
    app.get('/api/cv-questions/:id', cvQuestionController.show);

    app.post(
        '/api/cv-video-answers',
        [
            MustBeLoggedInIncomplete,
            fileUpload({
                createParentPath: true
            }),
            CVAnswersValidate,
            Validate
        ],
        cvAnswerController.store
    );
    app.post('/api/cv-questions/order', cvQuestionController.changeOrder);
    app.post(
        '/api/cv-questions',
        [CVQuestionsValidate, Validate],
        cvQuestionController.store
    );

    app.patch(
        '/api/cv-questions/:id',
        [CVQuestionsValidate, Validate],
        cvQuestionController.update
    );

    app.put(
        '/api/cv-video-answers/:id',
        [CVAnswersValidate, Validate],
        cvAnswerController.update
    );

    app.delete('/api/cv-video-answers/:id', cvAnswerController.destroy);
    app.delete('/api/cv-video', cvAnswerController.destroyAllForUser);
    app.delete('/api/cv-questions/:id', cvQuestionController.destroy);
};

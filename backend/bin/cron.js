const Sentry = require('@sentry/node');
const lockfile = require('proper-lockfile');

const config = require('../config');
const CronService = require('../services/Cron');

const cronScheduler = new CronService();
const { cronDsn } = config.sentry;

const RenewSubscription = require('../crons/RenewSubscription');
const PerLeadPayment = require('../crons/PerLeadPayment');

const Queue = require('bull');

const renewSubscriptionQueue = new Queue('RenewSubscription', {
    redis: config.redis
});
const perLeadPaymentQueue = new Queue('PerLeadPayment', {
    redis: config.redis
});

if (cronDsn) {
    Sentry.init({ dsn: cronDsn });
}

const renewSubscription = new RenewSubscription(renewSubscriptionQueue);
const perLeadPayment = new PerLeadPayment(perLeadPaymentQueue);

(async () => {
    const isLocked = await lockfile.check(__filename);

    if (isLocked) {
        console.log(`This script is already running!`);
        process.exit(1);
    }

    await lockfile.lock(__filename);

    cronScheduler.addJob(
        'renewSubscription',
        'EVERY_MINUTE',
        (...args) => renewSubscription.handle(...args)
    );

    cronScheduler.addJob(
        'perLeadPayment',
        'EVERY_MONTH',
        (...args) => perLeadPayment.handle(...args)
    );

    console.log('Scheduler started.');
})();

process
    .once('SIGINT', () => process.exit(1))
    .once('SIGTERM', () => process.exit(1));

module.exports = cronScheduler;

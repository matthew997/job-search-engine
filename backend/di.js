const { ContainerBuilder } = require('node-dependency-injection');

const TranzilaService = require('./services/Tranzila');
const TranzilaClient = require('./services/TranzilaClient');
const SettingRepository = require('./repositories/Setting');

const tranzilaServiceFactory = async () => {
    const tranzilaCredentials = await SettingRepository.getTranzilaCredentials();
    const tranzilaClient = new TranzilaClient(tranzilaCredentials);

    return new TranzilaService(tranzilaClient);
};

module.exports = async app => {
    const container = new ContainerBuilder();
    const redis = app.get('redis');

    const CacheService = require('./services/Cache');
    const MailService = require('./services/Mail');

    const cacheService = new CacheService(redis);
    const mailService = new MailService();
    const tranzilaService = await tranzilaServiceFactory();

    const cacheServiceDefinition = container.register('cacheService');
    const mailServiceDefinition = container.register('mailService');
    const tranzilaServiceDefinition = container.register('tranzilaService');
    const tranzilaServiceFactoryDefinition = container.register(
        'tranzilaServiceFactory'
    );

    cacheServiceDefinition.synthetic = true;
    mailServiceDefinition.synthetic = true;
    tranzilaServiceDefinition.synthetic = true;
    tranzilaServiceFactoryDefinition.synthetic = true;

    container.set('cacheService', cacheService);
    container.set('mailService', mailService);
    container.set('tranzilaService', tranzilaService);
    container.set('tranzilaServiceFactory', tranzilaServiceFactory);

    app.set('di', container);
};

require('dotenv').config();

const env = (key, defaultValue = null) => process.env[key] || defaultValue;
const isEnabled = key => env(key) && env(key) === 'true';

const config = {
    app: {
        env: env('NODE_ENV'),
        url: env('APP_URL', 'http://localhost:3000'),
        port: parseInt(env('PORT', 3000)),
        host: env('APP_HOST', '127.0.0.1'),
        secret: env('APP_SECRET'),
        frontendUrl: env('APP_FRONTEND_URL'),
        adminUrl: env('APP_ADMIN_URL'),
        multisite: env('APP_MULTISITE', ''),
        jsonRequestSizeLimit: env('APP_JSON_REQUEST_SIZE_LIMIT')
    },
    db: {
        url: env('DATABASE_URL'),
        host: env('DATABASE_HOST', 'localhost'),
        name: env('DATABASE_NAME'),
        username: env('DATABASE_USERNAME'),
        password: env('DATABASE_PASSWORD'),
        port: parseInt(env('DATABASE_PORT'), 27017),
        dialect: env('DATABASE_DIALECT', 'mysql'),
        logging: isEnabled('SEQUELIZE_LOGGING') ? console.log : false,
        define: {
            charset: 'utf8mb4',
            collate: 'utf8mb4_unicode_ci',
            timestamps: false
        }
    },
    redis: {
        host: env('REDIS_HOST'),
        port: env('REDIS_PORT'),
        pass: env('REDIS_PASS') ? env('REDIS_PASS') : undefined,
        password: env('REDIS_PASS') ? env('REDIS_PASS') : undefined,
        ttl: env('REDIS_TTL')
    },
    cache: {
        keyExpiresInMinutes: parseInt(env('CACHE_KEY_EXPIRES_IN_MINUTES', 0))
    },
    email: {
        host: env('EMAIL_HOST'),
        port: env('EMAIL_PORT'),
        secure: isEnabled('EMAIL_SECURE'),
        auth: {
            user: env('EMAIL_AUTH_USER'),
            pass: env('EMAIL_AUTH_PASSWORD')
        },
        from: {
            name: env('EMAIL_FROM_NAME'),
            address: env('EMAIL_FROM_ADDRESS')
        }
    },
    twilio: {
        sid: env('TWILIO_SID'),
        token: env('TWILIO_TOKEN'),
        from: env('TWILIO_FROM'),
        pinRequestInterval: parseInt(env('TWILIO_PIN_REQUEST_INTERVAL', 10)),
        useFixedPin: isEnabled('TWILIO_USE_FIXED_PIN'),
        defaultPin: env('TWILIO_DEFAULT_PIN', 111111),
        isSandbox: isEnabled('TWILIO_IS_SANDBOX')
    },
    sentry: {
        dsn: env('SENTRY_DSN'),
        dsnCron: env('SENTRY_DSN_CRON')
    },
    cloudflare: {
        accountId: env('CLOUDFLARE_ACCOUNT_ID'),
        apiKey: env('CLOUDFLARE_API_KEY'),
        email: env('CLOUDFLARE_EMAIL')
    }
};

module.exports = config;

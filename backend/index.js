const path = require('path');
const redisLib = require('redis');
const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const session = require('express-session');
const RedisStore = require('connect-redis')(session);
const Sentry = require('@sentry/node');
const config = require('./config');

const AddUserSession = require('./middleware/AddUserSession');

const app = express();

if (config.sentry.dsn) {
    Sentry.init({ dsn: config.sentry.dsn });
    app.use(Sentry.Handlers.requestHandler());
}

app.use(helmet());

const routesWithoutBodyParser = [];

app.use(bodyParser.urlencoded({ extended: true, limit: '1mb' }));
app.use((req, res, next) => {
    if (routesWithoutBodyParser.find(item => item === req.originalUrl)) {
        next();
    } else {
        bodyParser.json({ limit: config.app.jsonRequestSizeLimit })(
            req,
            res,
            next
        );
    }
});

app.use('/public', express.static(path.join(__dirname, './public')));

const redisClient = redisLib.createClient({
    ...config.redis
});

const redisStore = new RedisStore({
    client: redisClient
});

redisClient.unref();

const sessionConfig = {
    store: redisStore,
    secret: config.app.secret,
    resave: false,
    saveUninitialized: false,
    cookie: {}
};

if (config.app.env === 'production') {
    app.set('trust proxy', 1);
    sessionConfig.cookie.secure = true;
}

app.use(session(sessionConfig));

const multisite = config.app.multisite.split(',').map(site => site.trim());
const originsWhitelist = [
    'http://localhost:8080',
    config.app.frontendUrl,
    config.app.adminUrl,
    ...multisite
];
const corsOptions = {
    origin(origin, callback) {
        if (!origin || originsWhitelist.indexOf(origin) !== -1) {
            callback(null, true);
        } else {
            callback(new Error('Not allowed by CORS'));
        }
    },
    credentials: true
};

app.use(cors(corsOptions));
app.use(function(error, request, response, next) {
    if (error.message !== 'Not allowed by CORS') {
        return next();
    }

    return response
        .status(200)
        .json({ code: 200, message: 'Request not allowed by CORS' });
});

app.use(AddUserSession);

const Queue = require('bull');

const emailQueue = new Queue('email', {
    redis: config.redis
});
const applicationQueue = new Queue('application', {
    redis: config.redis
});
const applied24hQueue = new Queue('applied24h', {
    redis: config.redis
});
const perLeadPaymentQueue = new Queue('PerLeadPayment', {
    redis: config.redis
});
const renewSubscriptionQueue = new Queue('RenewSubscription', {
    redis: config.redis
});
const videoUploadQueue = new Queue('VideoUpload', {
    redis: config.redis
});
const applied48hQueue = new Queue('applied48h', {
    redis: config.redis
});
const pending72hQueue = new Queue('pending72h', {
    redis: config.redis
});
const inProcess72hQueue = new Queue('inProcess72h', {
    redis: config.redis
});

app.set('redis', redisClient);
app.set('emailQueue', emailQueue);
app.set('applicationQueue', applicationQueue);
app.set('applied24hQueue', applied24hQueue);
app.set('videoUploadQueue', videoUploadQueue);
app.set('applied48hQueue', applied48hQueue);
app.set('pending72hQueue', pending72hQueue);
app.set('inProcess72hQueue', inProcess72hQueue);

emailQueue.process(__dirname + '/queue/processors/Email.js');
applicationQueue.process(__dirname + '/queue/processors/Application.js');
applied24hQueue.process(__dirname + '/queue/processors/Applied24h.js');
applied48hQueue.process(__dirname + '/queue/processors/Applied48h.js');
pending72hQueue.process(__dirname + '/queue/processors/Pending72h.js');
inProcess72hQueue.process(__dirname + '/queue/processors/InProcess72h.js');

const PerLeadPaymentProcessor = require('./queue/processors/PerLeadPayment');
const RenewSubscriptionProcessor = require('./queue/processors/RenewSubscription');
const VideoUploadProcessor = require('./queue/processors/VideoUpload');

perLeadPaymentQueue.process(job => {
    try {
        const perLeadPayment = new PerLeadPaymentProcessor(app, job);

        return perLeadPayment.run();
    } catch (error) {
        console.error(error);
        return Promise.reject(error);
    }
});

renewSubscriptionQueue.process(job => {
    try {
        const renewSubscription = new RenewSubscriptionProcessor(app, job);

        return renewSubscription.run();
    } catch (error) {
        console.error(error);
        return Promise.reject(error);
    }
});

videoUploadQueue.process(job => {
    try {
        const videoUpload = new VideoUploadProcessor(app, job);

        return videoUpload.run();
    } catch (error) {
        console.error(error);
        return Promise.reject(error);
    }
});

(async () => {
    await require('./di')(app);
    await require(`./routes`)(app);
    await require(`./routeErrorHandler`)(app);
})();

app.use(Sentry.Handlers.errorHandler());

module.exports = app;

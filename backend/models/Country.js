'use strict';
module.exports = (sequelize, DataTypes) => {
    const Country = sequelize.define(
        'Country',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'countries'
        }
    );

    Country.associate = function (models) {
        Country.hasMany(models.City, {
            as: 'cities',
            foreignKey: 'country_id'
        });

        Country.hasMany(models.Company, {
            as: 'companies',
            foreignKey: 'country_id'
        });

        Country.hasMany(models.Review, {
            as: 'reviews',
            foreignKey: 'country'
        });
    };

    Country.addScope('defaultScope', {
        include: [
            {
                association: 'cities'
            }
        ]
    });

    return Country;
};

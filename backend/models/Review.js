'use strict';
module.exports = (sequelize, DataTypes) => {
    const Review = sequelize.define(
        'Review',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            user_id: {
                type: DataTypes.UUIDV4,
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            company_id: {
                type: DataTypes.UUIDV4,
                allowNull: true,
                references: {
                    model: 'companies',
                    key: 'id'
                }
            },
            start_date: {
                allowNull: false,
                type: DataTypes.DATEONLY
            },
            end_date: {
                allowNull: false,
                type: DataTypes.DATEONLY
            },
            rating: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            company: {
                type: DataTypes.STRING,
                allowNull: false
            },
            country: {
                type: DataTypes.STRING,
                allowNull: false,
                references: {
                    model: 'countries',
                    key: 'id'
                }
            },
            city: {
                type: DataTypes.STRING,
                allowNull: false,
                references: {
                    model: 'cities',
                    key: 'id'
                }
            },
            phone: {
                type: DataTypes.STRING,
                allowNull: false
            },
            phone_2: {
                type: DataTypes.STRING,
                allowNull: true
            },
            content: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            is_recommended: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            deleted_at: {
                allowNull: true,
                defaultValue: null,
                type: DataTypes.DATE
            }
        },
        {
            timestamps: true,
            underscored: true,
            tableName: 'reviews',
            paranoid: true
        }
    );

    Review.associate = ({ User, Company, City, Country }) => {
        Review.belongsTo(User, {
            as: 'user',
            foreignKey: 'user_id'
        });

        Review.belongsTo(Company, {
            as: 'companies',
            foreignKey: 'company_id'
        });

        // @TODO  fix companies to company and cities to city

        Review.belongsTo(Country, {
            as: 'countries',
            foreignKey: 'country'
        });

        Review.belongsTo(City, {
            as: 'cities',
            foreignKey: 'city'
        });
    };

    return Review;
};

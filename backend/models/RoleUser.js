('use strict');
module.exports = (sequelize, DataTypes) => {
    const RoleUser = sequelize.define(
        'RoleUser',
        {
            user_id: {
                type: DataTypes.UUID,
                allowNull: false,
                primaryKey: true
            },
            role_id: {
                type: DataTypes.UUID,
                allowNull: false,
                primaryKey: true
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'role_user'
        }
    );

    RoleUser.associate = models => {
        RoleUser.belongsTo(models.Role, {
            as: 'role',
            foreignKey: 'role_id'
        });
    };

    return RoleUser;
};

'use strict';
module.exports = (sequelize, DataTypes) => {
    const JobType = sequelize.define(
        'JobType',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            slug: {
                type: DataTypes.STRING,
                allowNull: false
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            deleted_at: {
                allowNull: true,
                defaultValue: null,
                type: DataTypes.DATE
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'job_types'
        }
    );

    JobType.associate = function (models) {
        JobType.hasMany(models.Company, { as: 'companies', foreignKey: 'type_id' });
    };

    return JobType;
};

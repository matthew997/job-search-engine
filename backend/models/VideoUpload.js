'use strict';
module.exports = (sequelize, DataTypes) => {
    const VideoUpload = sequelize.define(
        'VideoUpload',
        {
            id: {
                primaryKey: true,
                type: DataTypes.STRING
            },
            meta: {
                type: DataTypes.TEXT,
                allowNull: true,
                get() {
                    if (!this.getDataValue('meta')) {
                        return {};
                    }

                    return JSON.parse(this.getDataValue('meta'));
                },
                set(value) {
                    this.setDataValue('meta', JSON.stringify(value));
                }
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'video_uploads'
        }
    );

    return VideoUpload;
};

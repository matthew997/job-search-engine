'use strict';
module.exports = (sequelize, DataTypes) => {
    const CompanyLeadsFilter = sequelize.define(
        'CompanyLeadsFilter',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            company_id: {
                type: DataTypes.UUID,
                references: {
                    model: 'companies',
                    key: 'id'
                }
            },
            leads_limit: {
                type: DataTypes.INTEGER,
                allowNull: true,
                defaultValue: null
            },
            minimum_age: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            maximum_age: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            sex: {
                type: DataTypes.STRING,
                allowNull: true,
                defaultValue: null
            },
            has_american_visa: {
                allowNull: true,
                type: DataTypes.BOOLEAN
            },
            has_foreign_passport: {
                allowNull: true,
                type: DataTypes.BOOLEAN
            },
            gold_users_only: {
                allowNull: false,
                type: DataTypes.BOOLEAN
            },
            availability: {
                type: DataTypes.TEXT,
                allowNull: true,
                get() {
                    if (!this.getDataValue('availability')) {
                        return [];
                    }

                    return JSON.parse(this.getDataValue('availability'));
                },
                set(value) {
                    this.setDataValue('availability', JSON.stringify(value));
                }
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'company_leads_filters'
        }
    );

    CompanyLeadsFilter.associate = function (models) {
        CompanyLeadsFilter.belongsTo(models.Company, { as: 'company' });
        CompanyLeadsFilter.hasMany(models.Change, {
            as: 'changes',
            foreignKey: 'company_id',
            sourceKey: 'company_id',
            scope: {
                type: 'CompanyLeadsFilter'
            }
        });
    };

    CompanyLeadsFilter.publishChanges = (companyId, changes) => {
        return CompanyLeadsFilter.update(changes, {
            where: {
                company_id: companyId
            }
        });
    };

    CompanyLeadsFilter.AVAILABILITY_IMMEDIATE = 'immediate';
    CompanyLeadsFilter.AVAILABILITY_ONE_TO_THREE_MONTHS = '1-3m';
    CompanyLeadsFilter.AVAILABILITY_THREE_TO_SIX_MONTHS = '3-6m';
    CompanyLeadsFilter.EDITABLE_FIELDS = [
        'sex',
        'leads_limit',
        'minimum_age',
        'maximum_age',
        'availability',
        'gold_users_only',
        'has_american_visa',
        'has_foreign_passport'
    ];

    return CompanyLeadsFilter;
};

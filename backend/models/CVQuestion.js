'use strict';
module.exports = (sequelize, DataTypes) => {
    const CVQuestion = sequelize.define(
        'CVQuestion',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            index: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            title: {
                type: DataTypes.STRING,
                allowNull: false
            },
            info: {
                type: DataTypes.STRING,
                allowNull: false
            },
            bullets: {
                type: DataTypes.TEXT,
                allowNull: false,
                get() {
                    if (!this.getDataValue('bullets')) {
                        return [];
                    }

                    return JSON.parse(this.getDataValue('bullets'));
                },
                set(value) {
                    this.setDataValue('bullets', JSON.stringify(value));
                }
            },
            video_example: {
                type: DataTypes.UUID,
                allowNull: true,
                references: {
                    model: 'media',
                    key: 'id'
                }
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'cv_questions'
        }
    );

    CVQuestion.associate = function(models) {
        CVQuestion.hasMany(models.CVVideoAnswer, {
            as: 'answers',
            foreignKey: 'question_id'
        });

        CVQuestion.belongsTo(models.Media, {
            as: 'video',
            foreignKey: 'video_example'
        });
    };

    return CVQuestion;
};

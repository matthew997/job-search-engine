const bcrypt = require('bcryptjs');
const moment = require('moment');

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define(
        'User',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            first_name: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: {
                        args: [2, 32],
                        msg: 'First name must have a minimum of 2 characters'
                    }
                }
            },
            last_name: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    len: {
                        args: [2, 32],
                        msg: 'Last name must have a minimum of 2 characters'
                    }
                }
            },
            full_name: {
                type: DataTypes.VIRTUAL,
                get() {
                    return `${this.first_name} ${this.last_name}`;
                }
            },
            password: {
                type: DataTypes.STRING,
                allowNull: true
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
                validate: {
                    isEmail: {
                        msg: 'The given email address is not valid'
                    },
                    notNull: {
                        msg: 'Email is required'
                    }
                },
                unique: true
            },
            phone: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true
            },
            pin: {
                type: DataTypes.STRING,
                allowNull: true
            },
            birthday: {
                type: DataTypes.DATEONLY,
                allowNull: true
            },
            gender: {
                type: DataTypes.STRING,
                allowNull: true
            },
            marital_status: {
                type: DataTypes.STRING,
                allowNull: true
            },
            hometown: {
                type: DataTypes.STRING,
                allowNull: true
            },
            type: {
                type: DataTypes.STRING,
                defaultValue: 'user',
                allowNull: false
            },
            application_credits: {
                type: DataTypes.INTEGER,
                defaultValue: 3,
                allowNull: false
            },
            image_id: {
                type: DataTypes.UUID,
                allowNull: true,
                references: {
                    model: 'media',
                    key: 'id'
                }
            },
            cancellation_credits: {
                type: DataTypes.INTEGER,
                defaultValue: 2,
                allowNull: false
            },
            free_days: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
                allowNull: false
            },
            registration_completed: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
                allowNull: false
            },
            last_login: {
                allowNull: true,
                type: DataTypes.DATE
            },
            subscription: {
                allowNull: true,
                defaultValue: null,
                type: DataTypes.DATE
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            deleted_at: {
                allowNull: true,
                type: DataTypes.DATE
            }
        },
        {
            timestamps: true,
            underscored: true,
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            deletedAt: 'deleted_at',
            tableName: 'users',
            paranoid: true,
            defaultScope: {
                attributes: { exclude: ['password'] }
            },
            hooks: {
                beforeSave: (user, options) => {
                    if (options.fields.includes('password')) {
                        if (!user.password) {
                            return;
                        }

                        user.password = bcrypt.hashSync(user.password, 8);
                    }
                }
            }
        }
    );

    User.associate = models => {
        User.belongsTo(models.Media, {
            as: 'image',
            foreignKey: 'image_id'
        });

        User.belongsToMany(models.Role, {
            as: 'roles',
            through: 'role_user',
            foreignKey: 'user_id',
            otherKey: 'role_id'
        });

        User.hasOne(models.PasswordReset, {
            as: 'passwordReset',
            through: 'password_resets',
            foreignKey: 'user_id'
        });

        User.hasMany(models.CVVideoAnswer, {
            as: 'CVVideoAnswers',
            foreignKey: 'user_id'
        });

        User.hasMany(models.RoleUser, {
            as: 'roleUser',
            foreignKey: 'user_id'
        });

        User.hasMany(models.Company, {
            as: 'companies',
            foreignKey: 'user_id'
        });

        User.hasMany(models.Passport, {
            as: 'passports',
            foreignKey: 'user_id'
        });

        User.hasOne(models.UserDetail, {
            as: 'details',
            foreignKey: 'user_id'
        });

        User.hasOne(models.UserRegistration, {
            as: 'registration',
            foreignKey: 'user_id'
        });

        User.hasMany(models.Review, {
            as: 'reviews',
            foreignKey: 'user_id'
        });

        User.hasMany(models.Application, {
            as: 'application',
            foreignKey: 'user_id'
        });

        User.hasMany(models.CreditCard, {
            as: 'creditCards',
            foreignKey: 'user_id'
        });

        User.belongsToMany(models.DropdownOption, {
            as: 'languages',
            through: 'UserLanguage',
            foreignKey: 'user_id',
            otherKey: 'language_name',
            targetKey: 'value',
            scope: {
                parent: 'languages'
            }
        });

        User.hasMany(models.Change, {
            as: 'changes',
            foreignKey: 'user_id',
            scope: {
                type: 'User'
            }
        });

        User.hasMany(models.Payment, {
            as: 'payments',
            foreignKey: 'user_id'
        });
    };

    User.prototype.isAdmin = async function () {
        const roles = await this.getRoles();

        return roles.some(role => role.name.toLowerCase() === 'admin');
    };

    User.prototype.toJSON = function () {
        const attributes = Object.assign({}, this.get());

        delete attributes.password;

        return attributes;
    };

    User.prototype.extendSubscription = function (days = 30) {
        return this.update({
            subscription: moment().add(days, 'days')
        });
    };

    User.publishChanges = async (id, changes) => {
        await User.update(changes, {
            where: {
                id
            }
        });
    };

    User.TYPE_USER = 'user';
    User.TYPE_COMPANY = 'company';

    User.DISPLAYABLE_FIELDS = [
        'id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'birthday',
        'gender',
        'marital_status',
        'hometown',
        'application_credits',
        'cancellation_credits',
        'image_id',
        'type',
        'registration_completed',
        'last_login',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    User.TEST_PHONES = [
        '0503000400',
        '0503000888',
        '5551231230',
        '0503000881',
        '0503000882',
        '5551231231',
        '0551231232',
        '0555555555',
        '0512345670',
        '0512345671',
        '0512345672',
        '972548327131'
    ];

    User.EDITABLE_FIELDS = ['first_name', 'last_name', 'email', 'phone'];

    return User;
};

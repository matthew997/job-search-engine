const moment = require('moment');

module.exports = (sequelize, DataTypes) => {
    const PasswordReset = sequelize.define(
        'PasswordReset',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            user_id: {
                type: DataTypes.UUID
            },
            token: {
                type: DataTypes.STRING,
                allowNull: false
            },
            expires_at: {
                allowNull: false,
                type: DataTypes.DATE
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'password_resets'
        }
    );

    PasswordReset.associate = models => {
        PasswordReset.belongsTo(models.User, { as: 'user' });
    };

    PasswordReset.prototype.isExpired = function() {
        return moment(this.expires_at).isBefore(moment());
    };

    return PasswordReset;
};

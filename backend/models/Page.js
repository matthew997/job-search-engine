module.exports = (sequelize, DataTypes) => {
    const Page = sequelize.define(
        'Page',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            slug: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true
            },
            title: {
                type: DataTypes.STRING,
                allowNull: false
            },
            content: {
                type: DataTypes.TEXT,
                allowNull: false,
                get() {
                    if (!this.getDataValue('content')) {
                        return {};
                    }

                    return JSON.parse(this.getDataValue('content'));
                },
                set(value) {
                    this.setDataValue('content', JSON.stringify(value));
                }
            },
            meta: {
                type: DataTypes.TEXT,
                allowNull: true,
                get() {
                    if (!this.getDataValue('meta')) {
                        return {};
                    }

                    return JSON.parse(this.getDataValue('meta'));
                },
                set(value) {
                    this.setDataValue('meta', JSON.stringify(value));
                }
            },
            status: {
                type: DataTypes.STRING,
                allowNull: false,
                defaultValue: 'draft'
            },
            created_by: {
                type: DataTypes.UUID,
                allowNull: true,
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            deleted_at: {
                allowNull: true,
                type: DataTypes.DATE
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'pages'
        }
    );

    Page.associate = ({ User }) => {
        Page.belongsTo(User, {
            as: 'author',
            foreignKey: 'created_by'
        });
    };

    Page.STATUS_PUBLISHED = 'published';
    Page.STATUS_DRAFT = 'draft';
    Page.STATUS_DELETED = 'deleted';

    return Page;
};

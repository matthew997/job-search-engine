module.exports = (sequelize, DataTypes) => {
    const Dropdown = sequelize.define(
        'Dropdown',
        {
            name: {
                primaryKey: true,
                type: DataTypes.STRING,
                allowNull: false
            },
            display_name: {
                type: DataTypes.STRING,
                allowNull: false
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'dropdowns'
        }
    );

    Dropdown.associate = models => {
        Dropdown.hasMany(models.DropdownOption, {
            as: 'options',
            foreignKey: 'parent',
            otherKey: 'name'
        });
    };

    return Dropdown;
};

'use strict';
module.exports = (sequelize, DataTypes) => {
    const Content = sequelize.define(
        'Content',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            key: {
                type: DataTypes.STRING,
                allowNull: false
            },
            value: {
                type: DataTypes.TEXT,
                allowNull: false,
                get() {
                    if (!this.getDataValue('value')) {
                        return {};
                    }

                    return JSON.parse(this.getDataValue('value'));
                },
                set(value) {
                    this.setDataValue('value', JSON.stringify(value));
                }
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'contents'
        }
    );

    Content.associate = function(models) {};

    return Content;
};

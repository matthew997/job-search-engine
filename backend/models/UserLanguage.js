'use strict';
module.exports = (sequelize, DataTypes) => {
    const UserLanguage = sequelize.define(
        'UserLanguage',
        {
            user_id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            language_name: {
                primaryKey: true,
                type: DataTypes.STRING,
                allowNull: false
            },
            proficiency: {
                allowNull: false,
                type: DataTypes.STRING
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            deleted_at: {
                allowNull: true,
                type: DataTypes.DATE
            }
        },
        {
            timestamps: true,
            underscored: true,
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            deletedAt: 'deleted_at',
            paranoid: true,
            tableName: 'language_user'
        }
    );

    UserLanguage.associate = models => {
        UserLanguage.belongsTo(models.DropdownOption, {
            as: 'language',
            foreignKey: 'language_name',
            otherKey: 'value',
            targetKey: 'value'
        });

        UserLanguage.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'user_id'
        });
    };

    return UserLanguage;
};

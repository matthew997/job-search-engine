module.exports = (sequelize, DataTypes) => {
    const Application = sequelize.define(
        'Application',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            user_id: {
                type: DataTypes.UUID,
                allowNull: false,
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            company_id: {
                type: DataTypes.UUID,
                allowNull: false,
                references: {
                    model: 'company',
                    key: 'id'
                }
            },
            status: {
                type: DataTypes.STRING,
                allowNull: false
            },
            note: {
                type: DataTypes.STRING,
                allowNull: true
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            deleted_at: {
                allowNull: true,
                type: DataTypes.DATE
            },
            used_statuses: {
                type: DataTypes.TEXT,
                allowNull: true
            }
        },
        {
            timestamps: true,
            underscored: true,
            tableName: 'user_applications',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            deletedAt: 'deleted_at',
            paranoid: true
        }
    );

    Application.associate = models => {
        Application.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'user_id'
        });
        Application.belongsTo(models.Company, {
            as: 'company',
            foreignKey: 'company_id'
        });
    };

    Application.STATUS_APPROVED = 'approved';
    Application.STATUS_IN_PROGRESS = 'in_progress';
    Application.STATUS_PENDING = 'pending';
    Application.STATUS_REJECTED = 'rejected';
    Application.STATUS_APPLIED = 'applied';
    Application.STATUS_FOLLOW_UP = 'follow_up';

    return Application;
};

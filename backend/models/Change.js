'use strict';
module.exports = (sequelize, DataTypes) => {
    const Change = sequelize.define(
        'Change',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            company_id: {
                type: DataTypes.UUID,
                references: {
                    model: 'companies',
                    key: 'id'
                },
                allowNull: true
            },
            user_id: {
                type: DataTypes.UUID,
                references: {
                    model: 'users',
                    key: 'id'
                },
                allowNull: true
            },
            type: {
                type: DataTypes.STRING,
                allowNull: false
            },
            value: {
                type: DataTypes.TEXT,
                allowNull: false,
                get() {
                    if (!this.getDataValue('value')) {
                        return {};
                    }

                    return JSON.parse(this.getDataValue('value'));
                },
                set(value) {
                    this.setDataValue('value', JSON.stringify(value));
                }
            },
            status: {
                type: DataTypes.ENUM(['approved', 'rejected', 'pending']),
                allowNull: false,
                defaultValue: 'pending'
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'changes'
        }
    );

    Change.associate = models => {
        Change.belongsTo(models.Company, { as: 'company' });
        Change.belongsTo(models.User, { as: 'user' });
    };

    Change.STATUS_PENDING = 'pending';
    Change.STATUS_APPROVED = 'approved';
    Change.STATUS_REJECTED = 'rejected';

    return Change;
};

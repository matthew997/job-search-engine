module.exports = (sequelize, DataTypes) => {
    const DropdownOption = sequelize.define(
        'DropdownOption',
        {
            parent: {
                primaryKey: true,
                type: DataTypes.STRING,
                allowNull: false
            },
            value: {
                primaryKey: true,
                type: DataTypes.STRING,
                allowNull: true
            },
            text: {
                type: DataTypes.STRING,
                allowNull: false
            },
            order: {
                type: DataTypes.INTEGER,
                allowNull: false
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'dropdown_option'
        }
    );

    DropdownOption.associate = models => {
        DropdownOption.belongsTo(models.Dropdown, {
            as: 'dropdown',
            foreignKey: 'parent',
            otherKey: 'name'
        });
    };

    return DropdownOption;
};

'use strict';
module.exports = (sequelize, DataTypes) => {
    const Passport = sequelize.define(
        'Passport',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            user_id: {
                type: DataTypes.UUIDV4
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            issued_on: {
                allowNull: false,
                type: DataTypes.DATEONLY
            },
            expiration_date: {
                allowNull: false,
                type: DataTypes.DATEONLY
            },
            has_foreign_passport: {
                allowNull: true,
                type: DataTypes.BOOLEAN
            },
            country: {
                allowNull: true,
                type: DataTypes.STRING
            },
            has_american_visa: {
                allowNull: true,
                type: DataTypes.BOOLEAN
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            deleted_at: {
                allowNull: true,
                type: DataTypes.DATE,
                defaultValue: null
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'passports',
            paranoid: true
        }
    );

    Passport.associate = ({ User }) => {
        Passport.belongsTo(User, {
            as: 'user',
            foreignKey: 'user_id'
        });
    };

    return Passport;
};

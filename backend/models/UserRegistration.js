module.exports = (sequelize, DataTypes) => {
    const UserRegistration = sequelize.define(
        'UserRegistration',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            user_id: {
                type: DataTypes.UUID,
                allowNull: false,
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            first_step: {
                allowNull: false,
                type: DataTypes.BOOLEAN,
                defaultValue: false
            },
            second_step: {
                allowNull: false,
                type: DataTypes.BOOLEAN,
                defaultValue: false
            },
            third_step: {
                allowNull: false,
                type: DataTypes.BOOLEAN,
                defaultValue: false
            },
            company_accept_terms: {
                allowNull: false,
                type: DataTypes.BOOLEAN,
                defaultValue: false
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'user_registration'
        }
    );

    UserRegistration.associate = models => {
        UserRegistration.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'user_id'
        });
    };

    UserRegistration.prototype.hasCompletedAllSteps = function () {
        return this.first_step && this.second_step && this.third_step;
    };

    return UserRegistration;
};

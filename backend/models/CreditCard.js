module.exports = (sequelize, DataTypes) => {
    const CreditCard = sequelize.define(
        'CreditCard',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            user_id: {
                type: DataTypes.UUID,
                allowNull: false,
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            token: {
                type: DataTypes.STRING,
                allowNull: false
            },
            last_4_digits: {
                type: DataTypes.STRING,
                allowNull: false
            },
            exp: {
                type: DataTypes.STRING,
                allowNull: false
            },
            brand: {
                type: DataTypes.STRING,
                allowNull: true
            },
            default: {
                type: DataTypes.BOOLEAN,
                defaultValue: false
            },
            created_at: {
                allowNull: true,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: true,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            deleted_at: {
                allowNull: true,
                type: DataTypes.DATE
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'credit_cards'
        }
    );

    CreditCard.associate = models => {
        CreditCard.belongsTo(models.User, {
            as: 'user'
        });
    };

    return CreditCard;
};

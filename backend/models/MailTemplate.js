const db = require('../models');

module.exports = (sequelize, DataTypes) => {
    const MailTemplate = sequelize.define(
        'MailTemplate',
        {
            id: {
                primaryKey: true,
                autoIncrement: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            code: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true
            },
            description: {
                type: DataTypes.TEXT,
                allowNull: true
            },
            title: {
                type: DataTypes.STRING,
                allowNull: true
            },
            content: {
                type: DataTypes.TEXT,
                allowNull: true
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: sequelize.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            }
        },
        {
            paranoid: true,
            timestamps: false,
            underscored: true,
            tableName: 'mail_templates'
        }
    );

    MailTemplate.associate = models => { };

    return MailTemplate;
};

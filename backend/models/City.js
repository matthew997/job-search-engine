'use strict';
module.exports = (sequelize, DataTypes) => {
    const City = sequelize.define(
        'City',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            country_id: {
                type: DataTypes.UUID,
                allowNull: false,
                references: {
                    model: 'countries',
                    key: 'id'
                }
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'cities'
        }
    );

    City.associate = function (models) {
        City.belongsTo(models.Country, {
            as: 'country',
            foreignKey: 'country_id'
        });

        City.hasMany(models.Company, {
            as: 'companies',
            foreignKey: 'country_id'
        });

        City.hasMany(models.Review, {
            as: 'reviews',
            foreignKey: 'city'
        });
    };

    return City;
};

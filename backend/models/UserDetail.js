'use strict';
module.exports = (sequelize, DataTypes) => {
    const UserDetail = sequelize.define(
        'UserDetail',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            user_id: {
                type: DataTypes.UUID,
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            education: {
                type: DataTypes.STRING,
                allowNull: false
            },
            experience: {
                allowNull: true,
                type: DataTypes.STRING
            },
            experience_job_type: {
                allowNull: true,
                defaultValue: null,
                type: DataTypes.STRING
            },
            service: {
                allowNull: true,
                type: DataTypes.STRING
            },
            availability: {
                allowNull: false,
                type: DataTypes.STRING
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            deleted_at: {
                allowNull: true,
                type: DataTypes.DATE,
                defaultValue: null
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'user_details'
        }
    );

    UserDetail.associate = function(models) {};

    return UserDetail;
};

'use strict';

module.exports = (sequelize, DataTypes) => {
    const CVVideoAnswer = sequelize.define(
        'CVVideoAnswer',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            user_id: {
                type: DataTypes.UUID,
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            question_id: {
                type: DataTypes.UUID,
                references: {
                    model: 'cv_questions',
                    key: 'id'
                }
            },
            video: {
                type: DataTypes.STRING,
                allowNull: false
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'cv_video_answers'
        }
    );

    CVVideoAnswer.associate = function(models) {
        CVVideoAnswer.belongsTo(models.CVQuestion, {
            as: 'question'
        });

        CVVideoAnswer.belongsTo(models.User, {
            as: 'user'
        });
    };

    return CVVideoAnswer;
};

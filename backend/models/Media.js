'use strict';
module.exports = (sequelize, DataTypes) => {
    const Media = sequelize.define(
        'Media',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            user_id: {
                type: DataTypes.UUID,
                allowNull: false
            },
            type: {
                type: DataTypes.STRING,
                allowNull: false
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            alt: {
                type: DataTypes.STRING,
                allowNull: false
            },
            filename: {
                type: DataTypes.STRING,
                allowNull: false
            },
            extension: {
                type: DataTypes.STRING,
                allowNull: false
            },
            meta: {
                type: DataTypes.TEXT,
                allowNull: true,
                get() {
                    if (!this.getDataValue('meta')) {
                        return {};
                    }
                    return JSON.parse(this.getDataValue('meta'));
                },
                set(value) {
                    this.setDataValue('meta', JSON.stringify(value));
                }
            },
            date: {
                type: DataTypes.STRING,
                allowNull: false
            },
            size: {
                type: DataTypes.STRING,
                allowNull: false
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: true,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            stream: {
                allowNull: true,
                defaultValue: null,
                type: DataTypes.STRING
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'media'
        }
    );

    Media.associate = function (models) {
        Media.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'user_id'
        });
    };

    return Media;
};

('use strict');

module.exports = (sequelize, DataTypes) => {
    const Setting = sequelize.define(
        'Setting',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            title: {
                type: DataTypes.STRING,
                allowNull: false,
                defaultValue: ''
            },
            tag: {
                type: DataTypes.STRING,
                allowNull: false,
                defaultValue: 'other'
            },
            key: {
                type: DataTypes.STRING,
                allowNull: false
            },
            value: {
                type: DataTypes.STRING,
                allowNull: true
            },
            type: {
                type: DataTypes.STRING,
                allowNull: false,
                defaultValue: 'string'
            },
            order: {
                type: DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 1
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'settings'
        }
    );

    Setting.PUBLIC_KEYS = [
        'privacy_policy',
        'terms_of_use',
        'google_tag_manager'
    ];

    return Setting;
};

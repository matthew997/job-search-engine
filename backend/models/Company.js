module.exports = (sequelize, DataTypes) => {
    const Company = sequelize.define(
        'Company',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            user_id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            type_id: {
                type: DataTypes.UUID,
                allowNull: false,
                primaryKey: true,
                references: {
                    model: 'job_types',
                    key: 'id'
                }
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            country_id: {
                type: DataTypes.UUID,
                allowNull: false,
                references: {
                    model: 'countries',
                    key: 'id'
                }
            },
            city_id: {
                type: DataTypes.UUID,
                allowNull: false,
                references: {
                    model: 'cities',
                    key: 'id'
                }
            },
            company_logo: {
                type: DataTypes.UUID,
                allowNull: false,
                primaryKey: true,
                references: {
                    model: 'media',
                    key: 'id'
                }
            },
            promote_video: {
                type: DataTypes.STRING,
                allowNull: false
            },
            facebook_link: {
                type: DataTypes.STRING,
                allowNull: false
            },
            instagram_link: {
                type: DataTypes.STRING,
                allowNull: true
            },
            promote_image: {
                type: DataTypes.UUID,
                allowNull: false,
                primaryKey: true,
                references: {
                    model: 'media',
                    key: 'id'
                }
            },
            about: {
                type: DataTypes.STRING(1000),
                allowNull: false
            },
            working_environment: {
                type: DataTypes.STRING(1000),
                allowNull: false
            },
            office_nature: {
                type: DataTypes.STRING(1000),
                allowNull: false
            },
            rating: {
                type: DataTypes.DECIMAL(4, 2),
                allowNull: true
            },
            number_of_reviews: {
                type: DataTypes.INTEGER,
                defaultValue: 0
            },
            is_on_homepage: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            free_leads: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
                allowNull: false
            },
            active: {
                type: DataTypes.BOOLEAN,
                defaultValue: false
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            deleted_at: {
                allowNull: true,
                type: DataTypes.DATE
            }
        },
        {
            timestamps: true,
            underscored: true,
            tableName: 'companies',
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            deletedAt: 'deleted_at',
            paranoid: true
        }
    );

    Company.associate = models => {
        Company.belongsTo(models.User, { as: 'user', foreignKey: 'user_id' });

        Company.belongsTo(models.Media, {
            as: 'promoteImage',
            foreignKey: 'promote_image'
        });

        Company.belongsTo(models.Media, {
            as: 'companyLogo',
            foreignKey: 'company_logo'
        });

        Company.belongsTo(models.Country, {
            as: 'country',
            foreignKey: 'country_id'
        });

        Company.belongsTo(models.City, {
            as: 'city',
            foreignKey: 'city_id'
        });

        Company.hasOne(models.CompanyLeadsFilter, {
            as: 'filter',
            foreignKey: 'company_id'
        });

        Company.hasMany(models.Application, { as: 'applications' });

        Company.hasMany(models.Review, { as: 'reviews' });

        Company.hasMany(models.Change, {
            as: 'changes',
            foreignKey: 'company_id',
            scope: {
                type: 'Company'
            }
        });

        Company.belongsTo(models.JobType, {
            as: 'jobType',
            foreignKey: 'type_id'
        });
    };

    Company.publishChanges = (companyId, changes) => {
        return Company.update(changes, {
            where: {
                id: companyId
            }
        });
    };

    Company.EDITABLE_FIELDS = [
        'name',
        'country_id',
        'city_id',
        'company_logo',
        'promote_video',
        'promote_image',
        'facebook_link',
        'instagram_link',
        'about',
        'working_environment',
        'office_nature'
    ];

    return Company;
};

'use strict';
module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define(
        'Role',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW
            }
        },
        {
            timestamps: false,
            underscored: true,
            tableName: 'roles'
        }
    );

    Role.associate = function(models) {
        Role.belongsToMany(models.User, {
            as: 'users',
            through: 'role_user',
            foreignKey: 'role_id',
            otherKey: 'user_id'
        });
    };

    Role.ADMIN = 'admin';
    Role.USER = 'user';
    Role.DISPLAYABLE_FIELDS = ['id', 'name', 'created_at', 'updated_at'];

    return Role;
};

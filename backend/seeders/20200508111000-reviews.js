'use strict';

const ReviewFactory = require('../factories/Review');
const UserRepository = require('../repositories/User');
const CompanyRepository = require('../repositories/Company');
const { sequelize } = require('../models');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const reviews = [];

        const users = await UserRepository.findAll();

        for (const user of users) {
            const companies = await CompanyRepository.random(3, {
                attributes: [
                    [sequelize.fn('DISTINCT', sequelize.col('id')), 'companyId']
                ]
            });

            for (const company of companies) {
                const review = await ReviewFactory.make(
                    {
                        company_id: company.dataValues.companyId,
                        user_id: user.id
                    },
                    { raw: true }
                );

                reviews.push(review);
            }
        }

        return queryInterface.bulkInsert('reviews', reviews, {});
    },

    down: (queryInterface, Sequelize) => {}
};

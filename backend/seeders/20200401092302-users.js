'use strict';

const bcrypt = require('bcryptjs');
const uuid = require('uuid/v4');
const moment = require('moment');

const CreditCardRepository = require('../repositories/CreditCard');
const UserRepository = require('../repositories/User');
const PassportRepository = require('../repositories/Passport');
const RoleRepository = require('../repositories/Role');
const UserRegistrationRepository = require('../repositories/UserRegistration');
const { Role, User } = require('../models');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const USER_PHONE = '0512345670';
        const ADMIN_PHONE = '0512345671';
        const COMPANY_PHONE = '0512345672';

        const users = [
            {
                id: uuid(),
                first_name: 'admin',
                last_name: 'admin123',
                email: 'admin@chut.test',
                phone: ADMIN_PHONE,
                birthday: moment('1970-01-01').toDate(),
                gender: 'male',
                marital_status: 'single',
                hometown: 'ABC',
                pin: '1111',
                pin_generated_at: new Date(),
                password: bcrypt.hashSync('test'),
                type: User.TYPE_USER,
                registration_completed: true,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                id: uuid(),
                first_name: 'user',
                last_name: 'user123',
                email: 'user@chut.test',
                phone: USER_PHONE,
                birthday: moment('1970-01-01').toDate(),
                gender: 'male',
                marital_status: 'single',
                hometown: 'ABC',
                pin: '1111',
                pin_generated_at: new Date(),
                password: bcrypt.hashSync('test'),
                type: User.TYPE_USER,
                registration_completed: true,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                id: uuid(),
                first_name: 'company',
                last_name: 'company123',
                email: 'company@chut.test',
                phone: COMPANY_PHONE,
                birthday: moment('1970-01-01').toDate(),
                gender: 'male',
                marital_status: 'single',
                hometown: 'ABC',
                pin: '1111',
                pin_generated_at: new Date(),
                password: bcrypt.hashSync('test'),
                type: User.TYPE_COMPANY,
                registration_completed: true,
                created_at: new Date(),
                updated_at: new Date()
            }
        ];

        for (let i = 0; i < 3; i++) {
            users.push({
                id: uuid(),
                first_name: `userTest${i}`,
                last_name: `userTest${i}`,
                email: `userTest${i}@chut.test`,
                phone: `050300088${i}`,
                birthday: moment('1970-01-01').toDate(),
                gender: 'male',
                marital_status: 'single',
                hometown: 'ABC',
                pin: '1111',
                pin_generated_at: new Date(),
                password: bcrypt.hashSync('test'),
                type: User.TYPE_USER,
                registration_completed: true,
                created_at: new Date(),
                updated_at: new Date(),
                last_login: moment('2002-01-01').toDate()
            });
        }

        await queryInterface.bulkInsert('users', users, {});

        const admin = await UserRepository.findOne({
            where: {
                first_name: 'admin'
            }
        });

        const adminRole = await RoleRepository.findOne({
            where: {
                name: Role.ADMIN
            }
        });

        const user = await UserRepository.findOne({
            where: {
                first_name: 'user'
            }
        });

        const company = await UserRepository.findOne({
            where: {
                first_name: 'company'
            }
        });

        const userRole = await RoleRepository.findOne({
            where: {
                name: Role.USER
            }
        });

        const userTest0 = await UserRepository.findOne({
            where: {
                first_name: 'userTest0'
            }
        });

        const userTest1 = await UserRepository.findOne({
            where: {
                first_name: 'userTest1'
            }
        });

        const userTest2 = await UserRepository.findOne({
            where: {
                first_name: 'userTest2'
            }
        });

        await admin.setRoles([adminRole]);
        await user.setRoles([userRole]);
        await company.setRoles([userRole]);
        await userTest0.setRoles([userRole]);
        await userTest1.setRoles([userRole]);
        await userTest2.setRoles([userRole]);

        await PassportRepository.create({
            user_id: user.id,
            name: 'user user123',
            issued_on: '1970-01-01',
            expiration_date: '1980-01-01',
            has_foreign_passport: true,
            country: null,
            has_american_visa: true
        });

        await PassportRepository.create({
            user_id: user.id,
            name: 'user user123',
            issued_on: '1970-01-01',
            expiration_date: '1980-01-01',
            has_foreign_passport: true,
            country: 'USA',
            has_american_visa: true
        });

        await PassportRepository.create({
            user_id: userTest0.id,
            name: 'userTest0',
            issued_on: '1970-01-01',
            expiration_date: '1980-01-01',
            has_foreign_passport: true,
            country: 'POLAND',
            has_american_visa: true
        });

        await PassportRepository.create({
            user_id: userTest0.id,
            name: 'userTest0',
            issued_on: '1970-01-01',
            expiration_date: '1980-01-01',
            has_foreign_passport: true,
            country: 'VATICAN',
            has_american_visa: false
        });

        await CreditCardRepository.create({
            user_id: company.id,
            token: '123abc',
            last_4_digits: '1234',
            exp: '02/22',
            brand: 'visa',
            default: true
        });

        await CreditCardRepository.create({
            user_id: userTest0.id,
            token: '123abc',
            last_4_digits: '1234',
            exp: '02/22',
            brand: 'visa',
            default: true
        });

        await CreditCardRepository.create({
            user_id: userTest1.id,
            token: '123abc',
            last_4_digits: '1234',
            exp: '02/22',
            brand: 'visa',
            default: true
        });

        await CreditCardRepository.create({
            user_id: userTest2.id,
            token: '123abc',
            last_4_digits: '1234',
            exp: '02/22',
            brand: 'visa',
            default: true
        });

        await UserRegistrationRepository.create({
            user_id: user.id,
            first_step: true,
            second_step: true,
            third_step: true
        });

        await UserRegistrationRepository.create({
            user_id: company.id,
            first_step: true,
            second_step: true,
            third_step: true
        });

        return;
    },

    down: (queryInterface, Sequelize) => {}
};

'use strict';

const UserRepository = require('../repositories/User');
const CVQuestionRepository = require('../repositories/CVQuestion');

const uuid = require('uuid/v4');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const answers = [];
        const video = '1a41337e596d127d6809014b9dd1187b';

        const users = await UserRepository.findAll({
            type: 'user'
        });

        const questions = await CVQuestionRepository.findAll();

        for (const question of questions) {
            for (const user of users) {
                const answer = {
                    video,
                    id: uuid(),
                    user_id: user.id,
                    question_id: question.id,
                    created_at: new Date(),
                    updated_at: new Date()
                };

                answers.push(answer);
            }
        }

        return queryInterface.bulkInsert('cv_video_answers', answers, {});
    },

    down: (queryInterface, Sequelize) => {}
};

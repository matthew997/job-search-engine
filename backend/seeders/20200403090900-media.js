'use strict';
const UserRepository = require('../repositories/User');
const uuid = require('uuid/v4');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const user = await UserRepository.findOne();

        const media = [
            {
                id: uuid(),
                user_id: user.id,
                type: 'image',
                name: 'icon_briefcase_business',
                alt: 'briefcase',
                filename: 'b22cde75bc64265ca732c6b3',
                extension: 'svg',
                meta: JSON.stringify({
                    breakpoints: [],
                    width: 80,
                    height: 66
                }),
                date: 'seeders',
                size: '1044',
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                id: '4aab715a-3961-4b0b-a4df-6384231fdf9c',
                user_id: user.id,
                type: 'video',
                name: 'big-buck-bunny_trailer.webm',
                alt: 'big-buck-bunny_trailer.webm',
                filename: 'e53b9e3b256da7189894a9d1',
                extension: 'webm',
                meta: '{}',
                date: 'seeders',
                size: '2165175',
                created_at: new Date(),
                updated_at: new Date()
            }
        ];

        return queryInterface.bulkInsert('media', media, {});
    },

    down: (queryInterface, Sequelize) => {}
};

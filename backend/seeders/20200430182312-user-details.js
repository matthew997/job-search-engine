'use strict';

const UserRepository = require('../repositories/User');
const JobTypeRepository = require('../repositories/JobType');
const uuid = require('uuid/v4');
const { Op } = require('sequelize');
const faker = require('faker');
const { CompanyLeadsFilter } = require('../models');
const DropdownOptionRepository = require('../repositories/DropdownOption');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const users = await UserRepository.findAll({
            where: {
                email: {
                    [Op.like]: 'user%'
                }
            }
        });

        const jobType = await JobTypeRepository.random();
        const usersDetails = [];
        const experience = ['7-12m', '1-3y', '3y', '1-6m'];
        const availability = [
            CompanyLeadsFilter.AVAILABILITY_IMMEDIATE,
            CompanyLeadsFilter.AVAILABILITY_ONE_TO_THREE_MONTHS,
            CompanyLeadsFilter.AVAILABILITY_THREE_TO_SIX_MONTHS
        ];

        for (const user of users) {
            const [
                { value: education },
                { value: service }
            ] = await Promise.all([
                DropdownOptionRepository.getRandomOptionFromParent('education'),
                DropdownOptionRepository.getRandomOptionFromParent('service')
            ]);

            usersDetails.push({
                id: uuid(),
                user_id: user.id,
                education: education,
                experience: faker.random.arrayElement(experience),
                experience_job_type: jobType.id,
                service: service,
                availability: faker.random.arrayElement(availability),
                created_at: new Date(),
                updated_at: new Date()
            });
        }

        return queryInterface.bulkInsert('user_details', usersDetails, {});
    },

    down: (queryInterface, Sequelize) => {}
};

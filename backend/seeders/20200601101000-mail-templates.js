'use strict';

const uuid = require('uuid/v4');

module.exports = {
    up: async (queryInterface, Sequelize) => {

        return queryInterface.bulkInsert(
            'mail_templates',
            [
                {
                    id: uuid(),
                    code: 'CONTACT',
                    name: 'Contact email',
                    description:
                        'Email sent after contact form is submitted',
                    title: 'Contact message: {{title}}',
                    content: `<h1>{{fullName}} submitted the contact form.</h1>                 
                        <h2>Title: {{title}}</h2>
                        <p>Message: {{message}}</p>`,
                    created_at: new Date(),
                    updated_at: new Date()
                },
                {
                    id: uuid(),
                    code: 'NEWSLETTER',
                    name: 'Newsletter email',
                    description:
                        'Email sent after newsletter is submitted',
                    title: 'Newsletter!',
                    content: `<h1>New newsletter signup</h1>
                        <p>{{email}} has signed up to the newsletter</p>`,
                    created_at: new Date(),
                    updated_at: new Date()
                },
                {
                    id: uuid(),
                    code: 'APPLICATION_CANDIDATE',
                    name: 'Application email to the candidate',
                    description: ' Email sent to the candidate after application is submitted',
                    title: 'Application submitted',
                    content: `<h1>You submitted the application to {{companyName}} company</h1>`,
                    created_at: new Date(),
                    updated_at: new Date()
                },
                {
                    id: uuid(),
                    code: 'NEW_LEAD',
                    name: 'Application email to the company',
                    description: ' Email sent when business gets a new lead',
                    title: 'Application submitted',
                    content: `<h1>{{fullName}} submitted the application to your company: {{companyName}} company</h1>`,
                    created_at: new Date(),
                    updated_at: new Date()
                },
                {
                    id: uuid(),
                    code: 'APPLIED_24H',
                    name: 'Applied 24h status',
                    description: 'Email sent when a lead is in applied status for 24h',
                    title: 'Candidate applied 24h ago',
                    content: `<h1>Candidate {{fullName}} applied 24h ago to your company: {{companyName}}</h1>`,
                    created_at: new Date(),
                    updated_at: new Date()
                },
                {
                    id: uuid(),
                    code: 'CANDIDATE_STATUS_CHANGE',
                    name: 'Candidate status changed',
                    description: 'Email sent to the candidate when the status change occurs',
                    title: 'Status changed',
                    content: `<h1>Your application to {{companyName}} company is now {{status}}</h1>`,
                    created_at: new Date(),
                    updated_at: new Date()
                },
                {
                    id: uuid(),
                    code: 'CHANGE_STATUS',
                    name: "Change's status updated",
                    description: "Email sent when a change's status has changed",
                    title: "New change's status",
                    content: `<h1>Changes made for  {{name}} have a new status: {{status}}</h1>`,
                    created_at: new Date(),
                    updated_at: new Date()
                },
                {
                    id: uuid(),
                    code: 'INVOICE',
                    name: 'New invoice',
                    description: 'Email sent when a new invoice is ready',
                    title: 'New invoice',
                    content: 'New invoice is ready',
                    created_at: new Date(),
                    updated_at: new Date()
                },
                {
                    id: uuid(),
                    code: 'RECRUITER_INACTIVE',
                    name: "Recruiter is inactive",
                    description: "Email sent when a recruiter is set to inactive",
                    title: "Recruiter is inactive",
                    content: `<h1>Recruiter is now inactive</h1>`,
                    created_at: new Date(),
                    updated_at: new Date()
                }
            ],
            {}
        );
    },

    down: (queryInterface, Sequelize) => { }
};

'use strict';
const uuid = require('uuid/v4');
module.exports = {
    up: (queryInterface, Sequelize) => {
        const bullets = [
            'קווים מנחים לדבר עליהם קווים',
            'קווים מנחים לדבר עליהם קווים',
            'קווים מנחים לדבר עליהם קווים'
        ];
        const exampleQuestions = [];

        for (let i = 1; i <= 6; i++) {
            exampleQuestions.push({
                id: uuid(),
                index: i,
                title: `שאלה מספר ${i} - ספר/י לי קצת על עצמך`,
                info:
                    'קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט',
                bullets: JSON.stringify(bullets),
                video_example: '4aab715a-3961-4b0b-a4df-6384231fdf9c',
                created_at: new Date(),
                updated_at: new Date()
            });
        }
        return queryInterface.bulkInsert('cv_questions', exampleQuestions);
    },
    down: (queryInterface, Sequelize) => {}
};

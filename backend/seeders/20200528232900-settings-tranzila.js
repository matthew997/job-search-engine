'use strict';

const uuid = require('uuid/v4');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const settings = [
            {
                key: 'tranzila_terminal',
                value: '',
                tag: 'tranzila',
                title: 'Tranzila terminal (username)',
                type: 'string',
                order: 1
            },
            {
                key: 'tranzila_password',
                value: '',
                tag: 'tranzila',
                title: 'Tranzila token password',
                type: 'string',
                order: 2
            },
            {
                key: 'tranzila_transaction_password',
                value: '',
                tag: 'tranzila',
                title: 'TranzilaPW (transaction password) ',
                type: 'string',
                order: 3
            }
        ];

        for (const setting of settings) {
            setting.id = uuid();
            setting.created_at = new Date();
            setting.updated_at = new Date();
        }

        return queryInterface.bulkInsert('settings', settings, {});
    },

    down: (queryInterface, Sequelize) => {}
};

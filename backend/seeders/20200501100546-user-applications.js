'use strict';

const uuid = require('uuid/v4');
const faker = require('faker');

const UserRepository = require('../repositories/User');
const CompanyRepository = require('../repositories/Company');
const { Application } = require('../models');
const { Op } = require('sequelize');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const user = await UserRepository.findOneByEmail('user@chut.test');

        const users = await UserRepository.findAll({
            where: {
                email: {
                    [Op.in]: [
                        'userTest0@chut.test',
                        'userTest1@chut.test',
                        'userTest2@chut.test'
                    ]
                }
            }
        });
        const companies = await CompanyRepository.findAll();

        const statuses = [
            Application.STATUS_APPROVED,
            Application.STATUS_IN_PROGRESS,
            Application.STATUS_PENDING,
            Application.STATUS_REJECTED,
            Application.STATUS_APPLIED,
            Application.STATUS_FOLLOW_UP
        ];

        const applications = [];

        for (const [index, status] of statuses.entries()) {
            applications.push({
                status,
                id: uuid(),
                company_id: companies[index].id,
                note: faker.lorem.sentences(3),
                user_id: user.id,
                created_at: new Date(),
                updated_at: new Date()
            });
        }

        for (let y = 0; y < 3; y++) {
            for (let i = 0; i < companies.length; i++) {
                applications.push({
                    status: faker.random.arrayElement(statuses),
                    id: uuid(),
                    company_id: companies[i].id,
                    note: faker.lorem.sentences(3),
                    user_id: faker.random.arrayElement(users).id,
                    created_at: new Date(),
                    updated_at: new Date()
                });
            }
        }

        return queryInterface.bulkInsert('user_applications', applications, {});
    },

    down: (queryInterface, Sequelize) => {}
};

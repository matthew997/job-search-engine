'use strict';

const faker = require('faker');

const CountryRepository = require('../repositories/Country');

module.exports = {
    up: (queryInterface, Sequelize) => {
        const options = {
            include: [
                {
                    association: 'cities'
                }
            ]
        };

        return Promise.all([
            CountryRepository.create(
                {
                    name: 'USA',
                    cities: generateCities(10)
                },
                options
            ),
            CountryRepository.create(
                {
                    name: 'Germany',
                    cities: generateCities(3, 'de')
                },
                options
            )
        ]);
    },

    down: (queryInterface, Sequelize) => {}
};

const generateCities = (count = 5, locale = 'en_US') => {
    const cities = [];

    faker.locale = locale;

    for (let i = 1; i <= count; i++) {
        cities.push({
            name: faker.address.city()
        });
    }

    return cities;
};

'use strict';

const uuid = require('uuid/v4');

module.exports = {
    up: (queryInterface, Sequelize) => {
        const jobTypes = [];
        const list = {
            sales: 'מכירות‌',
            service: 'שירות‌',
            construction: 'בניה‌'
        };

        for (const [slug, name] of Object.entries(list)) {
            jobTypes.push({
                name,
                slug,
                id: uuid(),
                created_at: new Date(),
                updated_at: new Date(),
            });
        }

        return queryInterface.bulkInsert('job_types', jobTypes, {});
    },

    down: (queryInterface, Sequelize) => {}
};

'use strict';

const uuid = require('uuid/v4');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const pages = [
            {
                slug: 'test-1-page',
                title: 'Test 1 page',
                content: JSON.stringify([
                    { key: 'header', value: 'Test 1 page' }
                ]),
                meta: JSON.stringify({ title: 'Test 1 page' }),
                status: 'published'
            },
            {
                slug: 'test-2-page',
                title: 'Test 2 page',
                content: JSON.stringify([
                    { key: 'header', value: 'Test 2 page' }
                ]),
                meta: JSON.stringify({ title: 'Test 2 page' }),
                status: 'published'
            }
        ];

        for (const page of pages) {
            page.id = uuid();
            page.created_at = new Date();
            page.updated_at = new Date();
        }

        return queryInterface.bulkInsert('pages', pages, {});
    },

    down: (queryInterface, Sequelize) => {}
};

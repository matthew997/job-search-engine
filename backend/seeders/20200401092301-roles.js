'use strict';

const uuid = require('uuid/v4');

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert(
            'roles',
            [
                {
                    id: uuid(),
                    name: 'admin',
                    created_at: new Date(),
                    updated_at: new Date()
                },
                {
                    id: uuid(),
                    name: 'user',
                    created_at: new Date(),
                    updated_at: new Date()
                }
            ],
            {}
        );
    },

    down: (queryInterface, Sequelize) => {}
};

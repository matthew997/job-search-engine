'use strict';

const DropdownRepository = require('../repositories/Dropdown');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const promises = [];
        const dropdowns = {
            marital_status: 'Marital status',
            education: 'Education',
            service: 'Service',
            application_status: 'Application Status',
            languages: 'Languages',
            language_proficiency: 'Language proficiency'
        };

        const dropdownOptions = {
            marital_status: [
                {
                    text: 'רווק',
                    value: 'single'
                },
                {
                    text: 'נשוי',
                    value: 'married'
                },
                {
                    text: 'גרוש',
                    value: 'divorcee'
                },
                {
                    text: 'אלמן',
                    value: 'widower'
                }
            ],
            education: [
                {
                    text: 'תיכונית‌',
                    value: 'high_school'
                },
                {
                    text: 'תואר‌ ‌ראשון‌',
                    value: 'first_degree'
                },
                {
                    text: 'קורס‌ ‌מקצועי‌',
                    value: 'course'
                }
            ],
            service: [
                {
                    text: 'שירות‌ ‌צבאי‌ ‌מלא‌',
                    value: 'military'
                },
                {
                    text: 'שירות‌ ‌לאומי‌',
                    value: 'social'
                },
                {
                    text: 'אף‌ ‌אחד‌ ‌מהם‌',
                    value: 'other'
                }
            ],
            application_status: [
                {
                    text: 'אושר‌',
                    value: 'approved'
                },
                {
                    text: 'בתהליך‌ ‌מיון‌',
                    value: 'in_progress'
                },
                {
                    text: 'ממתין‌',
                    value: 'pending'
                },
                {
                    text: 'נדחה‌',
                    value: 'rejected'
                },
                {
                    text: 'מועמדות‌ ‌הוגשה‌',
                    value: 'applied'
                },
                {
                    text: 'במעקב‌',
                    value: 'follow_up'
                }
            ],
            languages: [
                {
                    text: 'עברית',
                    value: 'hebrew'
                },
                {
                    text: 'אנגלית',
                    value: 'english'
                },
                {
                    text: 'רוסית',
                    value: 'russian'
                },
                {
                    text: 'ערבית',
                    value: 'arabic'
                },
                {
                    text: 'צרפתית',
                    value: 'french'
                },
                {
                    text: 'ספרדית',
                    value: 'spanish'
                }
            ],
            language_proficiency: [
                {
                    text: 'רמה גבוהה',
                    value: 'advanced'
                },
                {
                    text: 'יכול לנהל שיחה',
                    value: 'conversational'
                },
                {
                    text: 'שפת אם',
                    value: 'native'
                }
            ]
        };

        for (const [name, displayName] of Object.entries(dropdowns)) {
            const options = dropdownOptions[name].map((option, index) => ({
                ...option,
                order: index + 1
            }));

            const promise = DropdownRepository.create(
                {
                    name,
                    options,
                    display_name: displayName
                },
                {
                    include: [
                        {
                            association: 'options'
                        }
                    ]
                }
            );

            promises.push(promise);
        }

        return Promise.all(promises);
    },

    down: (queryInterface, Sequelize) => {}
};

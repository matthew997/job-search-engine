'use strict';

const uuid = require('uuid/v4');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const settings = [
            {
                key: 'email_from_name',
                value: 'Chutzlaaretz',
                tag: 'email',
                title: 'What name should be displayed on the emails received from the site?',
                type: 'string',
                order: 2
            },
            {
                key: 'email_from_address',
                value: 'email@chutzlaaretz.com',
                tag: 'email',
                title: 'What should be the address that user sees when receiving emails from the site?',
                type: 'string',
                order: 3
            }
        ];

        for (const setting of settings) {
            setting.id = uuid();
            setting.created_at = new Date();
            setting.updated_at = new Date();
        }

        return queryInterface.bulkInsert('settings', settings, {});
    },

    down: (queryInterface, Sequelize) => { }
};

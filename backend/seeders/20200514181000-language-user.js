'use strict';

const UserRepository = require('../repositories/User');
const DropdownOptionRepository = require('../repositories/DropdownOption');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const users = await UserRepository.findAll({
            type: 'user'
        });

        const entries = [];

        for (const user of users) {
            const [
                { value: language_name },
                { value: proficiency }
            ] = await Promise.all([
                DropdownOptionRepository.getRandomOptionFromParent('languages'),
                DropdownOptionRepository.getRandomOptionFromParent(
                    'language_proficiency'
                )
            ]);

            const entry = {
                proficiency,
                language_name,
                user_id: user.id
            };

            entries.push(entry);
        }

        return queryInterface.bulkInsert('language_user', entries, {});
    },

    down: (queryInterface, Sequelize) => {}
};

'use strict';

const uuid = require('uuid/v4');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const components = {
            'header-component': {
                logo: {
                    id: uuid(),
                    type: 'image',
                    name: 'logo',
                    filename: '7b624cb2bf2b60f9aafa89a1',
                    extension: 'png',
                    meta: { breakpoints: [], width: 149, height: 36 },
                    date: 'seeders',
                    size: '3034'
                },
                menuItems: [
                    {
                        text: 'עבודות בחו״ל',
                        link: '/jobs'
                    },
                    {
                        text: 'אודות',
                        link: '/about'
                    },
                    {
                        text: 'צור קשר',
                        link: '/contact'
                    }
                ],
                phoneNumber: '03-1234567'
            },

            'footer-component': {
                sublogoText:
                    ' לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית הועניב היושבב שערש שמחויט',
                phoneNumber: '03-1234567',
                terms: 'אני מאשר/ת בזו קבלת עדכונים ומידע מהאתר ״חוצלארץ״',
                newsletter:
                    'תמיד טוב להיות עם הדרכון על הדופק. הישארו מעודכנים בהצעות עבודה חדשות! מבטיחים לא לשלוח דואר זבל :)',
                links: {
                    socialmedia: {
                        first: '/',
                        second: '/',
                        third: '/'
                    },
                    colFirst: [
                        {
                            link: '/',
                            text: 'עבודות בחו״ל'
                        },
                        {
                            link: '/',
                            text: 'אודות'
                        },
                        {
                            link: '/',
                            text: 'יצירת קשר'
                        },
                        {
                            link: '/',
                            text: 'לינק לדוגמה'
                        },
                        {
                            link: '/',
                            text: 'לינק לדוגמה'
                        },
                        {
                            link: '/',
                            text: 'לינק לדוגמה'
                        }
                    ],
                    colSecond: [
                        {
                            link: '/',
                            text: 'עבודות בחו״ל'
                        },
                        {
                            link: '/',
                            text: 'אודות'
                        },
                        {
                            link: '/',
                            text: 'יצירת קשר'
                        },
                        {
                            link: '/',
                            text: 'לינק לדוגמה'
                        },
                        {
                            link: '/',
                            text: 'לינק לדוגמה'
                        },
                        {
                            link: '/',
                            text: 'לינק לדוגמה'
                        }
                    ]
                },
                logo: {
                    id: uuid(),
                    type: 'image',
                    name: 'logo',
                    filename: 'dc1b6d33111109d5eb91b6c3',
                    extension: 'png',
                    meta: { breakpoints: [], width: 149, height: 36 },
                    date: 'seeders',
                    size: '3034'
                },
                socialmedia: {
                    logoFirst: {
                        id: uuid(),
                        type: 'image',
                        name: 'socialogoFirst',
                        filename: 'd65aca167e69cd90a4c5aed1',
                        extension: 'svg',
                        meta: { breakpoints: [], width: 149, height: 36 },
                        date: 'seeders',
                        size: '3034'
                    },
                    logoSecond: {
                        id: uuid(),
                        type: 'image',
                        name: 'socialogoSecond',
                        filename: 'b1ccf49e60eae3d0c16a7eec',
                        extension: 'svg',
                        meta: { breakpoints: [], width: 149, height: 36 },
                        date: 'seeders',
                        size: '3034'
                    },
                    logoThird: {
                        id: uuid(),
                        type: 'image',
                        name: 'socialogoThird',
                        filename: '7853939d7c2ad6c988c9719f',
                        extension: 'svg',
                        meta: { breakpoints: [], width: 149, height: 36 },
                        date: 'seeders',
                        size: '3034'
                    }
                }
            }
        };

        const pages = {
            'contact-us-page': {
                subtitle: 'יש לך שאלה או בקשה ? פנה אלינו נשמח לעזור'
            },
            'about-us-page': {
                headline:
                    "Lorem Ipsum Dolor Sit Emmett, Elit Colouris Adipiscing Consultant Monfred Edandom Silkoff, exciting and exciting. Amihlif Goller Monferr Subert Lorem in Yehok Dough, to shrink in Gak Litz's city, and whose hearts were sacked. Lorem Ipsum Dolor Sit Emmett, Elit Colouris Adipiscing Consultant Monfred Edandom Silkoff, exciting and exciting. Amihlif Goller Monferr Subert Lorem in Yehok Dough, to shrink in Gak Litz's city, and whose hearts were sacke",
                columnLeft:
                    "Lorem Ipsum Dolor Sit Emmett, Elit Colouris Adipiscing Consultant Monfred Edandom Silkoff, exciting and exciting. Amihlif Goller Monferr Subert Lorem in Yehok Dough, to shrink in Gak Litz's city, and whose hearts were sacked. Lorem Ipsum Dolor Sit Emmett, Elit Colouris Adipiscing Consultant Monfred Edandom Silkoff, exciting and exciting. Amihlif Goller Monferr Subert Lorem in Yehok Dough, to shrink in Gak Litz's city, and whose hearts were sacked",
                columnRight:
                    "Lorem Ipsum Dolor Sit Emmett, Elit Colouris Adipiscing Consultant Monfred Edandom Silkoff, exciting and exciting. Amihlif Goller Monferr Subert Lorem in Yehok Dough, to shrink in Gak Litz's city, and whose hearts were sacked. Lorem Ipsum Dolor Sit Emmett, Elit Colouris Adipiscing Consultant Monfred Edandom Silkoff, exciting and exciting. Amihlif Goller Monferr Subert Lorem in Yehok Dough, to shrink in Gak Litz's city, and whose hearts were sacke",
                promotionalImages: {
                    left1: {
                        meta: {
                            breakpoints: [640, 480, 320],
                            width: 661,
                            height: 661
                        },
                        id: '42ff855d-909e-4950-a879-2ad146605c93',
                        name: 'about-left-1.jpg',
                        alt: 'about-left-1.jpg',
                        filename: '17468d2cb7a23d4b818040f5',
                        extension: 'jpg',
                        type: 'image',
                        size: 458781,
                        date: 'seeders'
                    },
                    right1: {
                        meta: {
                            breakpoints: [480, 320],
                            width: 522,
                            height: 522
                        },
                        id: 'cc5b8640-432c-4287-9187-5da4d590bf0b',
                        name: 'about-right-1.jpg',
                        alt: 'about-right-1.jpg',
                        filename: '4950c0a6d36276fb32e68180',
                        extension: 'jpg',
                        type: 'image',
                        size: 287790,
                        date: 'seeders'
                    },
                    right2: {
                        meta: {
                            breakpoints: [320],
                            width: 343,
                            height: 343
                        },
                        id: 'bce2a803-304f-4b63-94ca-7f4b0622863a',
                        name: 'about-right-2.jpg',
                        alt: 'about-right-2.jpg',
                        filename: '9b1c0722c5a537a0c2c2df5c',
                        extension: 'jpg',
                        type: 'image',
                        size: 137216,
                        date: 'seeders'
                    },
                    center1: {
                        meta: {
                            breakpoints: [],
                            width: 219,
                            height: 219
                        },
                        id: 'a23d9c41-8415-4166-ab05-53c150fa68a6',
                        name: 'about-center-1.jpg',
                        alt: 'about-center-1.jpg',
                        filename: 'be1e9f16621d4e31769ece6c',
                        extension: 'jpg',
                        type: 'image',
                        size: 76498,
                        date: 'seeders'
                    },
                    center2: {
                        meta: {
                            breakpoints: [320],
                            width: 384,
                            height: 384
                        },
                        id: '7e54e44b-466f-4126-ba37-9081193ad9a4',
                        name: 'about-center-2.jpg',
                        alt: 'about-center-2.jpg',
                        filename: '4f0bdd0aacbbffa82213e0c5',
                        extension: 'jpg',
                        type: 'image',
                        size: 144330,
                        date: 'seeders'
                    },
                    center3: {
                        meta: {
                            breakpoints: [320],
                            width: 404,
                            height: 404
                        },
                        id: '7d791b24-d16d-4e4d-82e1-82aea3d80964',
                        name: 'about-center-3.jpg',
                        alt: 'about-center-3.jpg',
                        filename: 'ba88281dcfe5933246758b1c',
                        extension: 'jpg',
                        type: 'image',
                        size: 152412,
                        date: 'seeders'
                    }
                },
                banner: {
                    image: {
                        meta: {
                            breakpoints: [1200, 640, 480, 320],
                            width: 1440,
                            height: 301
                        },
                        id: 'b16132e6-9b5d-4195-86b2-4ed94ab3bf21',
                        name: 'about-banner.jpg',
                        alt: 'about-banner.jpg',
                        filename: 'afb8d8cae7bcf3abc2464092',
                        extension: 'jpg',
                        type: 'image',
                        size: '318907',
                        date: 'seeders'
                    },
                    text: 'בואו גם להנות מחוויה עולמית וגם',
                    textBold: 'להרוויח כמו שצריך!'
                }
            },
            'home-page': {
                mainBanner: {
                    image: {
                        id: uuid(),
                        type: 'image',
                        name: 'main_banner',
                        filename: '0eb49c268e01508016c5af16',
                        extension: 'jpg',
                        meta: { breakpoints: [], width: 1918, height: 370 },
                        date: 'seeders',
                        size: '171737'
                    },
                    headline: 'גם מטיילים וגם מרוויחים!',
                    subheadline: 'עבודות לישראלים בחו״ל',
                    button: {
                        link: '/',
                        text: 'בואו נטוס!'
                    },
                    videoButton: {
                        link: '/',
                        text: 'גלו עוד התהליך'
                    }
                },

                aboutProcess: {
                    headline: 'טוב, מאיפה מתחילים?',
                    subheadline: 'עשינו את הכל סופר פשוט',
                    button: {
                        link: '/',
                        text: 'בואו נטוס!'
                    },
                    items: [
                        {
                            image: {
                                id: uuid(),
                                type: 'image',
                                name: 'icon_subscribers',
                                filename: '4b246327777c2cb8e321a0d9',
                                extension: 'svg',
                                meta: {
                                    breakpoints: [],
                                    width: 80,
                                    height: 70
                                },
                                date: 'seeders',
                                size: '1541'
                            },
                            headline: 'נרשמים',
                            text:
                                'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק'
                        },
                        {
                            image: {
                                id: uuid(),
                                type: 'image',
                                name: 'icon_interactions_interact',
                                filename: '74daecd0d425dd719e853a15',
                                extension: 'svg',
                                meta: {
                                    breakpoints: [],
                                    width: 80,
                                    height: 84
                                },
                                date: 'seeders',
                                size: '1243'
                            },
                            headline: 'עונים על שאלון',
                            text:
                                'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק'
                        },
                        {
                            image: {
                                id: uuid(),
                                type: 'image',
                                name: 'icon_briefcase_business',
                                filename: 'b22cde75bc64265ca732c6b3',
                                extension: 'svg',
                                meta: {
                                    breakpoints: [],
                                    width: 80,
                                    height: 66
                                },
                                date: 'seeders',
                                size: '1044'
                            },
                            headline: 'בוחרים את סוג העבודה',
                            text:
                                'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק'
                        },
                        {
                            image: {
                                id: uuid(),
                                type: 'image',
                                name: 'icon_travel',
                                filename: 'fe44696e1e3d927d28464ce7',
                                extension: 'svg',
                                meta: {
                                    breakpoints: [],
                                    width: 99,
                                    height: 77
                                },
                                date: 'seeders',
                                size: '2954'
                            },
                            headline: 'וטסים להרוויח!',
                            text:
                                'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק'
                        }
                    ]
                },
                hotJobs: {
                    headline: 'העבודות החמות מסביב לעולם',
                    noJobsText:
                        '!ככל הנראה הם עדיין עושים חיים, נעלה לכאן חוויות בהמשך'
                },
                whoWeAre: {
                    headline: 'מי אנחנו?',
                    text: `
                            <p>
                                קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק
                                סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס
                                קוויס, אקווזמן קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי
                                סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם
                                ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט
                            </p>
                            <p>
                                קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף מוסן מנת. להאמית קרהשק
                                סכעיט דז מא, מנכם למטכין נשואי מנורך. נולום ארווס סאפיאן - פוסיליס
                                קוויס, אקווזמן קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי
                                סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם
                                ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט
                            </p>
                        `,
                    video: {
                        source: 'https://www.youtube.com/watch?v=Bey4XXJAqS8'
                    }
                },
                customerReviews: {
                    headline: 'כמה מילים מהחברים שלנו שעושים חיים בעולם',
                    reviews: [
                        {
                            image: {
                                id: uuid(),
                                type: 'image',
                                name: 'icon_interactions_interact',
                                filename: '74daecd0d425dd719e853a15',
                                extension: 'svg',
                                meta: {
                                    breakpoints: [],
                                    width: 80,
                                    height: 84
                                },
                                date: 'seeders',
                                size: '1243'
                            },
                            name: 'שירי אטיאס',
                            location: 'ארה״ב - סן פרנסיסקו',
                            text:
                                'שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית הועניב היושבב שערש שמחויט'
                        },
                        {
                            image: {
                                id: uuid(),
                                type: 'image',
                                name: 'icon_interactions_interact',
                                filename: '74daecd0d425dd719e853a15',
                                extension: 'svg',
                                meta: {
                                    breakpoints: [],
                                    width: 80,
                                    height: 84
                                },
                                date: 'seeders',
                                size: '1243'
                            },
                            name: 'שירי אטיאס',
                            location: 'ארה״ב - סן פרנסיסקו',
                            text:
                                'שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית הועניב היושבב שערש שמחויט'
                        },
                        {
                            image: {
                                id: uuid(),
                                type: 'image',
                                name: 'icon_interactions_interact',
                                filename: '74daecd0d425dd719e853a15',
                                extension: 'svg',
                                meta: {
                                    breakpoints: [],
                                    width: 80,
                                    height: 84
                                },
                                date: 'seeders',
                                size: '1243'
                            },
                            name: 'שירי אטיאס',
                            location: 'ארה״ב - סן פרנסיסקו',
                            text:
                                'שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית הועניב היושבב שערש שמחויט'
                        },
                        {
                            image: {
                                id: uuid(),
                                type: 'image',
                                name: 'icon_interactions_interact',
                                filename: '74daecd0d425dd719e853a15',
                                extension: 'svg',
                                meta: {
                                    breakpoints: [],
                                    width: 80,
                                    height: 84
                                },
                                date: 'seeders',
                                size: '1243'
                            },
                            name: 'שירי אטיאס',
                            location: 'ארה״ב - סן פרנסיסקו',
                            text:
                                'שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית הועניב היושבב שערש שמחויט'
                        }
                    ]
                }
            },
            'customer-sign-page': {
                signInTab: {
                    step1: {
                        contentText:
                            'על מנת להתחבר למערכת, הכניסו מספר טלפון והזינו את קוד האימות שקיבלתם בהודעת SMS',
                        termsAndConditionsText:
                            'אני מאשר/ת שקראתי ואני מסכים/ה למדיניות הפרטיות ותנאי השימוש',
                        errorText: 'המספר שהזנת לא נמצא במערכת',
                        phoneNumberAttemptInfo: {
                            text: 'אבל הי כנראה זה הזמן להצטרף למשפחה',
                            ctaText: 'להרשמה',
                            ctaLink: '/link-to-nowhere'
                        },
                        submitButtonText: 'המשך'
                    },
                    step2: {
                        contentText: 'שלחנו לך קוד זיהוי SMS',
                        explanationText: 'הכניסו את קוד הזיהוי להמשך',
                        resendMessage: 'שלח שוב',
                        errorText: 'נראה שמספר הזיהוי שהקשת לא תואם',
                        submitButtonText: 'המשך'
                    }
                },
                signUpTab: {
                    contentText:
                        'בכדי להרשם למערכת חוצלארץ יש לעבור שאלון קצר שכולל צילום וידאו לאחר מכן תוכלו להגיש קורות חיים למשרות שוות בכל העולם',
                    buttonText: 'קדימה בואו נתחיל'
                }
            },
            'signup-page': {
                headline: 'הרשמה - חיפוש עבודה בחו״ל',
                personalData: {
                    termsConditions:
                        'אני מאשר/ת שקראתי ואני מסכים/ה למדיניות הפרטיות ותנאי השימוש'
                },
                authentication: {
                    headline: 'הזינו את קוד האימות שקילבתם בהודעת SMS',
                    sendAgainText: 'לא קיבלת קוד? ',
                    sendAgainLink: 'חזור למילוי מספר טלפון'
                },
                goldUser: {
                    text:
                        'היי! בגלל שיש לך ניסיון בתחום, רצינו להציע לך להיות משתמש זהב. מה זה אומר? זה אומר שתוכל/י לתת חוות דעת על המקומות בהם עבדת וככה לעזור למצטרפים חדשים! במקרה ואת/ה לא מעוניין/ת - יש ללחוץ ״להמשך הרשמה״',
                    icon: {
                        meta: {
                            breakpoints: [],
                            width: 107,
                            height: 107
                        },
                        id: uuid(),
                        type: 'image',
                        name: 'info_image.svg',
                        alt: 'info_image.svg',
                        filename: 'a6fa7d857c4dd7317ebb1fde',
                        extension: 'svg',
                        date: 'seeders',
                        size: '1593'
                    }
                }
            },
            'video-cv-done-page': {
                title: 'הרשמה - חיפוש עבודה בחו״ל',
                heading: 'סיימנו!',
                description:
                    'קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט'
            },
            'business-registration-finished-page': {
                icon: {
                    meta: {
                        breakpoints: [],
                        width: 59,
                        height: 74
                    },
                    id: uuid(),
                    type: 'image',
                    name: 'registration_done.svg',
                    alt: 'registration_done.svg',
                    filename: '8a2043690dc034251836dcea',
                    extension: 'svg',
                    date: 'seeders',
                    size: '4843'
                },
                title: 'הרשמה - חשבון עסקי',
                heading: 'סיימנו!',
                description:
                    'קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט'
            },
            'business-registration-page': {
                personalInformation: {
                    informationText: ' מספר זה ישמש כאמצעי כניסה לאתר'
                },
                smsVerification: {
                    text: 'הזינו את קוד האימות שקילבתם בהודעת SMS'
                }
            },
            'edit-company-leads-filter-page': {
                text: 'Lorem ipsum...'
            },
            'personal-zone-page': {
                upperText:
                    'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. הועניב לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. הועניב',
                videoTabUpperText:
                    'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. הועניב לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. הועניב'
            },
            'applications-page': {
                videoLink: 'https://www.youtube.com/watch?v=Bey4XXJAqS8',
                text:
                    'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. הועניב לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. הועניב'
            },
            'payment-page': {
                headline: 'הסבר על שיטת התמחור של חוצלארץ',
                text:
                    'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. הועניב לורם',
                monthlyPrice: 99,
                pricePerLead: 77
            },
            'privacy-policy-page': {
                title: 'Privacy policy',
                content: `
                <p>This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>
                <p>What personal information do we collect from the people that visit our blog, website or app?<br>When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, mailing address, phone number, credit card information, foot pain or other details to help you with your experience.</p>
                <h3>When do we collect information?</h3>
                <p>We collect information from you when you place an order or enter information on our site.</p>
                <h3>How do we use your information?</h3>
                <p>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>
                <p>Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.</p>
                <p>We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.</p>
                <p>All transactions are processed through a gateway provider and are not stored or processed on our servers.</p>
                <h3>Do we use 'cookies'?</h3>
                <p>Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart.<br> They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>
                <p>We use cookies to:</p>
                <ul>
                    <li>Help remember and process the items in the shopping cart.</li>
                    <li>Understand and save user's preferences for future visits.</li>
                    <li>Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future.</li>
                </ul>
                <p>We may also use trusted third-party services that track this information on our behalf.</p>
                <p>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies</p>
                <p>If users disable cookies in their browser:</p>
                <p>If you turn cookies off, Some of the features that make your site experience more efficient may not function properly.Some of the features that make your site experience more efficient and may not function properly.</p>
                <h3>Third-party disclosure</h3>
                <p>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information.</p>
                <h3>Third-party links</h3>
                <p>We do not include or offer third-party products or services on our website.</p>
                <h3>Google</h3>
                <p>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users.</p>
                <p>
                    <a
                        href="https://support.google.com/adwordspolicy/answer/1316548?hl=en"
                        target="_blank">
                        https://support.google.com/adwordspolicy/answer/1316548?hl=en
                    </a>
                </p>
                <p>We have not enabled Google AdSense on our site but we may do so in the future.</p>
                <h3>California Online Privacy Protection Act</h3>
                <p>
                    CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being shared. - See more at:<br>
                    <a
                        href="http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf"
                        target="_blank">
                        http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf
                    </a>
                </p>
                <p>According to CalOPPA, we agree to the following: Users can visit our site anonymously. Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website. Our Privacy Policy link includes the word 'Privacy' and can easily be found on the page specified above.</p>
                <p>You will be notified of any Privacy Policy changes:</p>
                <ul>
                    <li>On our Privacy Policy Page Can change your personal information:</li>
                    <li>By logging into your account</li>
                </ul>
                <h3>How does our site handle Do Not Track signals?</h3>
                <p>We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.</p>
                <h3>COPPA (Children Online Privacy Protection Act)</h3>
                <p>When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.</p>
                <p>We do not specifically market to children under the age of 13 years old. Do we let third-parties, including ad networks or plug-ins collect PII from children under 13?</p>
                <h3>Fair Information Practices</h3>
                <p>The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.</p>
                <p>We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.</p>
                <h3>CAN SPAM Act</h3>
                <p>The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.</p>
                <p>We collect your email address in order to:</p>
                <p>To be in accordance with CANSPAM, we agree to the following:</p>
                <p>If at any time you would like to unsubscribe from receiving future emails, you can email us at and we will promptly remove you from ALL correspondence.</p>
                <h3>Contacting Us</h3>
                <p>If there are any questions regarding this privacy policy, you may contact us using the information below.</p>
                <p>CHUTZLARETZ LLC</p>
                <p>260 Madison Avenue, Suite 204</p>
                <p>NY, NY 10016</p>
                <p>USA</p>
                <p>Email: <a href="mailto:customercare@CHUTZLARETZ.com">customercare@CHUTZLARETZ.com</a></p>
                <p>Tel: <a href="tel:8009454770">(800) 9454770</a></p>
            `
            },
            'terms-of-use-page': {
                title: 'Terms of use',
                content: `<h3>GENERAL</h3>
                <p>Access to and use of this Website and the products and services available through this Website (collectively, the "Services") are subject to the following terms, conditions and notices (the "Terms and Conditions "). By using the Services, you are agreeing to all of the Terms and Conditions, as may be updated by us regularly.</p>
                <p>You should check this page regularly to take notice of any changes we may have made to the Terms and Conditions . Access to this website is permitted on a temporary basis, and Chutzluretz Ltd reserves the right to remove or amend the Services without notice. We will not be liable if for any reason this Website is unavailable at any time or for any period. Periodically we may restrict access to some parts or the entire website. Your use of the website is contingent upon your acceptance of our Privacy Policy and the following Terms and Conditions. If you do not agree to the Terms and Conditions of business contained in the following pages, then you may not use the Chutzluretz website.</p>
                <p>If you have any inquiries, comments or concerns regarding this agreement or any other part of this site or regarding any of our featured products and services or if you have experienced technical problems while using this site, please send an email to customercare@Chutzluretz.com.</p>
                <h3>PRIVACY POLICY</h3>
                <p>Our privacy policy, which sets out how we will use your information, can be found at Privacy Policy. By using this website, you consent to the processing described therein and warrant that all data provided by you is accurate.</p>
                <h3>COOKIES</h3>
                <p>Like many websites, we also use "cookie" technology. A cookie is a small data file that we transfer to your computer's hard disk. We do not use cookies to collect personally identifiable information. We use cookies to better understand how you interact with the site and our service, to monitor aggregate usage by our users and web traffic routing on the site, and to improve the site and our services.</p>
                <h3>YOUR OBLIGATIONS AND CONDUCT</h3>
                <p>You must provide us with true and accurate information for example: your real name, phone number, e-mail address and other requested information as indicated. Falsifying or omitting contact information such as a name, address, and/or telephone number when registering with Chutzluretz is not permitted. You are also not permitted to use fax or disconnected numbers as a telephone number. When using this website you must obey all applicable international, federal, state, and local laws.</p>
                <p>It is your responsibility to select a password that is unique to your Chutzluretz account and that you do not disclose this information to anyone else or keep this information in an insecure place.</p>
                <p>You must not misuse this Website. You will not: commit or encourage a criminal offence; transmit or distribute a virus, trojan, worm, logic bomb or post any other material which is malicious, technologically harmful, in breach of confidence or in any way offensive or obscene; hack into any aspect of the Services; corrupt data; cause annoyance to other users; infringe upon the rights of any other person's proprietary rights; send any unsolicited advertising or promotional material, commonly referred to as "spam"; or attempt to affect the performance or functionality of any computer facilities of or accessed through this Website. Breaching this provision would constitute a criminal offence under the Computer Misuse Act 1990. Chutzluretz will report any such breach to the relevant law enforcement authorities and disclose your identity to them.</p>
                <p>We will not be liable for any loss or damage caused by a distributed denial-of-service attack, viruses or other technologically harmful material that may infect your computer equipment, computer programs, data or other proprietary material due to your use of this Website or to your downloading of any material posted on it, or on any website linked to it.</p>
                <p>We reserve the right to bar users from this Site on a permanent or temporary basis. Any such user shall be notified and must not then attempt to use this Site under any other name or through any other user. Chutzluretz will report any breach of these rules that would constitute a criminal offence under the Computer Misuse Act of 1990 to the relevant law enforcement agencies and disclose your identity to them.</p>
                <h3>INTELLECTUAL PROPERTY, SOFTWARE AND CONTENT</h3>
                <p>This website is owned by and operated by Chutzluretz Ltd. Unless otherwise noted in this website, Chutzluretz Ltd. owns the copyright with respect to all content on the website. Content includes: text, graphics, logos, audio clips, trademarks, software server information, and anything else hosted on this website. The Chutzluretz logo, name, tagline and other marks indicated on the website are the trademarks or registered trademarks of Chutzluretz in the US, Canada, Japan, China, Australia and Europe. All rights to content, services, and server information are reserved. Any modification made to the content of this website by a third party is a violation of Chutzluretz’s copyright. Additionally, the Chutzluretz website may contain other proprietary notices and copyright information, the terms of which must be observed and followed.</p>
                <p>The website graphics, logos, page headers, button icons, scripts and service names are the trademarks or trade dress of Chutzluretz. Chutzluretz’s trademarks and trade dress may not be used in connection with any product or service that is not Chutzluretz’s, in any manner that is likely to cause confusion among customers or in any manner that disparages or discredits Chutzluretz. All other trademarks not owned by Chutzluretz that appear on this website are the property of their respective owners, who may or may not be affiliated with, connected to, or sponsored by Chutzluretz.</p>
                <p>Nothing contained on the website should be construed as granting, by implication, estoppel, or otherwise, any license or right to use the website or any information displayed on the website, through the use of framing or otherwise, except: (a) as expressly permitted by these Terms and Conditions of Business; or (b) with the prior written permission of Chutzluretz or the prior written permission from such third party that may own the trademark or copyright of information displayed on the website.</p>
                <p>The intellectual property rights in all software and content made available to you on or through this Website remains the property of Chutzluretz or its licensors and are protected by copyright laws and treaties around the world. All such rights are reserved by Chutzluretz and its licensors. You may store, print and display the content supplied solely for your own personal use. You are not permitted to publish, manipulate, distribute or otherwise reproduce, in any format, any of the content or copies of the content supplied to you or which appears on this Website nor may you use any such content in connection with any business or commercial enterprise. You may link Chutzluretz content to other sites, but only if you acknowledge the website as the source of the material and does not disparage or discredit Chutzluretz.</p>
                <p>You shall not modify, translate, reverse engineer, decompile, disassemble or create derivative works based on any software or accompanying documentation supplied by Chutzluretz or its licensors.</p>
                <h3>TERMS OF SALE</h3>
                <p>By placing an order on Chutzluretz website you are offering to purchase a product on and subject to the following terms and conditions. All orders are subject to availability and confirmation of the order price. At any time Chutzluretz has the right to cancel the order before it is dispatched, refunding any money due to the customer.</p>
                <p>Chutzluretz will endeavour to process the Order and send the Goods within the time period stated at point of purchase Any dates quoted for delivery of the Goods are indicative only. Time for delivery will not be of the essence of the Contract and Chutzluretz will not be liable for any loss or expenses sustained by the Buyer arising from any delay in the delivery of the Goods howsoever caused.. We will not be held responsible for any deliveries delayed due to postal delays or force majeure.</p>
                <p>In order to contract with Chutzluretz you must be at least 18 years of age and can form a legally binding contract under applicable law If your order is accepted we will inform you by email when we dispatch the item. When placing an order you undertake that all details you provide to us are true and accurate, that you are an authorised user of the credit, debit card or PayPal account used to place your order and that there are sufficient funds to cover the order cost. We reserve the right to obtain validation of your credit card, debit card or PayPal details before accepting your Order. All prices advertised are subject to such change.</p>
                <h3>PRICING AND PAYMENT</h3>
                <p>At Chutzluretz we try to ensure that all details, descriptions and prices that appear on this website are accurate. In the unlikely event of an error in the price of any goods that you have ordered we will inform you of this as soon as possible and give you the option of reconfirming your order at the correct price or cancelling it. If we are unable to contact you we will treat the order as cancelled and any monies paid will be refunded to you.</p>
                <p>Taxes, Duties and Extra Charges: Prices advertised on the Site may not include shipping and handling charges or applicable country or region-specific sales, goods or services taxes, which may be added to the price you pay. You are also responsible for any customs duties or any other similar fees associated with your Order.</p>
                <p>Upon receiving your order we carry out a standard pre-authorisation check on your payment card to ensure that there are sufficient funds to fulfil the transaction. Goods will not be dispatched until this pre-authorisation check has been completed. Your card will be debited once the order has been accepted.</p>
                <p>We may use your personal information to conduct appropriate anti-fraud checks.</p>
                <p>Personal Information that you provide may be disclosed to a credit reference or fraud prevention agency, which may keep a record of that information.</p>
                <h3>SHIPPING</h3>
                <p>Products will generally be shipped to you within approximately 12 days of the date receiving impression box from the customer and confirming impression was taken as instructed.</p>
                <h3>NO MEDICAL ADVISE</h3>
                <p>You acknowledge and agree that Chutzluretz does not provide any form of medical care, medical opinion, medical advice, diagnosis or treatment, and that Chutzluretz does not evaluate whether or not you should seek medical attention, through the Chutzluretz Site. The contents of the Chutzluretz Site, such as articles, graphics, images, information from our third-party contributors, Product descriptions and instructions, and other material contained on the Site are for informational purposes only. Although such Content may be provided by individuals in the medical profession, the provision of such Content does not create a doctor-patient relationship, and does not constitute a medical opinion, medical advice, or diagnosis or treatment of any particular condition. The Content is not intended to be a substitute for professional medical advice, diagnosis, or treatment. Always seek the advice of your physician or other qualified healthcare provider with any questions you may have regarding a medical condition.</p>
                <h3>RETURNS AND REFUNDS</h3>
                <p>If you are unsatisfied with your purchase, you may return it to us for a refund within 100 days. In order to return the product you should contact our customer service (see under F&Q Section on the support page on site).</p>
                <h3>DISCLAIMER OF LIABILITY</h3>
                <p>The information contained in this website is for general information purposes only. The information is provided by Chutzluretz and while we endeavour to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.</p>
                <p>In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website.</p>
                <p>We will not be liable for any loss or damage caused by a distributed denial-of-service attack, viruses or other technologically harmful material that may infect your computer equipment, computer programs, data or other proprietary material due to your use of this Website or to your downloading of any material posted on it, or on any website linked to it.</p>
                <p>Information on the Chutzluretz website may contain technical inaccuracies or typographical errors. Chutzluretz attempts to make its descriptions as accurate as possible, but does not warrant that the content of the Chutzluretz website is accurate, complete, reliable, current, or error-free.</p>
                <h3>INDEMNITY</h3>
                <p>You agree fully to indemnify, defend and hold us, and our directors, employees, agents and suppliers, harmless immediately on demand, from and against all claims, liability, damages, losses, costs and expenses, including reasonable legal fees, arising out of any breach of the Conditions by you or any other liabilities arising out of your use of this Website, or the use by any other person accessing the Website using your shopping account and/or your Personal Information.</p>
                <h3>VARIATION</h3>
                <p>Chutzluretz shall have the right in its absolute discretion at any time and without notice to amend, remove or vary any of its services or any page of this Website.</p>
                <h3>INVALIDITY</h3>
                <p>If any part of the Terms and Conditions is unenforceable (including any provision in which we exclude our liability to you) the enforceability of any other part of the Terms and Conditions will not be affected with all other clauses remaining in full force and effect. So far as possible where any clause/sub-clause or part of a clause/sub-clause can be severed to render the remaining part valid, the clause shall be interpreted accordingly. Alternatively, you agree that the clause shall be rectified and interpreted in such a way that closely resembles the original meaning of the clause /sub-clause as is permitted by law.</p>
                <h3>COMPLAINTS</h3>
                <p>We will try to do everything we can to resolve any disputes when they first arise. If you do have any complaints or comments then please contact us at customercare@Chutzluretz.com</p>
                <h3>WAIVER</h3>
                <p>If you breach these conditions and we take no action, we will be entitled to exercise our rights in any other situation where you are in breach of our terms and conditions.</p>
                <h3>ENTIRE AGREEMENT</h3>
                <p>These Terms and Conditions constitute the entire agreement of the parties and supersede any and all preceding and contemporaneous agreements between you and Chutzluretz. Any waiver of any provision of the Terms and Conditions will be effective only if in writing and signed by a Director of Chutzluretz.</p>
                <p>We comply with US legal and tax regulations.</p>
                <p>Chutzluretz LLC</p>
                <p>380 Lexington Avenue #1700</p>
                <p>New York, NY 10168</p>
                <p>USA</p>`
            }
        };

        const elements = [components, pages];
        const contents = getContents(elements);

        return queryInterface.bulkInsert('contents', contents, {});
    },

    down: (queryInterface, Sequelize) => {}
};

const getContents = elements => {
    const contents = [];

    for (const element of elements) {
        for (const [key, value] of Object.entries(element)) {
            const id = uuid();
            const content = {
                id,
                key,
                value: JSON.stringify(value),
                created_at: new Date(),
                updated_at: new Date()
            };

            contents.push(content);
        }
    }

    return contents;
};

'use strict';

const uuid = require('uuid/v4');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const settings = [
            {
                key: 'receiving_email',
                value: 'email@chutzlaaretz.com',
                tag: 'email',
                title:
                    'Which email should receive contact and newsletter submissions from the site',
                type: 'string',
                order: 1
            }
        ];

        for (const setting of settings) {
            setting.id = uuid();
            setting.created_at = new Date();
            setting.updated_at = new Date();
        }

        return queryInterface.bulkInsert('settings', settings, {});
    },

    down: (queryInterface, Sequelize) => {}
};

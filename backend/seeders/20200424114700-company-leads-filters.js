'use strict';

const CompanyRepository = require('../repositories/Company');
const CompanyLeadsFilterFactory = require('../factories/CompanyLeadsFilter');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const filters = [];
        const companies = await CompanyRepository.findAll();

        for (const company of companies) {
            const filter = await CompanyLeadsFilterFactory.make({ company_id: company.id }, { raw: true });

            filters.push(filter);
        }

        return queryInterface.bulkInsert('company_leads_filters', filters, {});
    },

    down: (queryInterface, Sequelize) => {}
};

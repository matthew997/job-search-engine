'use strict';

const CompanyFactory = require('../factories/Company');
const UserRepository = require('../repositories/User');
const JobTypeRepository = require('../repositories/JobType');
const MediaRepository = require('../repositories/Media');
const CountryRepository = require('../repositories/Country');
const CityRepository = require('../repositories/City');

const { User } = require('../models');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const companies = [];
        const users = await UserRepository.findAll({
            where: {
                type: User.TYPE_COMPANY
            }
        });
        const types = await JobTypeRepository.findAll();
        const media = await MediaRepository.findOne({
            where : {
                type: 'image'
            }
        });

        for (const user of users) {
            for (const type of types) {
                for (let i = 1; i <= 5; i++) {
                    const country = await CountryRepository.random();
                    const city = await CityRepository.random(1, {
                        where: {
                            country_id: country.id
                        }
                    });

                    const company = await CompanyFactory.make(
                        {
                            user_id: user.id,
                            type_id: type.id,
                            country_id: country.id,
                            city_id: city.id,
                            promote_image: media.id,
                            company_logo: media.id
                        },
                        { raw: true }
                    );

                    companies.push(company);
                }
            }
        }

        return queryInterface.bulkInsert('companies', companies, {});
    },

    down: (queryInterface, Sequelize) => {}
};

# Chutzlaaretz BACKEND

## Node JS Express

### Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

-   Node - v11.15.0
-   NPM - v6.7.0
-   Docker - v19.03.5 (optional)

### Installing

From terminal

#### config setup

```
cp .env.example .env
```

#### install dependencies

npm install

#### docker setup

```
docker-compose up --build -d
```

#### database setup

Configure the database in config/index.js

```
NODE_ENV=development npm run db-drop
NODE_ENV=development npm run db-create (if the database hasn't already been created)
NODE_ENV=development npm run migrate
NODE_ENV=development npm run seed
```

## Production

#### install dependences

```
npm install
```

#### nginx configuration
```
server {
    listen 80;

    client_max_body_size 64M;
    server_name chutzlaaretz-api.local;

    error_log /var/log/nginx/api-chutzlaaretz-local.error.log;
    access_log /var/log/nginx/api-chutzlaaretz-local.access.log;

    proxy_buffers 4 256k;
    proxy_buffer_size 128k;
    proxy_busy_buffers_size 256k;

    location / {
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-NginX-Proxy true;
        proxy_set_header X-Forwarded-Proto https;
        proxy_pass http://127.0.0.1:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }

     location ~* \.(?:ico|css|js|gif|jpe?g|png|svg|webp)$ {
        # Some basic cache-control for static files to be sent to the browser
        expires 30d;
        add_header Pragma public;
        add_header Cache-Control "public, must-revalidate, proxy-revalidate";

        proxy_pass http://127.0.0.1:3000;
        proxy_set_header Host $host;
        proxy_buffering off;
    }
}
```

##### Edit /etc/hosts
```
127.0.0.1 chutzlaaretz-api.local
```

#### run both (cron and queue)

```
NODE_ENV=production pm2 start pm2.json
```

#### or launch main api

```
NODE_ENV=production pm2 start ./index.js
```

## Local

##### database setup

```
npm run db-drop
npm run db-create #(if the database hasn't already been created)
npm run migrate
npm run seed
```

##### run scripts when necessary (from backend directory)

```
node bin/cron.js
node bin/server.js
```

##### start node server

```
npm run dev
```

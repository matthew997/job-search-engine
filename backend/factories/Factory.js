class Factory {
    static async create(props = {}, { raw = false } = {}) {
        const data = this.generateData(props);
        const model = await this.model.create(data);

        return raw ? model.toJSON() : model;
    }

    static async make(props = {}, { raw = false } = {}) {
        const data = this.generateData(props);
        const model = await this.model.build(data);

        return raw ? model.toJSON() : model;
    }
}

module.exports = Factory;

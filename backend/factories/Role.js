const Factory = require('./Factory');
const { Role } = require('../models');
const uuid = require('uuid/v4');
const faker = require('faker');

class RoleFactory extends Factory {
    static get model() {
        return Role;
    }

    static generateData(props = {}) {
        const defaultProps = {
            id: uuid(),
            name: faker.random.arrayElement(['admin', 'user']),
            created_at: new Date(),
            updated_at: new Date()
        };

        return Object.assign({}, defaultProps, props);
    }
}

module.exports = RoleFactory;

const Factory = require('./Factory');
const { Review } = require('../models');
const faker = require('faker');

class ReviewFactory extends Factory {
    static get model() {
        return Review;
    }

    static generateData(props = {}) {
        const defaultProps = {
            start_date: faker.date.past(),
            end_date: faker.date.recent(),
            company: faker.company.companyName(),
            city: faker.address.city(),
            country: faker.address.country(),
            phone: faker.phone.phoneNumber(),
            phone_2: faker.phone.phoneNumber(),
            rating: faker.random.number({ min: 1, max: 10 }),
            content: faker.lorem.paragraph(),
            is_recommended: faker.random.boolean(),
            created_at: faker.date.past(),
            updated_at: new Date()
        };

        return Object.assign({}, defaultProps, props);
    }
}

module.exports = ReviewFactory;

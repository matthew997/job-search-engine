const faker = require('faker');
const uuid = require('uuid/v4');

const Factory = require('./Factory');
const { Company } = require('../models');

class CompanyFactory extends Factory {
    static get model() {
        return Company;
    }

    static generateData(props = {}) {
        const defaultProps = {
            name: faker.company.companyName(),
            city_id: uuid(),
            country_id: uuid(),
            promote_video: 'ce1f258f998a3f6e5870ef4182c1f461',
            facebook_link: faker.internet.url(),
            instagram_link: faker.internet.url(),
            promote_image: faker.random.image(),
            about: faker.lorem.sentence(),
            working_environment: faker.lorem.sentence(),
            office_nature: faker.lorem.sentence(),
            rating: faker.random.number({ min: 1, max: 10, precision: 0.01 }),
            number_of_reviews: faker.random.number({ min: 10, max: 100 }),
            active: true,
            created_at: new Date(),
            updated_at: new Date()
        };

        return Object.assign({}, defaultProps, props);
    }
}

module.exports = CompanyFactory;

const faker = require('faker');
const Factory = require('./Factory');
const { User } = require('../models');
const bcrypt = require('bcryptjs');

class UserFactory extends Factory {
    static get model() {
        return User;
    }

    static generateData(props = {}) {
        const defaultProps = {
            first_name: faker.name.firstName(),
            last_name: faker.name.lastName(),
            email: faker.internet.email().toLowerCase(),
            phone: `+15005${faker.random.number({
                min: 10,
                max: 99
            })}${faker.random.number({ min: 1000, max: 9999 })}`,
            password: bcrypt.hashSync('test')
        };

        return Object.assign({}, defaultProps, props);
    }
}

module.exports = UserFactory;

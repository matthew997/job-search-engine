const faker = require('faker');
const { range } = require('lodash');
const uuid = require('uuid/v4');

const Factory = require('./Factory');
const { CompanyLeadsFilter } = require('../models');

class CompanyLeadsFilterFactory extends Factory {
    static get model() {
        return CompanyLeadsFilter;
    }

    static generateData(props = {}) {
        const ages = range(17, 80);
        const minimumAge = faker.random.arrayElement(ages);

        const availability = [
            faker.random.arrayElement([
                CompanyLeadsFilter.AVAILABILITY_IMMEDIATE,
                CompanyLeadsFilter.AVAILABILITY_ONE_TO_THREE_MONTHS,
                CompanyLeadsFilter.AVAILABILITY_THREE_TO_SIX_MONTHS
            ])
        ];

        const defaultProps = {
            company_id: uuid(),
            leads_limit: faker.random.arrayElement([null, 0, 1, 2, 3, 4, 5, 10, 20, 30]),
            minimum_age: minimumAge,
            maximum_age: faker.random.arrayElement(range(minimumAge, 80)),
            sex: faker.random.arrayElement([null, 'm', 'f']),
            has_american_visa: faker.random.arrayElement([null, true, false]),
            has_foreign_passport: faker.random.arrayElement([null, true, false]),
            gold_users_only: faker.random.arrayElement([true, false]),
            availability: JSON.stringify(availability),
            created_at: new Date(),
            updated_at: new Date()
        };

        return Object.assign({}, defaultProps, props);
    }
}

module.exports = CompanyLeadsFilterFactory;

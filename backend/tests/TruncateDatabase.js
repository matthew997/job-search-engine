const map = require('lodash/map');
const { sequelize } = require('../models');
const models = require('../models/index');

module.exports = async () => {
    const transaction = await sequelize.transaction();
    const options = { raw: true, transaction };

    try {
        await sequelize.query('SET FOREIGN_KEY_CHECKS=0', options);
        await Promise.all(
            map(Object.keys(models), async key => {
                if (['sequelize', 'Sequelize'].includes(key)){
                    return null;
                }
                
                return models[key].truncate({ transaction });
            })
        );
        await sequelize.query('SET FOREIGN_KEY_CHECKS=1', options);
        await transaction.commit();
    } catch (err) {
        await transaction.rollback();
    }
};

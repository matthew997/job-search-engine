('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'dropdowns',
            {
                name: {
                    primaryKey: true,
                    type: Sequelize.STRING,
                    allowNull: false
                },
                display_name: {
                    type: Sequelize.STRING,
                    allowNull: false
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('dropdowns');
    }
};

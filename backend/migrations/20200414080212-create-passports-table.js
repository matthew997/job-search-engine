('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'passports',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                user_id: {
                    type: Sequelize.UUID,
                    references: {
                        model: 'users',
                        key: 'id'
                    }
                },
                name: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                issued_on: {
                    allowNull: false,
                    type: Sequelize.DATEONLY
                },
                expiration_date: {
                    allowNull: false,
                    type: Sequelize.DATEONLY
                },
                has_foreign_passport: {
                    allowNull: true,
                    type: Sequelize.BOOLEAN
                },
                country: {
                    allowNull: true,
                    type: Sequelize.STRING
                },
                has_american_visa: {
                    allowNull: true,
                    type: Sequelize.BOOLEAN
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                deleted_at: {
                    allowNull: true,
                    type: Sequelize.DATE,
                    defaultValue: null
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('passports');
    }
};

('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'mail_templates',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                code: {
                    type: Sequelize.STRING,
                    allowNull: false,
                    unique: true
                },
                name: {
                    type: Sequelize.STRING,
                    allowNull: false,
                    unique: true
                },
                description: {
                    type: Sequelize.TEXT,
                    allowNull: true
                },
                title: {
                    type: Sequelize.STRING,
                    allowNull: true
                },
                content: {
                    type: Sequelize.TEXT,
                    allowNull: true
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: (queryInterface) => {
        return queryInterface.dropTable('mail_templates');
    }
};

('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'user_details',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                user_id: {
                    type: Sequelize.UUID,
                    references: {
                        model: 'users',
                        key: 'id'
                    }
                },
                education: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                experience: {
                    allowNull: true,
                    type: Sequelize.STRING
                },
                experience_job_type: {
                    allowNull: true,
                    defaultValue: null,
                    type: Sequelize.STRING
                },
                service: {
                    allowNull: true,
                    type: Sequelize.STRING
                },
                availability: {
                    allowNull: false,
                    type: Sequelize.STRING
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                deleted_at: {
                    allowNull: true,
                    type: Sequelize.DATE,
                    defaultValue: null
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('user_details');
    }
};

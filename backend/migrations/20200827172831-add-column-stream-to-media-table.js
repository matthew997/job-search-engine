module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn('media', 'stream', {
            allowNull: true,
            defaultValue: null,
            type: Sequelize.STRING
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn('media', 'stream');
    }
};

('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'language_user',
            {
                user_id: {
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4,
                    references: {
                        model: 'users',
                        key: 'id'
                    }
                },
                language_name: {
                    type: Sequelize.STRING
                },
                proficiency: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('language_user');
    }
};

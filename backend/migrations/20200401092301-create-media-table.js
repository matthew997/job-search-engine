('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'media',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                user_id: {
                    type: Sequelize.UUID,
                    allowNull: false,
                    references: {
                        model: 'users',
                        key: 'id'
                    }
                },
                type: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                name: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                alt: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                filename: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                extension: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                meta: {
                    type: Sequelize.TEXT,
                    allowNull: false
                },
                date: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                size: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('media');
    }
};

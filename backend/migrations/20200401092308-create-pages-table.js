('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'pages',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                slug: {
                    type: Sequelize.STRING,
                    allowNull: false,
                    unique: true
                },
                title: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                content: {
                    type: Sequelize.TEXT,
                    allowNull: false
                },
                meta: {
                    type: Sequelize.TEXT,
                    allowNull: true
                },
                status: {
                    type: Sequelize.STRING,
                    allowNull: false,
                    defaultValue: 'draft'
                },
                created_by: {
                    type: Sequelize.UUID,
                    allowNull: true,
                    references: {
                        model: 'users',
                        key: 'id'
                    }
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                deleted_at: {
                    allowNull: true,
                    type: Sequelize.DATE
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('pages');
    }
};

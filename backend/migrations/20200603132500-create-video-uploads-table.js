('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'video_uploads',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.STRING
                },
                meta: {
                    type: Sequelize.TEXT,
                    allowNull: false
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('video_uploads');
    }
};

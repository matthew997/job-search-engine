('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'job_types',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                name: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                slug: {
                    type: Sequelize.STRING,
                    allowNull: false,
                    unique: true
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                deleted_at: {
                    allowNull: true,
                    type: Sequelize.DATE,
                    defaultValue: null
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('job_types');
    }
};

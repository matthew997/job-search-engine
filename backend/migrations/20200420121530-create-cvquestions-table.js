('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'cv_questions',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                index: {
                    type: Sequelize.INTEGER,
                    allowNull: false
                },
                title: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                info: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                bullets: {
                    type: Sequelize.TEXT,
                    allowNull: false
                },
                video_example: {
                    type: Sequelize.UUID,
                    allowNull: true,
                    references: {
                        model: 'media',
                        key: 'id'
                    }
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci',
                hooks: {
                    beforeSave(user) {
                        user.updated_at = new Date();
                    }
                }
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('cv_questions');
    }
};

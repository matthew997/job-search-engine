module.exports = {
    up: (queryInterface, Sequelize) =>
        queryInterface.createTable(
            'credit_cards',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                user_id: {
                    type: Sequelize.UUID,
                    allowNull: false,
                    references: {
                        model: 'users',
                        key: 'id'
                    }
                },
                token: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                last_4_digits: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                exp: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                brand: {
                    type: Sequelize.STRING,
                    allowNull: true
                },
                default: {
                    type: Sequelize.BOOLEAN,
                    defaultValue: false
                },
                created_at: {
                    allowNull: true,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: true,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                deleted_at: {
                    allowNull: true,
                    type: Sequelize.DATE
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        ),

    down: queryInterface => queryInterface.dropTable('credit_cards')
};

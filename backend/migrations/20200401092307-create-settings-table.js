module.exports = {
    up: (queryInterface, Sequelize) =>
        queryInterface.createTable(
            'settings',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                title: {
                    type: Sequelize.STRING,
                    allowNull: false,
                    defaultValue: ''
                },
                tag: {
                    type: Sequelize.STRING,
                    allowNull: false,
                    defaultValue: 'other'
                },
                key: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                value: {
                    type: Sequelize.STRING,
                    allowNull: true
                },
                type: {
                    type: Sequelize.STRING,
                    allowNull: false,
                    defaultValue: 'string'
                },
                order: {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                    defaultValue: 1
                },
                created_at: {
                    type: Sequelize.DATE,
                    allowNull: true,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    type: Sequelize.DATE,
                    allowNull: false,
                    defaultValue: Sequelize.literal('NOW()')
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        ),

    down: queryInterface => queryInterface.dropTable('settings')
};

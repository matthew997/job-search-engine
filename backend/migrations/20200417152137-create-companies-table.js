'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'companies',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                user_id: {
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4,
                    allowNull: false,
                    references: {
                        model: 'users',
                        key: 'id'
                    }
                },
                name: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                type_id: {
                    type: Sequelize.UUID,
                    allowNull: false,
                    references: {
                        model: 'job_types',
                        key: 'id'
                    }
                },
                country_id: {
                    type: Sequelize.UUID,
                    allowNull: false,
                    references: {
                        model: 'countries',
                        key: 'id'
                    }
                },
                city_id: {
                    type: Sequelize.UUID,
                    allowNull: false,
                    references: {
                        model: 'cities',
                        key: 'id'
                    }
                },
                company_logo: {
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4,
                    allowNull: false,
                    references: {
                        model: 'media',
                        key: 'id'
                    }
                },
                promote_video: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                facebook_link: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                instagram_link: {
                    type: Sequelize.STRING,
                    allowNull: true
                },
                promote_image: {
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4,
                    allowNull: false,
                    references: {
                        model: 'media',
                        key: 'id'
                    }
                },
                about: {
                    type: Sequelize.STRING(1000),
                    allowNull: false
                },
                working_environment: {
                    type: Sequelize.STRING(1000),
                    allowNull: false
                },
                office_nature: {
                    type: Sequelize.STRING(1000),
                    allowNull: false
                },
                rating: {
                    type: Sequelize.DECIMAL(4, 2),
                    allowNull: true
                },
                is_on_homepage: {
                    type: Sequelize.BOOLEAN,
                    allowNull: false,
                    defaultValue: false
                },
                number_of_reviews: {
                    type: Sequelize.INTEGER,
                    defaultValue: 0
                },
                active: {
                    type: Sequelize.BOOLEAN,
                    defaultValue: false
                },
                free_leads: {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                    defaultValue: 0
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                deleted_at: {
                    allowNull: true,
                    type: Sequelize.DATE
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_general_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('companies');
    }
};

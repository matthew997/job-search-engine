'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn('user_applications', 'deleted_at', {
            allowNull: true,
            defaultValue: null,
            type: Sequelize.DATE
        });

        await queryInterface.addColumn('payments', 'deleted_at', {
            allowNull: true,
            defaultValue: null,
            type: Sequelize.DATE
        });

        await queryInterface.addColumn('language_user', 'deleted_at', {
            allowNull: true,
            defaultValue: null,
            type: Sequelize.DATE
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn('user_applications', 'deleted_at');
        await queryInterface.removeColumn('payments', 'deleted_at');
        await queryInterface.removeColumn('language_user', 'deleted_at');
    }
};

module.exports = {
    up: (queryInterface, Sequelize) =>
        queryInterface.createTable(
            'users',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                first_name: {
                    type: Sequelize.STRING,
                    allowNull: true
                },
                last_name: {
                    type: Sequelize.STRING,
                    allowNull: true
                },
                email: {
                    type: Sequelize.STRING,
                    allowNull: false,
                    unique: true
                },
                phone: {
                    type: Sequelize.STRING,
                    allowNull: false,
                    unique: true
                },
                birthday: {
                    type: Sequelize.DATEONLY,
                    allowNull: true
                },
                gender: {
                    type: Sequelize.STRING,
                    allowNull: true
                },
                marital_status: {
                    type: Sequelize.STRING,
                    allowNull: true
                },
                hometown: {
                    type: Sequelize.STRING,
                    allowNull: true
                },
                type: {
                    type: Sequelize.STRING,
                    defaultValue: 'user',
                    allowNull: false
                },
                image_id: {
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4,
                    allowNull: true
                },
                password: {
                    type: Sequelize.STRING,
                    allowNull: true
                },
                pin: {
                    type: Sequelize.STRING,
                    allowNull: true
                },
                pin_generated_at: {
                    type: Sequelize.DATE,
                    allowNull: true
                },
                application_credits: {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                    defaultValue: 3
                },
                cancellation_credits: {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                    defaultValue: 2
                },
                subscription: {
                    allowNull: true,
                    defaultValue: null,
                    type: Sequelize.DATE
                },
                free_days: {
                    allowNull: false,
                    defaultValue: 0,
                    type: Sequelize.INTEGER
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                deleted_at: {
                    type: Sequelize.DATE,
                    allowNull: true
                },
                last_login: {
                    type: Sequelize.DATE,
                    allowNull: true
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        ),

    down: queryInterface => queryInterface.dropTable('users')
};

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn('users', 'registration_completed', {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn('users', 'registration_completed');
    }
};

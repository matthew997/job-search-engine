('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'company_leads_filters',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                company_id: {
                    type: Sequelize.UUID,
                    allowNull: false,
                    references: {
                        model: 'companies',
                        key: 'id'
                    }
                },
                leads_limit: {
                    type: Sequelize.INTEGER,
                    allowNull: true,
                    defaultValue: null
                },
                minimum_age: {
                    type: Sequelize.INTEGER,
                    allowNull: false
                },
                maximum_age: {
                    type: Sequelize.INTEGER,
                    allowNull: false
                },
                sex: {
                    type: Sequelize.STRING,
                    allowNull: true,
                    defaultValue: null
                },
                has_american_visa: {
                    allowNull: true,
                    type: Sequelize.BOOLEAN
                },
                has_foreign_passport: {
                    allowNull: true,
                    type: Sequelize.BOOLEAN
                },
                gold_users_only: {
                    allowNull: false,
                    type: Sequelize.BOOLEAN
                },
                availability: {
                    type: Sequelize.TEXT,
                    allowNull: true
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('company_leads_filters');
    }
};

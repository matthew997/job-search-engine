('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'cv_video_answers',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                user_id: {
                    type: Sequelize.UUID,
                    references: {
                        model: 'users',
                        key: 'id'
                    }
                },
                question_id: {
                    type: Sequelize.UUID,
                    references: {
                        model: 'cv_questions',
                        key: 'id'
                    }
                },
                video: {
                    type: Sequelize.STRING,
                    allowNull: true
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('cv_video_answers');
    }
};

('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'dropdown_option',
            {
                parent: {
                    primaryKey: true,
                    type: Sequelize.STRING,
                    allowNull: false
                },
                value: {
                    primaryKey: true,
                    type: Sequelize.STRING,
                    allowNull: true
                },
                text: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                order: {
                    type: Sequelize.INTEGER,
                    allowNull: false
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('dropdown_option');
    }
};

('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'reviews',
            {
                id: {
                    primaryKey: true,
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4
                },
                user_id: {
                    type: Sequelize.UUID,
                    defaultValue: Sequelize.UUIDV4,
                    references: {
                        model: 'users',
                        key: 'id'
                    }
                },
                company_id: {
                    type: Sequelize.UUID,
                    allowNull: true,
                    references: {
                        model: 'companies',
                        key: 'id'
                    }
                },
                start_date: {
                    allowNull: false,
                    type: Sequelize.DATEONLY
                },
                end_date: {
                    allowNull: false,
                    type: Sequelize.DATEONLY
                },
                rating: {
                    type: Sequelize.INTEGER,
                    allowNull: false
                },
                company: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                country: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                city: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                phone: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                phone_2: {
                    type: Sequelize.STRING,
                    allowNull: true
                },
                content: {
                    type: Sequelize.TEXT,
                    allowNull: false
                },
                is_recommended: {
                    type: Sequelize.BOOLEAN,
                    allowNull: false
                },
                created_at: {
                    allowNull: true,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                updated_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                },
                deleted_at: {
                    allowNull: true,
                    defaultValue: null,
                    type: Sequelize.DATE
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('reviews');
    }
};

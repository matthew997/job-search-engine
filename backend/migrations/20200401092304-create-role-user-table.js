('use strict');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            'role_user',
            {
                user_id: {
                    type: Sequelize.UUID,
                    allowNull: false,
                    onDelete: 'CASCADE',
                    references: {
                        model: 'users',
                        key: 'id'
                    },
                    primaryKey: true
                },
                role_id: {
                    type: Sequelize.UUID,
                    allowNull: false,
                    onDelete: 'CASCADE',
                    references: {
                        model: 'roles',
                        key: 'id'
                    },
                    primaryKey: true
                },
                created_at: {
                    allowNull: false,
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.literal('NOW()')
                }
            },
            {
                charset: 'utf8mb4',
                collate: 'utf8mb4_unicode_ci'
            }
        );
    },
    down: queryInterface => {
        return queryInterface.dropTable('role_user');
    }
};

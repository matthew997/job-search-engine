module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn('user_applications', 'used_statuses', {
            type: Sequelize.TEXT,
            allowNull: true
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn('user_applications', 'used_statuses');
    }
};

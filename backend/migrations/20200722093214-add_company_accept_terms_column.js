module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn(
            'user_registration',
            'company_accept_terms',
            {
                allowNull: false,
                defaultValue: false,
                type: Sequelize.BOOLEAN
            }
        );
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn(
            'user_registration',
            'company_accept_terms'
        );
    }
};

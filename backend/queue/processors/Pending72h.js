const { Application } = require('../../models');
const StatusService = require('../../services/Status');
const ApplicationRepository = require('../../repositories/Application');

module.exports = async function (job) {
    const { data } = job;
    const { application } = data;

    if (!application) {
        return Promise.resolve();
    }

    const currentApplication = await ApplicationRepository.findById(
        application.id
    );

    if (
        currentApplication &&
        currentApplication.status === Application.STATUS_PENDING
    ) {
        StatusService.addUserOneCreditWhenStatusChanged(application.user_id);
    }

    return Promise.resolve();
};

const ApplicationRepository = require('../../repositories/Application');
const { Application } = require('../../models');
const StatusService = require('../../services/Status');
const MailService = require('../../services/Mail');
const app = require('../../index');

module.exports = async function (job) {
    const { data } = job;
    const { application_id, replacements } = data;

    if (!application_id || !replacements) {
        return Promise.resolve();
    }

    try {
        const application = await ApplicationRepository.findById(
            application_id,
            {
                include: [
                    {
                        association: 'user',
                        attributes: ['email']
                    }
                ]
            }
        );

        if (application && application.status === Application.STATUS_APPLIED) {
            await application.update(
                {
                    status: Application.STATUS_PENDING,
                    updated_at: new Date()
                },
                {
                    fields: ['status', 'updated_at']
                }
            );

            StatusService.checkPendingStatusAfter72h(application);

            const mailService = new MailService();

            replacements.status = Application.STATUS_PENDING;

            await mailService.triggerEvent(
                'CANDIDATE_STATUS_CHANGE',
                application.user.email,
                replacements,
                app
            );
        }
    } catch (error) {
        console.error(error);
    }

    return Promise.resolve();
};

const VideoUploadRepository = require('../../repositories/VideoUpload');
const CloudflareStream = require('../../services/CloudflareStream');
const config = require('../../config');

class VideoUpload {
    constructor(app, job) {
        const { data } = job;

        this.app = app;
        this.item = data;
    }

    async run() {
        try {
            const cloudflareStream = new CloudflareStream(config.cloudflare);
            const mediaId = await cloudflareStream.upload(this.item.file);

            await VideoUploadRepository.create({
                id: mediaId,
                meta: this.item
            });
        } catch (error) {
            console.error(error);
        }
    }
}

module.exports = VideoUpload;

const CompanyRepository = require('../../repositories/Company');
const ContentRepository = require('../../repositories/Content');
const PaymentService = require('../../services/Payment');
const UserRepository = require('../../repositories/User');

class RenewSubscription {
    constructor(app, job) {
        const di = app.get('di');
        const { data: recruiter } = job;

        this.app = app;
        this.recruiter = recruiter;
        this.freeDaysUsed = 0;
        this.tranzila = di.get('tranzilaService');
    }

    async run() {
        try {
            const [creditCard = null] = this.recruiter.creditCards;
            const subscriptionPrice = await this.getSubscriptionPrice();

            if (subscriptionPrice) {
                if (!creditCard) {
                    return this.setRecruiterAsInactive();
                }

                const wasPaymentSuccessful = await this.tranzila.pay(
                    subscriptionPrice,
                    creditCard,
                    {
                        email: this.recruiter.email
                    }
                );

                if (!wasPaymentSuccessful) {
                    return this.setRecruiterAsInactive();
                }
            }

            await Promise.all([
                UserRepository.extendSubscription(this.recruiter.id),
                PaymentService.create({
                    type: 'subscription',
                    user_id: this.recruiter.id,
                    amount: subscriptionPrice
                })
            ]);

            if (this.freeDaysUsed) {
                const freeDaysLeft =
                    this.recruiter.free_days - this.freeDaysUsed;

                await UserRepository.setFreeDays(
                    this.recruiter.id,
                    freeDaysLeft
                );
            }
        } catch (error) {
            console.error(error);
        }
    }

    async getSubscriptionPrice() {
        const {
            value: { monthlyPrice = 0 }
        } = await ContentRepository.findByKey('payment-page');

        if (!monthlyPrice) {
            throw new Error('No subscription price has been set');
        }

        this.freeDaysUsed =
            this.recruiter.free_days >= 30 ? 30 : this.recruiter.free_days;

        const modifier = 1 - this.freeDaysUsed / 30;

        return (monthlyPrice * modifier).toFixed(2);
    }

    async setRecruiterAsInactive() {
        await CompanyRepository.setAllAsInactive(this.recruiter.id);
        // @TODO: notify the admin
    }
}

module.exports = RenewSubscription;

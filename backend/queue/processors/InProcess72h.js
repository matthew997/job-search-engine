const { Application } = require('../../models');
const StatusService = require('../../services/Status');
const ApplicationRepository = require('../../repositories/Application');
const MailService = require('../../services/Mail');
const app = require('../../index');

module.exports = async function (job) {
    const { data } = job;
    const { application, replacements } = data;

    if (!application) {
        return Promise.resolve();
    }

    try {
        const currentApplication = await ApplicationRepository.findById(
            application.id
        );

        if (
            currentApplication &&
            currentApplication.status === Application.STATUS_IN_PROGRESS
        ) {
            await currentApplication.update(
                {
                    status: Application.STATUS_FOLLOW_UP,
                    updated_at: new Date()
                },
                {
                    fields: ['status', 'updated_at']
                }
            );

            StatusService.addUserOneCreditWhenStatusChanged(
                application.user_id
            );

            const mailService = new MailService();

            replacements.status = Application.STATUS_FOLLOW_UP;

            await mailService.triggerEvent(
                'CANDIDATE_STATUS_CHANGE',
                application.user.email,
                replacements,
                app
            );
        }
    } catch (error) {
        console.error(error);
    }

    return Promise.resolve();
};

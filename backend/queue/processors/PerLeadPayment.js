const moment = require('moment');
const { Op } = require('sequelize');

const ApplicationRepository = require('../../repositories/Application');
const CompanyRepository = require('../../repositories/Company');
const ContentRepository = require('../../repositories/Content');
const PaymentService = require('../../services/Payment');

class PerLeadPayment {
    constructor(app, job) {
        const di = app.get('di');
        const { data: recruiter } = job;

        this.app = app;
        this.recruiter = recruiter;
        this.tranzila = di.get('tranzilaService');
    }

    async run() {
        try {
            const [creditCard = null] = this.recruiter.creditCards;
            const [billableLeads, pricePerLead] = await Promise.all([
                this.getBillableLeadsCount(),
                this.getPricePerLead()
            ]);

            if (!creditCard) {
                return this.setRecruiterAsInactive();
            }

            const totalPrice = billableLeads * pricePerLead;

            if (totalPrice) {
                const wasPaymentSuccessful = await this.tranzila.pay(
                    totalPrice,
                    creditCard,
                    {
                        email: this.recruiter.email
                    }
                );

                if (!wasPaymentSuccessful) {
                    return this.setRecruiterAsInactive();
                }
            }

            return PaymentService.create({
                user_id: this.recruiter.id,
                amount: totalPrice,
                type: 'leads'
            });
        } catch (error) {
            console.error(error);
        }
    }

    async getPricePerLead() {
        const {
            value: { pricePerLead = 0 }
        } = await ContentRepository.findByKey('payment-page');

        if (!pricePerLead) {
            throw new Error('No price per lead has been set');
        }

        return pricePerLead;
    }

    async getBillableLeadsCount() {
        let billableLeadsCount = 0;

        const lastMonth = moment().subtract(1, 'month');
        const from = moment(lastMonth).startOf('month');
        const to = moment(lastMonth).endOf('month');

        for (const company of this.recruiter.companies) {
            billableLeadsCount += await this.getCompanyBillableLeads(
                company,
                from,
                to
            );
        }

        return billableLeadsCount;
    }

    async getCompanyBillableLeads(company, from, to) {
        let freeLeadsLeft = 0;
        let billableLeadsCount = 0;

        const leadsCount = await ApplicationRepository.count({
            where: {
                company_id: company.id,
                created_at: {
                    [Op.between]: [from, to]
                }
            }
        });

        if (company.free_leads >= leadsCount) {
            billableLeadsCount = 0;

            freeLeadsLeft = company.free_leads - leadsCount;
        } else {
            billableLeadsCount = leadsCount - company.free_leads;
        }

        await CompanyRepository.setFreeLeads(company.id, freeLeadsLeft);

        return billableLeadsCount;
    }

    async setRecruiterAsInactive() {
        await CompanyRepository.setAllAsInactive(this.recruiter.id);
        // @TODO: notify the admin
    }
}

module.exports = PerLeadPayment;

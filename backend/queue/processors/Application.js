const EmailService = require('../../services/Email');
const ApplicationRepository = require('../../repositories/Application');

module.exports = async function (job) {
    const { data } = job;
    const { email } = data;
    const { application_id } = data;

    if (!application_id) {
        return Promise.resolve();
    }

    const application = await ApplicationRepository.findById(application_id);

    if (!application) {
        return Promise.resolve();
    }

    const emailService = new EmailService();

    emailService.send(email);

    return Promise.resolve();
};

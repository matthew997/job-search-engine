const EmailService = require('../../services/Email');

module.exports = async function(job) {
    const { data } = job;
    const { email } = data;

    const emailService = new EmailService();

    emailService.send(email);

    return Promise.resolve();
};

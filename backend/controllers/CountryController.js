const csvParser = require('csv-string');
const HTTP = require('http-status-codes');

const CountryRepository = require('../repositories/Country');
const CityRepository = require('../repositories/City');
const { Op } = require('sequelize');

const Controller = require('./Controller');

class CountryController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async index(request, response) {
        const countries = await CountryRepository.findAll();

        return response.status(HTTP.OK).json(countries);
    }

    async store(request, response) {
        const { name } = request.body;

        let country = await CountryRepository.findByName(name);

        if (country) {
            return response.sendStatus(HTTP.UNPROCESSABLE_ENTITY);
        }

        country = await CountryRepository.create({
            name,
            updated_at: new Date()
        });

        return response.status(HTTP.OK).json(country);
    }

    async update(request, response) {
        const { id } = request.params;
        const { cities, name } = request.body;

        const country = await CountryRepository.find(id);

        if (!country) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        if (cities) {
            for (const city of cities) {
                const query = {
                    name: city,
                    country_id: country.id
                };

                await CityRepository.updateOrCreate(query, query);
            }

            const cityIdsToBeRemoved = country.cities
                .filter(city => !cities.includes(city.name))
                .map(city => city.id);

            await CityRepository.destroy({
                where: {
                    id: {
                        [Op.in]: cityIdsToBeRemoved
                    }
                }
            });
        }

        if (name) {
            await country.update(
                {
                    name,
                    updated_at: new Date()
                },
                {
                    fields: ['name', 'updated_at']
                }
            );
        }

        return response.sendStatus(HTTP.OK).json(country);
    }

    async show(request, response) {
        const { id } = request.params;
        const country = await CountryRepository.findById(id);

        if (!country) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        return response.status(HTTP.OK).json(country);
    }

    async importCities(request, response) {
        const { id: countryId } = request.params;
        const { file } = request.files;

        const csv = file.data.toString();
        const parsedCities = csvParser.parse(csv);
        const country = await CountryRepository.find(countryId);

        for (const [name] of parsedCities.slice(1)) {
            const query = {
                name,
                country_id: country.id
            };

            await CityRepository.updateOrCreate(query, query);
        }

        return response.status(HTTP.OK).json(country.cities);
    }
}

module.exports = CountryController;

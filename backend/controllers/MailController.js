const HTTP = require('http-status-codes');
const SettingRepository = require('../repositories/Setting');
const Controller = require('./Controller');
const config = require('../config');

class MailController extends Controller {
    constructor(MailService) {
        super();

        this.MailService = MailService;
        this.bindMethods();
    }

    async contact(request, response) {
        const {
            name = null,
            email = null,
            message = null,
            title = null
        } = request.body;

        const replacements = {
            fullName: name,
            email,
            message,
            title
        };

        const receivingEmail = await SettingRepository.getByKey('receiving_email');

        await this.MailService.triggerEvent('CONTACT', receivingEmail || config.email.from.address, replacements, request.app);

        return response.sendStatus(HTTP.NO_CONTENT);
    }

    async newsletter(request, response) {
        const {
            email = null,
        } = request.body;

        const replacements = {
            email,
        };

        const receivingEmail = await SettingRepository.getByKey('receiving_email');

        await this.MailService.triggerEvent('NEWSLETTER', receivingEmail || config.email.from.address, replacements, request.app);

        return response.sendStatus(HTTP.NO_CONTENT);
    }
}

module.exports = MailController;

const fs = require('fs');
const HTTP = require('http-status-codes');

const VideoUploadRepository = require('../repositories/VideoUpload');
const models = require('../models');

const Controller = require('./Controller');

class CloudflareController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async webhook(request, response) {
        const { uid, readyToStream } = request.body;

        if (readyToStream) {
            const videoUpload = await VideoUploadRepository.findById(uid);

            if (videoUpload) {
                const { file, update } = videoUpload.meta;
                const { field, model, where } = update;

                await models[model].update(
                    {
                        [field]: uid
                    },
                    {
                        where
                    }
                );

                fs.unlink(file, function (error) {
                    if (error) {
                        console.error(error);
                    }
                });
            }
        }

        return response.sendStatus(HTTP.OK);
    }
}

module.exports = CloudflareController;

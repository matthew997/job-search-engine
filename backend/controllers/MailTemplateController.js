const HTTP = require('http-status-codes');
const { MailTemplate } = require('../models');
const MailTemplateService = require('../services/MailTemplate');
const MailTemplateRepository = require('../repositories/MailTemplate');

const Controller = require('./Controller');

class MailTemplateController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async index(request, response) {
        const templates = await MailTemplateRepository.findAll();

        return response.status(HTTP.OK).json({ templates });
    }

    async show(request, response) {
        const { id } = request.params;
        const template = await MailTemplateRepository.findById(id);

        if (!template) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        return response.status(HTTP.OK).json(template);
    }

    async update(request, response) {
        const { id } = request.params;
        const template = await MailTemplateRepository.findById(id);

        if (!template) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        const { body } = request;

        await template.update(
            { ...body, updated_at: new Date() },
            {
                fields: ['title', 'content', 'updated_at']
            }
        );

        return response.status(HTTP.OK).json(template);
    }

    variables(request, response) {
        const { key } = request.params;

        const variables = MailTemplateService.getVariablesForTemplate(key);

        if (!variables) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        return response.status(HTTP.OK).json(variables);
    }
}

module.exports = MailTemplateController;

const HTTP = require('http-status-codes');

const CompanyRepository = require('../repositories/Company');
const ChangeRepository = require('../repositories/Change');
const { CompanyLeadsFilter } = require('../models');

const Controller = require('./Controller');

class CompanyFilterController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async update(request, response) {
        const { id: userId } = request.loggedUser;
        const { companyId } = request.params;

        const company = await CompanyRepository.findById(companyId);

        if (!company) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        if (company.user_id !== userId) {
            return response.sendStatus(HTTP.FORBIDDEN);
        }

        await ChangeRepository.addForCompany(
            company.id,
            request.body,
            CompanyLeadsFilter
        );

        return response.sendStatus(HTTP.OK);
    }
}

module.exports = CompanyFilterController;

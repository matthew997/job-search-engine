const HTTP = require('http-status-codes');
const ms = require('ms');
const CVAnswerRepository = require('../repositories/CVVideoAnswer');
const CVAnswerService = require('../services/CVVideoAnswer');
const UserRepository = require('../repositories/User');
const CVQuestionRepository = require('../repositories/CVQuestion');
const UserRegistrationService = require('../services/UserRegistration');

const Controller = require('./Controller');

class CVVideoAnswerController extends Controller {
    constructor(videoUploadQueue) {
        super();

        this.videoUploadQueue = videoUploadQueue;

        this.bindMethods();
    }

    async index(request, response) {
        const answers = await CVAnswerRepository.findAllWithQuestion();

        for (const answer of answers) {
            answer.video = await CVAnswerService.getVideoFile(answer);
        }

        return response.status(HTTP.OK).json(answers);
    }

    async show(request, response) {
        const { id } = request.params;
        const answer = await CVAnswerRepository.findOneWithQuestion(id);

        if (!answer) {
            return response.status(HTTP.NOT_FOUND);
        }

        answer.video = await CVAnswerService.getVideoFile(answer);

        return response.status(HTTP.OK).json(answer);
    }

    async store(request, response) {
        const { id: userId } = request.loggedUser;
        const answerData = { ...request.body, user_id: userId };

        let videoPath;

        if (request.files) {
            const { video } = request.files;
            answerData.video = video;

            videoPath = await CVAnswerService.storeUploadedVideo(answerData);
        } else {
            videoPath = CVAnswerService.writeVideo(answerData);
        }

        const answer = await CVAnswerRepository.updateOrCreate(
            { ...answerData, video: videoPath },
            {
                question_id: answerData.question_id,
                user_id: userId
            }
        );

        const [answersCount, questionsCount] = await Promise.all([
            CVAnswerRepository.count({
                where: {
                    user_id: userId
                }
            }),
            CVQuestionRepository.count()
        ]);

        if (answersCount === questionsCount) {
            await UserRegistrationService.updateStepByUserId(
                userId,
                'third_step',
                true
            );
        }

        await this.videoUploadQueue.add(
            {
                file: CVAnswerService.getVideoPath(answer.video),
                update: {
                    model: 'CVVideoAnswer',
                    field: 'video',
                    where: {
                        id: answer.id
                    }
                }
            },
            {
                delay: ms('0.5 hrs')
            }
        );

        return response.status(HTTP.CREATED).json(answer);
    }

    createDirectory(directory) {
        try {
            fs.statSync(directory);
        } catch (e) {
            fs.mkdirSync(directory, { recursive: true });
        }
    }

    async update(request, response) {
        const { id } = request.params;
        const updatedData = request.body;
        const attributes = [
            'index',
            'title',
            'info',
            'bullets',
            'video_example'
        ];

        const answer = await CVAnswerRepository.findById(id, {
            include: [
                {
                    association: 'question',
                    attributes
                }
            ]
        });

        if (!answer) {
            return response.status(HTTP.NOT_FOUND);
        }

        const videoPath = await CVAnswerService.writeVideo(updatedData);

        await answer.update({ ...updatedData, video: videoPath });

        await this.videoUploadQueue.add(
            {
                file: CVAnswerService.getVideoPath(answer.video),
                update: {
                    model: 'CVVideoAnswer',
                    field: 'video',
                    where: {
                        id: answer.id
                    }
                }
            },
            {
                delay: ms('5m')
            }
        );

        return response.status(HTTP.OK).json(answer);
    }

    async destroy(request, response) {
        const { id } = request.params;
        const answer = await CVAnswerRepository.findById(id);

        CVAnswerService.deleteVideoAndAnswer(answer);

        return response.sendStatus(HTTP.NO_CONTENT);
    }

    async destroyAllForUser(request, response) {
        const { id: userId } = request.loggedUser;
        const user = await UserRepository.findById(userId);

        if (!user) {
            return response.status(HTTP.NOT_FOUND);
        }

        CVAnswerService.deleteAllVideosAndAnswersForUser(userId);

        return response.sendStatus(HTTP.NO_CONTENT);
    }

    async showAllForUser(request, response) {
        const { id: userId } = request.loggedUser;
        const answers = await CVAnswerRepository.findAllForUser(userId, true);

        // @TODO check that funcion with joinVideos - subtask/implement record wideo and watch for all answers

        // const video = await CVAnswerService.joinVideos(answers);

        return response.status(HTTP.OK).json({
            answers
        });
    }
}

module.exports = CVVideoAnswerController;

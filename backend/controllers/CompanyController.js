const path = require('path');
const moment = require('moment');
const HTTP = require('http-status-codes');

const { Company, Sequelize, sequelize: db } = require('../models');
const { Op } = Sequelize;
const ApplicationRepository = require('../repositories/Application');
const ChangeRepository = require('../repositories/Change');
const CompanyRepository = require('../repositories/Company');
const UserRepository = require('../repositories/User');
const StoreMediaFromFrontendHandler = require('../services/StoreMediaFromFrontendHandler');
const StoreVideoHandler = require('../services/StoreVideoHandler');
const ReviewsRepository = require('../repositories/Review');
const Sorting = require('../services/Sorting');
const UserRegistrationService = require('../services/UserRegistration');
const CompanyLeadsFilterRepository = require('../repositories/CompanyLeadsFilter');

const Controller = require('./Controller');

class CompanyController extends Controller {
    constructor(videoUploadQueue) {
        super();

        this.videoUploadQueue = videoUploadQueue;

        this.bindMethods();
    }

    async index(request, response) {
        const {
            city,
            country,
            jobType,
            isOnHomepage,
            jobsPage
        } = request.query;

        const { loggedUser } = request;

        const where = {
            active: true
        };

        if (city) {
            where.city_id = city;
        }

        if (country) {
            where.country_id = country;
        }

        if (jobType) {
            where.type_id = jobType;
        }

        if (jobsPage) {
            const filteredCompanies = await CompanyRepository.filterCompaniesToUser(
                where,
                loggedUser
            );

            return response.status(HTTP.OK).json(filteredCompanies);
        }

        const companies = await CompanyRepository.findAll({
            where,
            include: [
                {
                    association: 'promoteImage'
                },
                {
                    association: 'companyLogo'
                },
                {
                    association: 'country'
                },
                {
                    association: 'city'
                }
            ]
        });

        return response.send(companies);
    }

    async store(request, response) {
        const { id: userId } = request.loggedUser;
        const { promote_image, company_logo, promote_video } = request.files;

        const promoteImage = await StoreMediaFromFrontendHandler.handle(
            promote_image,
            userId
        );

        if (!promoteImage) {
            return response
                .status(HTTP.BAD_REQUEST)
                .json({ error: 'קובץ שגוי לקידום תמונה' });
        }

        const companyLogo = await StoreMediaFromFrontendHandler.handle(
            company_logo,
            userId
        );

        if (!companyLogo) {
            return response
                .status(HTTP.BAD_REQUEST)
                .json({ error: 'קובץ שגוי עבור לוגו החברה' });
        }

        const promoteVideo = await StoreVideoHandler.handle(promote_video);

        if (!promoteVideo) {
            return response
                .status(HTTP.BAD_REQUEST)
                .json({ error: 'קובץ שגוי לקידום וידאו' });
        }

        const company = await CompanyRepository.create({
            ...request.body,
            promote_image: promoteImage.id,
            company_logo: companyLogo.id,
            promote_video: promoteVideo,
            user_id: userId
        });

        await company.createFilter({
            leads_limit: null,
            minimum_age: 17,
            maximum_age: 80,
            gold_users_only: false,
            availability: ['1-3m', 'immediate', '3-6m']
        });

        await this.videoUploadQueue.add({
            file: this.getPromoteVideoFullPath(promoteVideo),
            update: {
                model: 'Company',
                field: 'promote_video',
                where: {
                    id: company.id
                }
            }
        });

        if (!request.loggedUser.registration_completed) {
            await UserRegistrationService.updateStepByUserId(
                userId,
                'second_step',
                true
            );
        }

        if (request.loggedUser.registration_completed) {
            await CompanyRepository.setAllAsActive(userId);
        }

        return response.status(HTTP.CREATED).json(company);
    }

    async show(request, response) {
        const { id } = request.params;
        const { id: user_id } = request.loggedUser;

        const application = await ApplicationRepository.findOne({
            where: {
                user_id,
                company_id: id
            }
        });

        if (!application) {
            const company = await CompanyRepository.findById(id, {
                attributes: [
                    'name',
                    'promote_video',
                    'facebook_link',
                    'instagram_link',
                    'about',
                    'working_environment',
                    'office_nature'
                ],
                include: [
                    {
                        association: 'promoteImage'
                    },
                    {
                        association: 'companyLogo'
                    },
                    {
                        association: 'country'
                    },
                    {
                        association: 'city'
                    },
                    {
                        association: 'user'
                    }
                ]
            });

            if (!company) {
                return response.sendStatus(HTTP.NOT_FOUND);
            }

            return response.status(HTTP.OK).json({ company });
        }

        const company = await CompanyRepository.findById(id, {
            include: [
                {
                    association: 'promoteImage'
                },
                {
                    association: 'companyLogo'
                },
                {
                    association: 'country'
                },
                {
                    association: 'city'
                },
                {
                    association: 'reviews',
                    include: [
                        {
                            association: 'user',
                            attributes: [
                                'first_name',
                                'last_name',
                                'full_name'
                            ],
                            include: [{ association: 'image' }],
                            paranoid: false
                        }
                    ]
                }
            ]
        });

        if (!company) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        return response.status(HTTP.OK).json({ company, application });
    }

    async showLocked(request, response) {
        const { id } = request.params;

        const company = await CompanyRepository.findById(id, {
            attributes: ['name', 'promote_video'],
            include: [
                {
                    association: 'promoteImage'
                },
                {
                    association: 'companyLogo'
                },
                {
                    association: 'country'
                },
                {
                    association: 'city'
                }
            ]
        });

        if (!company) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        return response.status(HTTP.OK).json({ company });
    }

    async destroy(request, response) {
        const { id } = request.params;

        const company = await CompanyRepository.findById(id);

        if (company) {
            this.addUserOneCreditWhenDeleteCompany(company);

            await CompanyLeadsFilterRepository.destroy({
                where: {
                    company_id: id
                }
            });

            await ApplicationRepository.destroy({
                where: {
                    company_id: id
                }
            });

            await CompanyRepository.destroy({
                where: {
                    id
                }
            });
        }

        return response.sendStatus(HTTP.NO_CONTENT);
    }

    async update(request, response) {
        const { id: userId } = request.loggedUser;
        const { id: companyId } = request.params;
        const { promote_image, promote_video, company_logo } =
            request.files || {};
        const mediaUpdate = {};

        const company = await CompanyRepository.findById(companyId, {
            include: [
                {
                    association: 'user'
                }
            ]
        });

        if (!company) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        if (company.user.id !== userId) {
            return response.sendStatus(HTTP.FORBIDDEN);
        }

        if (promote_image) {
            const promoteImage = await StoreMediaFromFrontendHandler.handle(
                promote_image,
                userId
            );

            if (!promoteImage) {
                return response
                    .status(HTTP.BAD_REQUEST)
                    .json({ error: 'קובץ שגוי לקידום תמונה' });
            }

            mediaUpdate.promote_image = promoteImage.id;
        }

        if (company_logo) {
            const companyLogo = await StoreMediaFromFrontendHandler.handle(
                company_logo,
                userId
            );

            if (!companyLogo) {
                return response
                    .status(HTTP.BAD_REQUEST)
                    .json({ error: 'קובץ שגוי עבור לוגו החברה' });
            }

            mediaUpdate.company_logo = companyLogo.id;
        }

        if (promote_video) {
            const promoteVideo = await StoreVideoHandler.handle(promote_video);

            if (!promoteVideo) {
                return response
                    .status(HTTP.BAD_REQUEST)
                    .json({ error: 'קובץ שגוי לקידום וידאו' });
            }

            mediaUpdate.promote_video = promoteVideo;

            await this.videoUploadQueue.add({
                file: this.getPromoteVideoFullPath(promoteVideo),
                update: {
                    model: 'Company',
                    field: 'promote_video',
                    where: {
                        id: company.id
                    }
                }
            });
        }

        const updateData = {
            ...request.body,
            ...mediaUpdate
        };

        await ChangeRepository.addForCompany(company.id, updateData, Company);

        return response.sendStatus(HTTP.OK);
    }

    async addFreeLeads(request, response) {
        const { id: companyId } = request.params;
        const { qty } = request.body;

        const company = await CompanyRepository.findById(companyId);

        if (!company) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        const leads = company.free_leads + parseInt(qty);

        await CompanyRepository.setFreeLeads(company.id, leads);

        return response.sendStatus(HTTP.OK);
    }

    async addFreeDays(request, response) {
        const { id: companyId } = request.params;
        const { qty } = request.body;

        const company = await CompanyRepository.findById(companyId, {
            include: [
                {
                    association: 'user'
                }
            ]
        });

        if (!company) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        const days = company.user.free_days + parseInt(qty);

        await UserRepository.setFreeDays(company.user.id, days);

        return response.sendStatus(HTTP.OK);
    }

    async setActiveStatus(request, response) {
        const { id: companyId } = request.params;
        const { active } = request.body;

        const company = await CompanyRepository.findById(companyId);

        if (!company) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        const method = active ? 'setAsActive' : 'setAsInactive';

        await CompanyRepository[method](company.id);

        return response.sendStatus(HTTP.OK);
    }

    async paymentTab(request, response) {
        const { id: companyId } = request.params;

        const from = moment().startOf('week');
        const to = moment().endOf('week');

        let [company, leadsThisWeek] = await Promise.all([
            CompanyRepository.findById(companyId, {
                attributes: {
                    include: [
                        [db.fn('count', 'applications.id'), 'total_leads']
                    ]
                },
                include: [
                    {
                        association: 'applications',
                        attributes: []
                    }
                ]
            }),

            ApplicationRepository.count({
                where: {
                    company_id: companyId,
                    created_at: {
                        [Op.between]: [from, to]
                    }
                }
            })
        ]);

        company = {
            ...company.toJSON(),
            leads_this_week: leadsThisWeek
        };

        return response.status(HTTP.OK).json(company);
    }

    async filters(request, response) {
        const filters = await CompanyRepository.getLocationFilters();

        return response.status(HTTP.OK).json(filters);
    }

    async getApplicationsForCompany(request, response) {
        let {
            page = 1,
            perPage = 10,
            q = '',
            sortBy = 'null',
            order: sortOrder = 'DESC',
            status = null
        } = request.query;
        const { id: companyId } = request.params;

        page = parseInt(page);
        perPage = parseInt(perPage);

        const where = {
            company_id: companyId
        };

        if (status) {
            where.status = status;
        }

        const fieldIncludesQueryString = {
            [Op.like]: `%${q}%`
        };

        if (request.query.range) {
            const range = JSON.parse(request.query.range);

            where.created_at = {
                [Op.between]: [range.startDate, range.endDate]
            };
        }

        if (q) {
            where[Op.or] = [
                {
                    status: fieldIncludesQueryString
                },
                {
                    user_id: fieldIncludesQueryString
                },
                {
                    ['$user.email$']: fieldIncludesQueryString
                },
                {
                    ['$user.first_name$']: fieldIncludesQueryString
                },
                {
                    ['$user.last_name$']: fieldIncludesQueryString
                },
                {
                    ['$user.details.experience$']: fieldIncludesQueryString
                },
                {
                    ['$user.details.availability$']: fieldIncludesQueryString
                }
            ];
        }

        let order = [['created_at', 'DESC']];

        if (sortBy) {
            order = Sorting.handle(sortBy, sortOrder, 'model');
        }

        const offset = (page - 1) * perPage;
        const limit = perPage;

        const applications = await ApplicationRepository.getAllPerCompany({
            where,
            offset,
            limit,
            order
        });

        return response.status(HTTP.OK).json(applications);
    }

    async getCompaniesForCMS(request, response) {
        let {
            page = 1,
            perPage = 25,
            q = '',
            sortBy = 'created_at',
            order = 'desc',
            isOnHomepage
        } = request.query;

        const where = {};

        const fieldIncludesQueryString = {
            [Op.like]: `%${q}%`
        };

        if (q) {
            where[Op.or] = [
                {
                    name: fieldIncludesQueryString
                },
                {
                    ['$jobType.slug$']: fieldIncludesQueryString
                },
                {
                    ['$jobType.name$']: fieldIncludesQueryString
                }
            ];
        }

        if (isOnHomepage) {
            where.is_on_homepage = isOnHomepage === 'true';
        }

        page = parseInt(page);
        perPage = parseInt(perPage);

        const offset = (page - 1) * perPage;
        const limit = parseInt(perPage);

        const {
            companiesRows,
            companiesCount
        } = await CompanyRepository.findAllAndCountLeads({
            where,
            sortBy,
            order,
            offset,
            limit
        });

        const companies = {
            rows: companiesRows,
            count: companiesCount
        };

        return response.status(HTTP.OK).json({ companies });
    }

    async getReviewsForCompany(request, response) {
        const { id: companyId } = request.params;

        const reviews = await ReviewsRepository.findAll({
            where: {
                company_id: companyId
            },
            include: [
                {
                    association: 'user'
                }
            ]
        });

        return response.status(HTTP.OK).json(reviews);
    }

    getPromoteVideoFullPath(videoPath) {
        return path.join(
            __dirname,
            `./../public/media/videos/users/${videoPath}`
        );
    }

    async setHomepageStatus(request, response) {
        const { id: companyId } = request.params;
        const { isOnHomepage } = request.body;

        const company = await CompanyRepository.findById(companyId);

        if (!company) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        const method = isOnHomepage ? 'removeFromHomepage' : 'setOnHomepage';

        await CompanyRepository[method](company.id);

        return response.sendStatus(HTTP.OK);
    }

    async setPaymentTerms(request, response) {
        const { userId } = request.params;

        if (!userId) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        await UserRegistrationService.updateCompanyAcceptedTerms(userId);

        return response.sendStatus(HTTP.OK);
    }

    async addUserOneCreditWhenDeleteCompany(company) {
        const applications = await ApplicationRepository.findAll({
            where: {
                company_id: company.id
            },
            include: [
                {
                    association: 'user',
                    attributes: ['application_credits']
                }
            ]
        });

        for (let i = 0; i < applications.length; i++) {
            const id = applications[i].user_id;

            const user = await UserRepository.findById(id);

            if (!user) {
                continue;
            }

            await user.update(
                {
                    application_credits: user.application_credits + 1,
                    updated_at: new Date()
                },
                {
                    fields: ['application_credits', 'updated_at']
                }
            );
        }
    }
}

module.exports = CompanyController;

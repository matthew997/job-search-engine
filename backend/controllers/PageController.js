const HTTP = require('http-status-codes');
const { Op } = require('sequelize');
const PageRepository = require('../repositories/Page');
const UserRepository = require('../repositories/User');
const { Page } = require('../models');
const slug = require('slug');
const IsSlugProhibitedHandler = require('../services/IsSlugProhibitedHandler');

const Controller = require('./Controller');

class PageController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async index(request, response) {
        let {
            page = 1,
            perPage = 25,
            q = '',
            sortBy = 'created_at',
            order = 'desc',
            status = 'all'
        } = request.query;

        const fieldIncludesQueryString = {
            [Op.like]: `%${q}%`
        };

        const where = {};

        if (status !== 'all') {
            where.status = status;
        }

        if (q) {
            where[Op.or] = [
                {
                    title: fieldIncludesQueryString
                },
                {
                    slug: fieldIncludesQueryString
                }
            ];
        }

        page = parseInt(page);
        perPage = parseInt(perPage);

        const offset = (page - 1) * perPage;
        const limit = perPage;

        const container = request.app.get('di');
        const Cache = container.get('cacheService');
        const key = `pages:index:${page}:${perPage}:${q}:${sortBy}:${order}:${status}`;

        if (await Cache.exists(key)) {
            return response.json(await Cache.get(key));
        }

        const pages = await PageRepository.findAndCountAll({
            offset,
            limit,
            where,
            include: [
                {
                    association: 'author',
                    attributes: ['first_name', 'last_name', 'full_name']
                }
            ],
            order: [[sortBy, order]]
        });

        await Cache.set(key, { pages });

        return response.json(pages);
    }

    async store(request, response) {
        const pageData = { ...request.body };
        const { id: userId } = request.loggedUser;
        pageData.slug = slug(pageData.slug.toLowerCase());

        const isSlugAvailable = await PageRepository.isSlugAvailable(
            pageData.slug
        );

        if (!isSlugAvailable) {
            return response
                .status(HTTP.UNPROCESSABLE_ENTITY)
                .json({ errors: { slug: ['שבלול כבר קיים.'] } });
        }

        if (IsSlugProhibitedHandler.handle(pageData.slug)) {
            return response
                .status(HTTP.UNPROCESSABLE_ENTITY)
                .json({ errors: { slug: ['שבלול זה אסור'] } });
        }

        const page = await PageRepository.create(
            {
                ...pageData,
                created_by: userId
            },
            {
                fields: [
                    'slug',
                    'title',
                    'content',
                    'meta',
                    'created_by',
                    'created_at',
                    'updated_at'
                ]
            }
        );

        const container = request.app.get('di');
        const Cache = container.get('cacheService');
        await Cache.forgetByPattern('pages:*');

        return response.json(page);
    }

    async show(request, response) {
        const { idOrSlug } = request.params;

        if (request.session && request.session.users) {
            const loggedAdmin = request.session.users.find(async id => {
                const currentUser = await UserRepository.findById(id);

                if (currentUser && (await currentUser.isAdmin())) {
                    return currentUser;
                }
            });

            if (loggedAdmin) {
                const container = request.app.get('di');
                const Cache = container.get('cacheService');
                const key = `pages:show:${idOrSlug}`;

                if (await Cache.exists(key)) {
                    return response.json(await Cache.get(key));
                }

                const page = await PageRepository.findOne({
                    where: {
                        [Op.or]: {
                            id: idOrSlug,
                            slug: idOrSlug
                        }
                    },
                    include: [
                        {
                            association: 'author'
                        }
                    ]
                });

                if (!page) {
                    return response.sendStatus(HTTP.NOT_FOUND);
                }

                await Cache.set(key, page);

                return response.json(page);
            }
        }

        const container = request.app.get('di');
        const Cache = container.get('cacheService');
        const key = `pages:published:show:${idOrSlug}`;

        if (await Cache.exists(key)) {
            return response.json(await Cache.get(key));
        }

        const page = await PageRepository.findOne({
            where: {
                [Op.or]: {
                    id: idOrSlug,
                    slug: idOrSlug
                },
                status: Page.STATUS_PUBLISHED
            }
        });

        if (!page) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        await Cache.set(key, page);

        return response.json(page);
    }

    async update(request, response) {
        const { id } = request.params;
        const pageData = { ...request.body };

        const page = await PageRepository.findById(id);

        if (!page) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        pageData.slug = slug(pageData.slug.toLowerCase());

        const isSlugAvailable = await PageRepository.isSlugAvailable(
            pageData.slug,
            id
        );

        if (!isSlugAvailable) {
            return response
                .status(HTTP.UNPROCESSABLE_ENTITY)
                .json({ errors: { slug: ['שבלול כבר קיים.'] } });
        }

        if (IsSlugProhibitedHandler.handle(pageData.slug)) {
            return response
                .status(HTTP.UNPROCESSABLE_ENTITY)
                .json({ errors: { slug: ['שבלול זה אסור.'] } });
        }

        await page.update(
            {
                ...pageData,
                updated_at: new Date()
            },
            {
                fields: ['title', 'slug', 'content', 'meta', 'updated_at']
            }
        );

        const container = request.app.get('di');
        const Cache = container.get('cacheService');
        await Cache.forgetByPattern('pages:*');

        return response.json(page);
    }

    async destroy(request, response) {
        const { id } = request.params;

        const page = await PageRepository.findById(id);

        await page.destroy({});

        const container = request.app.get('di');
        const Cache = container.get('cacheService');
        await Cache.forgetByPattern('pages:*');

        return response.sendStatus(HTTP.NO_CONTENT);
    }

    async bulk(request, response) {
        const allowedActions = [
            Page.STATUS_DELETED,
            Page.STATUS_PUBLISHED,
            Page.STATUS_DRAFT
        ];
        const { name: status, ids = [] } = request.body;

        if (allowedActions.includes(status) && ids.length) {
            await PageRepository.update(
                {
                    status
                },
                {
                    where: {
                        id: ids
                    }
                }
            );

            const container = request.app.get('di');
            const Cache = container.get('cacheService');
            await Cache.forgetByPattern('pages:*');
        }

        return response.sendStatus(HTTP.OK);
    }

    async status(request, response) {
        const { id } = request.params;
        const { status } = request.body;
        const allowedActions = [
            Page.STATUS_DELETED,
            Page.STATUS_PUBLISHED,
            Page.STATUS_DRAFT
        ];

        const page = await PageRepository.findById(id);

        if (!page) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        if (allowedActions.includes(status)) {
            await page.update(
                {
                    status
                },
                {
                    fields: ['status', 'updated_at']
                }
            );

            const container = request.app.get('di');
            const Cache = container.get('cacheService');
            await Cache.forgetByPattern('pages:*');
        }

        return response.json(page);
    }
}

module.exports = PageController;

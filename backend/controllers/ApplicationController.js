const HTTP = require('http-status-codes');

const ApplicationRepository = require('../repositories/Application');
const UserRepository = require('../repositories/User');
const CompanyRepository = require('../repositories/Company');
const { Application } = require('../models');
const moment = require('moment');
const StatusService = require('../services/Status');

const Controller = require('./Controller');

class ApplicationController extends Controller {
    constructor(MailService) {
        super();

        this.MailService = MailService;
        this.bindMethods();
    }

    async index(request, response) {
        const { id: userId } = request.loggedUser;

        const applications = await ApplicationRepository.getUserApplications(
            userId
        );

        return response.status(HTTP.OK).json(applications);
    }

    async store(request, response) {
        const { company_id } = request.body;
        const {
            id: user_id,
            application_credits,
            email,
            full_name
        } = request.loggedUser;
        const user = request.loggedUser;

        if (application_credits <= 0) {
            return response.status(HTTP.BAD_REQUEST).json({
                error: 'אין מספיק קרדיטים להגשת מועמדות',
                key: 'no_app_credits'
            });
        }

        const userApplication = await ApplicationRepository.findOne({
            where: {
                user_id,
                company_id
            }
        });

        if (userApplication) {
            return response.status(HTTP.BAD_REQUEST).json({
                error: 'הגשת כבר בקשה לחברה זו',
                key: 'already_applied'
            });
        }

        await user.update(
            { application_credits: application_credits - 1 },
            {
                fields: ['application_credits', 'updated_at']
            }
        );

        const application = await ApplicationRepository.create({
            company_id,
            user_id,
            status: Application.STATUS_APPLIED,
            used_statuses: Application.STATUS_APPLIED
        });

        const company = await CompanyRepository.findById(company_id, {
            include: [
                {
                    association: 'user',
                    attributes: ['email']
                }
            ]
        });

        const candidateReplacements = {
            companyName: company.name
        };

        const companyReplacements = {
            fullName: full_name,
            companyName: company.name
        };

        Promise.all([
            this.MailService.triggerEvent(
                'APPLICATION_CANDIDATE',
                email,
                candidateReplacements,
                request.app,
                application.id
            ),
            this.MailService.triggerEvent(
                'APPLIED_24H',
                company.user.email,
                companyReplacements,
                request.app,
                application.id
            ),
            this.MailService.triggerEvent(
                'NEW_LEAD',
                company.user.email,
                companyReplacements,
                request.app,
                application.id
            )
        ]);

        return response.status(HTTP.OK).json(application);
    }

    async show(request, response) {
        const { id } = request.params;
        const application = await ApplicationRepository.getDetailsForId(id);

        if (!application) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        return response.status(HTTP.OK).json(application);
    }

    async destroy(request, response) {}

    async updateNote(request, response) {
        const { id } = request.params;
        const { note } = request.body;
        const application = await ApplicationRepository.findById(id);

        if (!application) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        await application.update({ note });

        return response.status(HTTP.OK).json(application);
    }

    async updateStatus(request, response) {
        const { id } = request.params;
        const { status } = request.body;

        const application = await ApplicationRepository.findById(id, {
            include: [
                {
                    association: 'user',
                    attributes: ['email']
                },
                {
                    association: 'company',
                    attributes: ['name']
                }
            ]
        });

        let { used_statuses } = application;



        if(!used_statuses){
            used_statuses = 'applied';
        }

        if (used_statuses.includes(status)) {
            return response.status(HTTP.BAD_REQUEST).json({
                error: 'לא ניתן לשנות סטטוס, כבר נעשה שימוש בסטטוס זה',
                key: 'status_used'
            });
        }

        used_statuses = used_statuses.concat(',', status);
        
        
        if (!application) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        await application.update({ status, used_statuses });

        const replacements = {
            status,
            companyName: application.company.name
        };

        await this.MailService.triggerEvent(
            'CANDIDATE_STATUS_CHANGE',
            application.user.email,
            replacements,
            request.app
        );

        const statusesWithoutQueue = [
            Application.STATUS_REJECTED,
            Application.STATUS_APPROVED,
            Application.STATUS_FOLLOW_UP
        ];

        if (statusesWithoutQueue.includes(status)) {
            StatusService.addUserOneCreditWhenStatusChanged(
                application.user_id
            );
        }

        if (status === Application.STATUS_IN_PROGRESS) {
            StatusService.checkInProgressStatusAfter72Hours(
                application,
                replacements
            );
        }

        return response.status(HTTP.OK).json(application);
    }

    async cancel(request, response) {
        const { company_id } = request.body;
        const {
            id: user_id,
            cancellation_credits,
            application_credits
        } = request.loggedUser;
        const user = request.loggedUser;

        if (!user) {
            return response.sendStatus(HTTP.UNAUTHORIZED);
        }

        if (cancellation_credits <= 0) {
            return response.status(HTTP.BAD_REQUEST).json({
                error: 'אין מספיק קרדיטים לביטול',
                key: 'no_cancel_credits'
            });
        }

        const application = await ApplicationRepository.findOne({
            where: {
                user_id,
                company_id
            }
        });

        if (!application) {
            return response.status(HTTP.BAD_REQUEST).json({
                error: 'לא הגשת בקשה לחברה זו',
                key: 'not_applied'
            });
        }

        const date = moment(new Date());

        if (
            moment.duration(date.diff(application.created_at)).asMinutes() > 10
        ) {
            return response.status(HTTP.BAD_REQUEST).json({
                error: 'לא ניתן לבטל את הבקשה',
                key: 'cannot_cancel'
            });
        }

        await user.update(
            {
                cancellation_credits: cancellation_credits - 1,
                application_credits: application_credits + 1
            },
            {
                fields: [
                    'application_credits',
                    'cancellation_credits',
                    'updated_at'
                ]
            }
        );

        await application.destroy();

        return response.sendStatus(HTTP.NO_CONTENT);
    }

    async getApplicationsForRecruiter(request, response) {
        const { id: userId } = request.loggedUser;
        let { page = 1, perPage = 10, companyId } = request.query;

        page = parseInt(page);
        perPage = parseInt(perPage);

        const offset = (page - 1) * perPage;
        const limit = perPage;

        const company = await CompanyRepository.findById(companyId);

        if (!company) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        const user = await UserRepository.findById(userId, {
            include: [
                {
                    association: 'companies',
                    attributes: ['id']
                }
            ]
        });

        if (!user) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        if (company.user_id !== user.id) {
            return response.sendStatus(HTTP.BAD_REQUEST);
        }

        const applications = await ApplicationRepository.findAndCountAll({
            where: {
                company_id: companyId
            },
            include: [
                {
                    association: 'user',
                    where: {
                        deleted_at: null
                    }
                }
            ],
            offset,
            limit
        });

        return response.status(HTTP.OK).json(applications);
    }
}

module.exports = ApplicationController;

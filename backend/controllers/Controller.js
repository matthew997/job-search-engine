class Controller {
    bindMethods() {
        const proto = Object.getPrototypeOf(this);
        const methods = [
            ...Object.getOwnPropertyNames(Controller.prototype),
            ...Object.getOwnPropertyNames(proto)
        ];

        for (const method of methods) {
            if (typeof this[method] === 'function') {
                this[method] = this[method].bind(this);
            }
        }
    }
}

module.exports = Controller;

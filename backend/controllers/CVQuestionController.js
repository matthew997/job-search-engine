const HTTP = require('http-status-codes');
const CVQuestionRepository = require('../repositories/CVQuestion');
const CVVideoAnswerRepository = require('../repositories/CVVideoAnswer');

const Controller = require('./Controller');

class CVQuestionController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async index(request, response) {
        const questions = await CVQuestionRepository.findAll({
            include: [
                {
                    association: 'video'
                }
            ],
            order: [['index', 'asc']]
        });

        return response.status(HTTP.OK).json(questions);
    }

    async show(request, response) {
        const { id } = request.params;
        const question = await CVQuestionRepository.findById(id);

        if (!question) {
            return response.status(HTTP.NOT_FOUND);
        }

        return response.status(HTTP.OK).json(question);
    }

    async store(request, response) {
        const questionData = request.body;
        const index = await CVQuestionRepository.getLastIndex();

        questionData.index = index + 1;

        const question = await CVQuestionRepository.create(questionData);

        return response.status(HTTP.CREATED).json(question);
    }

    async update(request, response) {
        const { id } = request.params;
        const question = await CVQuestionRepository.findById(id);

        if (!question) {
            return response.status(HTTP.NOT_FOUND);
        }

        await question.update(request.body);

        return response.status(HTTP.OK).json(question);
    }

    async destroy(request, response) {
        const { id } = request.params;

        await CVVideoAnswerRepository.destroy({
            where: {
                question_id: id
            }
        });

        await CVQuestionRepository.destroy({
            where: {
                id
            }
        });

        return response.sendStatus(HTTP.NO_CONTENT);
    }

    async changeOrder(request, response) {
        const { elements = [] } = request.body;

        for (const { id, index } of elements) {
            await CVQuestionRepository.update(
                {
                    index
                },
                {
                    where: {
                        id
                    }
                }
            );
        }

        return response.sendStatus(HTTP.OK);
    }
}
module.exports = CVQuestionController;

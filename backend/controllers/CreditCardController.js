const HTTP = require('http-status-codes');
const sequelize = require('sequelize');
const CreditCardRepository = require('../repositories/CreditCard');

class CreditCardController {
    async index(request, response) {
        const { id: userId } = request.loggedUser;
        const creditCards = await CreditCardRepository.findAll({
            where: {
                user_id: userId,
                deleted_at: null
            },
            order: [['created_at', 'DESC']]
        });

        return response.status(HTTP.OK).json({ creditCards });
    }

    async setDefault(request, response) {
        const { id: userId } = request.loggedUser;
        const { id: cardId } = request.body;

        const card = await CreditCardRepository.findById(cardId);

        if (!card) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        if (card.user_id !== userId) {
            return response.sendStatus(HTTP.FORBIDDEN);
        }

        await CreditCardRepository.setDefaultCard(card);

        return response.sendStatus(HTTP.OK);
    }

    async destroy(request, response) {
        const { id: userId } = request.loggedUser;
        const { id: cardId } = request.params;

        const card = await CreditCardRepository.findById(cardId);

        if (!card) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        if (card.user_id !== userId) {
            return response.sendStatus(HTTP.FORBIDDEN);
        }

        await card.update({
            token: sequelize.fn(
                'concat',
                sequelize.literal('token'),
                '_',
                Date.now()
            ),
            updated_at: new Date(),
            deleted_at: new Date()
        });

        return response.sendStatus(HTTP.NO_CONTENT);
    }
}

module.exports = CreditCardController;

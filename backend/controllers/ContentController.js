const _ = require('lodash');
const HTTP = require('http-status-codes');
const ContentService = require('../services/Content');
const ContentRepository = require('../repositories/Content');
const { Op } = require('sequelize');

const Controller = require('./Controller');

class ContentController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async index(request, response) {
        const container = request.app.get('di');
        const Cache = container.get('cacheService');

        let contents = [];

        let { filters = '{}' } = request.query;

        filters = JSON.parse(filters);

        if (Object.entries(filters).length) {
            contents = await this.getFilteredIndex(filters, Cache);
        } else {
            contents = await this.getFullIndex(Cache);
        }

        return response.status(HTTP.OK).json(contents);
    }

    async show(request, response) {
        const { key } = request.params;

        const container = request.app.get('di');
        const Cache = container.get('cacheService');

        if (await Cache.exists(`content:show:${key}`)) {
            return response
                .status(HTTP.OK)
                .json(await Cache.get(`content:show:${key}`));
        }

        const content = await ContentRepository.findByKey(key);

        if (!content) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        await Cache.set(`content:show:${key}`, content);

        return response.status(HTTP.OK).json(content);
    }

    async update(request, response) {
        const value = request.body;
        const { key } = request.params;

        const container = request.app.get('di');
        const Cache = container.get('cacheService');

        const content = await ContentRepository.findByKey(key);

        if (!content) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        const errors = await ContentService.validate(key, value);

        if (errors.length) {
            return response.status(HTTP.BAD_REQUEST).json({ errors });
        }

        await content.update(
            {
                value,
                updated_at: new Date()
            },
            { fields: ['value', 'updated_at'] }
        );

        await Cache.forgetByPattern('content:*');

        return response.status(HTTP.OK).json(content);
    }

    async getFullIndex(Cache) {
        const cacheKey = 'content:index';

        if (await Cache.exists(cacheKey)) {
            return Cache.get(cacheKey);
        }

        const contents = await ContentRepository.findAll();

        await Cache.set(cacheKey, contents);

        return contents;
    }

    async getFilteredIndex(filters, Cache) {
        const query = [];
        const cacheKeyEntries = [];

        for (const [key, values] of Object.entries(filters)) {
            query.push({
                [key]: {
                    [Op.in]: values
                }
            });

            cacheKeyEntries.push(`${key}|${values.join(',')}`);
        }

        const cacheKey = cacheKeyEntries.join('-');

        if (await Cache.exists(cacheKey)) {
            return Cache.get(cacheKey);
        }

        const contents = await ContentRepository.findAll({
            where: {
                [Op.or]: query
            }
        });

        await Cache.set(cacheKey, contents);

        return contents;
    }
}

module.exports = ContentController;

const HTTP = require('http-status-codes');
const ContentRepository = require('../repositories/Content');
const CompanyRepository = require('../repositories/Company');
const CreditCardRepository = require('../repositories/CreditCard');
const UserRepository = require('../repositories/User');
const PaymentService = require('../services/Payment');
const Controller = require('./Controller');
class TranzilaController extends Controller {
    constructor(tranzila) {
        super();

        this.tranzila = tranzila;

        this.bindMethods();
    }

    async webhook(request, response) {
        const requestData = request.body;

        console.log(`TranzilaWebhook`, requestData);

        if (requestData.Response !== this.tranzila.client.RESPONSE_PAID) {
            return response.sendStatus(HTTP.OK);
        }

        let transaction;

        try {
            transaction = await this.tranzila.verifyTransaction(requestData);

            console.log('*---------------*');
            console.log('transaction => ', transaction);
            console.log('*---------------*');

            if (!transaction) {
                return response.sendStatus(HTTP.OK);
            }
        } catch (error) {
            console.error(error);
        }

        const {
            expyear,
            expmonth,
            cardtype,
            cardissuer,
            TranzilaTK: token
        } = requestData;

        const { returnData, sum } = transaction;
        const { save_card: saveCard = false, user_id: userId } = returnData;

        if (returnData.type === 'subscription-initial-payment') {
            await Promise.all([
                UserRepository.addSubscription(userId),
                CompanyRepository.setAllAsActive(userId),
                PaymentService.create({
                    type: 'subscription',
                    user_id: userId,
                    amount: (parseInt(sum) / 100).toFixed(2)
                })
            ]);
        }

        if (saveCard) {
            const tokenExists = await CreditCardRepository.checkIfTokenExists(
                token
            );

            if (!tokenExists && userId) {
                const cardData = {
                    token,
                    user_id: userId,
                    exp: `${expmonth}/${expyear}`,
                    last_4_digits: token.substr(-4),
                    brand: this.tranzila.getCardBrand(cardtype, cardissuer)
                };

                const card = await CreditCardRepository.create(cardData);

                await CreditCardRepository.setDefaultCard(card);
            }
        }

        return response.sendStatus(HTTP.OK);
    }

    async getAddCardPaymentUrl(request, response) {
        const url = await this.tranzila.getAddCardPaymentUrl(
            request.loggedUser.id
        );

        return response.status(HTTP.OK).send(url);
    }

    async getSubscriptionPaymentUrl(request, response) {
        const {
            value: { monthlyPrice = 0 }
        } = await ContentRepository.findByKey('payment-page'); // @TODO: move to settings?

        if (!monthlyPrice) {
            throw new Error('Monthly price has not been set!');
        }

        const url = await this.tranzila.getSubscriptionPaymentUrl(
            request.loggedUser,
            monthlyPrice
        );

        return response.status(HTTP.OK).send(url);
    }
}

module.exports = TranzilaController;

const HTTP = require('http-status-codes');
const SettingRepository = require('../repositories/Setting');

const Controller = require('./Controller');

class SettingController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async index(request, response) {
        const settings = await SettingRepository.findAll();

        return response.status(HTTP.OK).json(settings);
    }

    async getByTag(request, response) {
        const { tag = '' } = request.params;

        const settings = await SettingRepository.findAll({
            where: {
                tag
            }
        });

        return response.status(HTTP.OK).json(settings);
    }

    async getByKey(request, response) {
        const { key = '' } = request.params;
        const setting = await SettingRepository.findOne({
            where: {
                key
            }
        });

        if (!setting) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        return response.status(HTTP.OK).json(setting);
    }

    async update(request, response) {
        const { settings = [], tag = '' } = request.body;
        const Cache = this.getCache(request.app);

        for (const setting of settings) {
            const { key, value } = setting;

            await SettingRepository.update(
                { value, updated_at: new Date() },
                {
                    where: {
                        key,
                        tag
                    }
                }
            );
        }

        const shouldReloadTranzila = settings.some(({ key }) =>
            key.startsWith('tranzila_')
        );

        if (shouldReloadTranzila) {
            const container = request.app.get('di');
            const tranzilaServiceFactory = container.get(
                'tranzilaServiceFactory'
            );

            const tranzilaService = await tranzilaServiceFactory();

            container.set('tranzilaService', tranzilaService);
        }

        await Cache.forgetByPattern('settings:public*');

        return response.status(HTTP.OK).json();
    }

    async public(request, response) {
        const Cache = this.getCache(request.app);
        const cacheKey = 'settings:public';

        if (await Cache.exists(cacheKey)) {
            return response.status(HTTP.OK).json(await Cache.get(cacheKey));
        }

        const settings = await SettingRepository.getPublic();

        await Cache.set(cacheKey, settings);

        return response.status(HTTP.OK).json(settings);
    }

    async publicKey(request, response) {
        const { key = '' } = request.params;
        const Cache = this.getCache(request.app);

        const cacheKey = `settings:public:${key}`;

        if (await Cache.exists(cacheKey)) {
            return response.status(HTTP.OK).json(await Cache.get(cacheKey));
        }

        const setting = await SettingRepository.getPublicByKey(key);

        await Cache.set(cacheKey, setting);

        return response.status(HTTP.OK).json(setting);
    }

    getCache(app) {
        const container = app.get('di');

        return container.get('cacheService');
    }
}

module.exports = SettingController;

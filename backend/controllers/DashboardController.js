const HTTP = require('http-status-codes');

const UserRepository = require('../repositories/User');
const ApplicationRepository = require('../repositories/Application');
const CompanyRepository = require('../repositories/Company');
const PaymentRepository = require('../repositories/Payment');
const { User } = require('../models');
const sequelize = require('sequelize');
const { Op } = sequelize;

const Controller = require('./Controller');

class DashboardController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async statistics(request, response) {
        const { companyId, range } = request.query;

        const where = {};
        const paymentParams = {};
        let parsedDateRange;

        if (range) {
            try {
                parsedDateRange = JSON.parse(range);
            } catch (e) {
                return response.status(HTTP.BAD_REQUEST);
            }

            where.created_at = {
                [Op.between]: [
                    parsedDateRange.startDate,
                    parsedDateRange.endDate
                ]
            };

            paymentParams.created_at = {
                [Op.between]: [
                    parsedDateRange.startDate,
                    parsedDateRange.endDate
                ]
            };
        }

        if (companyId) {
            const company = await CompanyRepository.findById(companyId);

            if (!company) {
                return response.status(HTTP.NOT_FOUND);
            }

            where.company_id = company.id;
            paymentParams.user_id = company.user_id;
        }

        const [
            applications,
            users,
            applicationCount,
            candidatesCount,
            companiesCount,
            payment
        ] = await Promise.all([
            ApplicationRepository.findAll({
                where,
                attributes: [
                    [sequelize.fn('DATE', sequelize.col('created_at')), 'date'],
                    [sequelize.fn('count', 'id'), 'count'],
                    'status'
                ],
                group: ['status', 'date']
            }),

            ApplicationRepository.count({
                where,
                attributes: [
                    [sequelize.fn('DATE', sequelize.col('created_at')), 'date'],
                    [sequelize.fn('count', 'id'), 'count']
                ],
                group: ['date']
            }),

            ApplicationRepository.count({ where }),

            UserRepository.count({
                where: {
                    created_at: where.created_at,
                    type: User.TYPE_USER
                }
            }),

            UserRepository.count({
                where: {
                    created_at: where.created_at,
                    type: User.TYPE_COMPANY
                }
            }),

            PaymentRepository.amount('amount', {
                where: paymentParams
            })
        ]);

        return response.json({
            applications,
            users,
            candidatesCount,
            applicationCount,
            companiesCount,
            payment
        });
    }
}

module.exports = DashboardController;

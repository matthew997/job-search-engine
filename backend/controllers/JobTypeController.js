const HTTP = require('http-status-codes');
const { Op } = require('sequelize');
const slugify = require('slug');

const JobTypeRepository = require('../repositories/JobType');

const Controller = require('./Controller');

class JobTypeController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async index(request, response) {
        const { homepage, cms } = request.query;
        let include = [];

        if (homepage) {
            include = [
                {
                    association: 'companies',
                    include: [
                        {
                            association: 'country'
                        },
                        {
                            association: 'city'
                        },
                        {
                            association: 'promoteImage'
                        }
                    ],
                    where: {
                        is_on_homepage: true
                    },
                    required: false
                }
            ];
        }

        if (cms) {
            include = [
                {
                    association: 'companies',
                    include: [
                        {
                            association: 'country'
                        },
                        {
                            association: 'city'
                        }
                    ],
                    where: {
                        active: true
                    }
                }
            ];
        }

        const types = await JobTypeRepository.findAll({
            where: {
                deleted_at: {
                    [Op.is]: null
                }
            },
            include
        });

        return response.status(HTTP.OK).json(types);
    }

    async store(request, response) {
        let { name, slug } = request.body;

        slug = slugify(slug.toLowerCase());

        const isSlugAvailable = await JobTypeRepository.isSlugAvailable(slug);

        if (!isSlugAvailable) {
            return response
                .status(HTTP.UNPROCESSABLE_ENTITY)
                .json({ errors: { slug: ['שבלול כבר קיים.'] } });
        }

        let jobType = await JobTypeRepository.findByName(name);

        if (jobType) {
            return response.sendStatus(HTTP.UNPROCESSABLE_ENTITY);
        }

        jobType = await JobTypeRepository.create({
            name,
            slug,
            updated_at: new Date()
        });

        return response.status(HTTP.OK).json(jobType);
    }

    async show(request, response) {
        const { id } = request.params;
        const jobType = await JobTypeRepository.findById(id);

        if (!jobType) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        return response.status(HTTP.OK).json(jobType);
    }

    async destroy(request, response) {
        const { id } = request.params;
        const jobType = await JobTypeRepository.findById(id);

        if (jobType) {
            await jobType.update({
                deleted_at: new Date()
            });
        }

        return response.sendStatus(HTTP.NO_CONTENT);
    }

    async update(request, response) {
        const { id } = request.params;
        const { name, slug } = request.body;

        const jobType = await JobTypeRepository.findById(id);

        if (!jobType) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        if (name && slug) {
            await jobType.update(
                {
                    name,
                    slug,
                    updated_at: new Date()
                },
                {
                    fields: ['name', 'slug']
                }
            );
        }

        return response.sendStatus(HTTP.OK);
    }
}

module.exports = JobTypeController;

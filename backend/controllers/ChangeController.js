const HTTP = require('http-status-codes');

const models = require('../models');
const { Op } = require('sequelize');
const Sorting = require('../services/Sorting');
const ChangeRepository = require('../repositories/Change');
const Controller = require('./Controller');

class ChangeController extends Controller {
    constructor(MailService) {
        super();

        this.MailService = MailService;
        this.bindMethods();
    }

    async index(request, response) {
        let {
            page = 1,
            perPage = 10,
            q = '',
            sortBy = null,
            order: sortOrder = 'DESC'
        } = request.query;

        page = parseInt(page);
        perPage = parseInt(perPage);

        const offset = (page - 1) * perPage;
        const limit = perPage;

        const fieldIncludesQueryString = {
            [Op.like]: `%${q}%`
        };

        const where = {};

        if (q) {
            where[Op.or] = [
                {
                    ['$company.name$']: fieldIncludesQueryString
                },
                {
                    ['$user.companies.name$']: fieldIncludesQueryString
                },
                {
                    ['status']: fieldIncludesQueryString
                }
            ];
        }

        let order = [['created_at', 'DESC']];

        if (sortBy) {
            order = Sorting.handle(sortBy, sortOrder, 'model');
        }

        const include = [
            {
                association: 'company',
                attributes: ['id', 'name'],
                include: [
                    {
                        association: 'user',
                        attributes: ['id']
                    }
                ]
            },
            {
                association: 'user',
                attributes: ['id'],
                include: [
                    {
                        association: 'companies',
                        attributes: ['id', 'name']
                    }
                ]
            }
        ];

        const includeWithoutAttributes = ChangeRepository.removeIncludeAttributes(
            include
        );

        let rows = await ChangeRepository.findAll({
            attributes: ['id'],
            subQuery: false,
            where,
            order,
            include: includeWithoutAttributes,
            limit,
            order,
            offset,
            group: ['id']
        });

        rows = await ChangeRepository.findAll({
            subQuery: false,
            where: {
                id: rows.map(({ id }) => id)
            },
            order,
            include
        });

        const count = await ChangeRepository.count({
            distinct: true,
            where,
            include
        });

        return response.status(HTTP.OK).send({ count, rows });
    }

    async setStatus(request, response) {
        const { id: changeId } = request.params;
        const { status } = request.body;

        const change = await ChangeRepository.findById(changeId, {
            include: [
                {
                    association: 'user',
                    attributes: [
                        'email',
                        'first_name',
                        'last_name',
                        'full_name'
                    ],
                    required: false
                },
                {
                    association: 'company',
                    attributes: ['name'],
                    required: false,
                    include: [
                        {
                            association: 'user',
                            attributes: ['email']
                        }
                    ]
                }
            ]
        });

        if (!change) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        await change.update({
            status
        });

        if (status === models.Change.STATUS_APPROVED) {
            const { type: model, value: changes } = change;
            const modelId = change.company_id || change.user_id;

            await models[model].publishChanges(modelId, changes);
        }

        const replacements = {
            status
        };

        replacements.name = change.user
            ? change.user.full_name
            : change.company.name;

        await this.MailService.triggerEvent(
            'CHANGE_STATUS',
            change.user ? change.user.email : change.company.user.email,
            replacements,
            request.app
        );

        return response.sendStatus(HTTP.OK);
    }

    async bulk(request, response) {
        const allowedActions = ['delete'];
        const { name: action, ids = [] } = request.body;

        if (allowedActions.includes(action) && ids.length) {
            const where = {
                id: {
                    [Op.in]: ids
                }
            };

            if (action === 'delete') {
                await ChangeRepository.destroy({ where });
            }
        }

        return response.sendStatus(HTTP.OK);
    }
}

module.exports = ChangeController;

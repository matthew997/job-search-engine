const CompanyRepository = require('../repositories/Company');
const Controller = require('./Controller');

class UserCompanyController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async index(request, response) {
        const { id: userId } = request.loggedUser;

        const companies = await CompanyRepository.findAllForUser(userId);

        return response.send(companies);
    }
}

module.exports = UserCompanyController;

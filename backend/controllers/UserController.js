const HTTP = require('http-status-codes');
const crypto = require('crypto');
const moment = require('moment');

const config = require('../config');
const UserRepository = require('../repositories/User');
const ChangeRepository = require('../repositories/Change');
const UserLanguageRepository = require('../repositories/UserLanguage');
const RoleRepository = require('../repositories/Role');
const UserDetailsRepository = require('../repositories/UserDetails');
const ApplicationRepository = require('../repositories/Application');
const PassportRepository = require('../repositories/Passport');
const ReviewRepository = require('../repositories/Review');
const CreditCardRepository = require('../repositories/CreditCard');
const PaymentRepository = require('../repositories/Payment');
const CVVideoAnswerRepository = require('../repositories/CVVideoAnswer');

const { User, Role } = require('../models');
const UserService = require('../services/User');
const StoreMediaFromFrontendHandler = require('../services/StoreMediaFromFrontendHandler');
const Sequelize = require('sequelize');
const { Op } = Sequelize;

const Controller = require('./Controller');

class UserController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async me(request, response) {
        const { id: userId } = request.loggedUser;
        const { role } = request.query;

        const user = await UserService.getMe(userId, role);

        if (!user) {
            return response
                .status(HTTP.UNAUTHORIZED)
                .json({ error: 'No user found' });
        }

        return response.status(HTTP.OK).json(user);
    }

    async index(request, response) {
        let {
            page = 1,
            perPage = 25,
            q = '',
            sortBy = 'created_at',
            order = 'desc',
            role = 'user',
            type = '',
            status = '',
            deleted = null
        } = request.query;

        const where = {};

        const fieldIncludesQueryString = {
            [Op.like]: `%${q}%`
        };

        if (q) {
            where[Op.or] = [
                {
                    first_name: fieldIncludesQueryString
                },
                {
                    last_name: fieldIncludesQueryString
                },
                {
                    email: fieldIncludesQueryString
                },
                {
                    phone: fieldIncludesQueryString
                }
            ];
        }

        if (role === 'admin') {
            const adminRole = await RoleRepository.findOne({
                where: {
                    name: Role.ADMIN
                }
            });

            where['$roleUser.role_id$'] = {
                [Op.in]: [adminRole.id]
            };
        } else {
            const userRole = await RoleRepository.findOne({
                where: {
                    name: Role.USER
                }
            });

            where['$roleUser.role_id$'] = {
                [Op.in]: [userRole.id]
            };
        }

        const include = [
            {
                association: 'roleUser',
                attributes: []
            },
            {
                association: 'application',
                attributes: []
            },
            {
                association: 'passports',
                attributes: ['country']
            }
        ];

        if (type) {
            where.type = type;

            if (status === 'goldOnly') {
                include.push({
                    association: 'details',
                    attributes: ['availability'],
                    where: {
                        experience: {
                            [Op.in]: ['7-12m', '1-3y', '3y']
                        }
                    }
                });
            } else if (status === 'regularOnly') {
                include.push({
                    association: 'details',
                    attributes: ['availability'],
                    where: {
                        [Op.or]: [
                            {
                                experience: '1-6m'
                            },
                            {
                                experience: null
                            }
                        ]
                    }
                });
            } else {
                include.push({
                    association: 'details',
                    attributes: ['availability']
                });
            }
        }

        page = parseInt(page);
        perPage = parseInt(perPage);

        const offset = (page - 1) * perPage;
        const limit = parseInt(perPage);

        let paranoid = true;

        if (deleted) {
            where.deleted_at = {
                [Op.not]: null
            };
            paranoid = false;
        }

        if (sortBy === 'application_count') {
            sortBy = Sequelize.literal('`application_count`');
        }

        const rows = await UserRepository.findAll({
            distinct: true,
            where,
            offset,
            limit,
            attributes: {
                ...User.DISPLAYABLE_FIELDS,
                include: [
                    [
                        Sequelize.fn('COUNT', Sequelize.col('application.id')),
                        'application_count'
                    ]
                ]
            },
            include,
            order: [[sortBy, order]],
            subQuery: false,
            paranoid,
            group: type
                ? ['User.id', 'details.id', 'passports.id']
                : ['User.id', 'passports.id']
        });

        const count = await UserRepository.count({
            distinct: true,
            where,
            include,
            paranoid
        });

        return response.status(HTTP.OK).json({
            users: {
                rows,
                count
            }
        });
    }

    async show(request, response) {
        const user = await UserRepository.findById(request.params.id, {
            attributes: User.DISPLAYABLE_FIELDS,
            include: [
                {
                    association: 'details'
                },
                {
                    association: 'passports'
                },
                {
                    association: 'application',
                    include: [
                        {
                            association: 'company',
                            attributes: ['name'],
                            include: [
                                {
                                    association: 'country',
                                    attributes: ['name']
                                },
                                {
                                    association: 'city',
                                    attributes: ['name']
                                }
                            ]
                        }
                    ]
                },
                {
                    association: 'languages'
                },
                {
                    association: 'reviews'
                },
                {
                    association: 'CVVideoAnswers'
                }
            ],
            paranoid: false
        });

        if (!user) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        return response.status(HTTP.OK).json(user);
    }

    async store(request, response) {
        const { email, first_name, last_name, phone = null } = request.body;

        const isEmailAlreadyTaken = await UserRepository.findOne({
            where: {
                email
            }
        });

        if (isEmailAlreadyTaken) {
            const errors = {
                email: ['דוא"ל כבר נלקח.']
            };

            return response.status(HTTP.UNPROCESSABLE_ENTITY).json({ errors });
        }

        const isPhoneAlreadyExisted = await UserRepository.findOne({
            where: {
                phone
            }
        });

        if (isPhoneAlreadyExisted) {
            const errors = {
                phone: ['הטלפון כבר היה קיים']
            };

            return response.status(HTTP.UNPROCESSABLE_ENTITY).json({ errors });
        }

        const user = await UserRepository.create({
            email,
            password: null,
            first_name,
            last_name,
            phone,
            registration_completed: true,
            created_at: new Date(),
            updated_at: new Date()
        });

        const adminRole = await RoleRepository.findOne({
            where: {
                name: Role.ADMIN
            }
        });

        await user.setRoles([adminRole]);

        return response.status(HTTP.OK).json(user);
    }

    async destroy(request, response) {
        const { id } = request.params;

        const user = await UserRepository.findById(id);

        if (user) {
            await UserRepository.destroy({
                where: { id }
            });

            const where = {
                user_id: id
            };

            await Promise.all([
                UserDetailsRepository.destroy({ where }),
                ApplicationRepository.destroy({ where }),
                PassportRepository.destroy({ where }),
                ReviewRepository.destroy({ where }),
                CreditCardRepository.destroy({ where }),
                PaymentRepository.destroy({ where }),
                UserLanguageRepository.destroy({ where }),
                CVVideoAnswerRepository.destroy({ where })
            ]);
        }

        return response.sendStatus(HTTP.NO_CONTENT);
    }

    async restore(request, response) {
        const { id } = request.params;

        await UserRepository.restore({
            where: { id }
        });

        const user = await UserRepository.findById(id);

        if (!user) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }
        
        return response.sendStatus(HTTP.OK);
    }

    async update(request, response) {
        const { id } = request.params;
        const data = { ...request.body };

        const user = await UserRepository.findById(id);

        if (!user) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        const isEmailAlreadyTaken = await UserRepository.findOne({
            where: {
                email: data.email,
                id: {
                    [Op.not]: id
                }
            }
        });

        if (isEmailAlreadyTaken) {
            const errors = {
                email: ['דוא"ל כבר נלקח.']
            };

            return response.status(HTTP.UNPROCESSABLE_ENTITY).json({ errors });
        }

        await user.update(
            { ...data, updated_at: new Date() },
            {
                fields: [
                    'first_name',
                    'last_name',
                    'email',
                    'phone',
                    'pin',
                    'pin_generated_at',
                    'updated_at'
                ]
            }
        );

        return response.status(HTTP.OK).json(user);
    }

    async profile(request, response) {
        const { cms } = request.body;
        const { id } = request.session.user;

        const user = await UserRepository.getMe(id, cms);

        if (!user) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        const isEmailAlreadyTaken = await UserRepository.findOne({
            where: {
                email: request.body.email,
                id: {
                    [Op.not]: id
                }
            }
        });

        if (isEmailAlreadyTaken) {
            const errors = {
                email: ['דוא"ל כבר נלקח.']
            };

            return response.status(HTTP.UNPROCESSABLE_ENTITY).json({ errors });
        }

        await user.update(
            { ...request.body, updated_at: new Date() },
            {
                fields: [
                    'first_name',
                    'last_name',
                    'email',
                    'phone',
                    'updated_at'
                ]
            }
        );

        return response.status(HTTP.OK).json(user);
    }

    async updatePassword(request, response) {
        const {
            current_password,
            password,
            password_confirmation,
            cms
        } = request.body;

        const { id } = request.session.user;

        const user = await UserRepository.getMe(id, cms);

        if (!user) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        const isCurrentPasswordCorrect = await UserRepository.login(
            user.email,
            current_password || '',
            cms
        );

        if (!isCurrentPasswordCorrect) {
            const errors = {
                current_password: ['Current password is invalid.']
            };

            return response.status(HTTP.UNPROCESSABLE_ENTITY).json({ errors });
        }

        const validationErrors = [];

        if (password.length < 6) {
            validationErrors.push(
                'Password cannot contain less than 6 characters!'
            );
        }

        if (!/\d/.test(password)) {
            validationErrors.push('Password must contain numbers and letters!');
        }

        if (password !== password_confirmation) {
            validationErrors.push(
                'Password confirmation must be equal to Password'
            );
        }

        if (validationErrors.length) {
            return response
                .status(HTTP.UNPROCESSABLE_ENTITY)
                .json({ errors: validationErrors });
        }

        await user.update(
            { ...request.body, updated_at: new Date() },
            {
                fields: ['password', 'updated_at']
            }
        );

        return response.status(HTTP.OK).json(user);
    }

    async getProfileInformation(request, response) {
        const { id: userId, type: userType } = request.loggedUser;
        const profile = await UserRepository.getProfileInformation(
            userId,
            userType
        );

        return response.status(HTTP.OK).json(profile);
    }

    async updateUserDetails(request, response) {
        const { id: userId } = request.loggedUser;
        const {
            personal,
            details,
            passports = [],
            reviews = []
        } = request.body;
        const ONE_TO_SIX_MONTHS_EXPERIENCE = '1-6m';

        const user = await UserRepository.findById(userId, {
            include: [
                {
                    association: 'details'
                },
                {
                    association: 'passports'
                },
                {
                    association: 'reviews'
                }
            ]
        });

        await user.update(personal, {
            fields: [
                'first_name',
                'last_name',
                'email',
                'phone',
                'birthday',
                'gender',
                'marital_status',
                'hometown',
                'updated_at'
            ]
        });

        if (
            !details.hasExperience ||
            details.experience === ONE_TO_SIX_MONTHS_EXPERIENCE
        ) {
            await ReviewRepository.destroy({
                where: {
                    user_id: userId
                }
            });
        }

        await user.details.update(details, {
            fields: [
                'education',
                'experience',
                'experience_job_type',
                'service',
                'availability',
                'updated_at'
            ]
        });

        await this.saveReviews(user, reviews);
        await this.savePassports(user, passports);
        await UserLanguageRepository.setUserLanguages(user, details.languages);

        const profile = await UserRepository.getProfileInformation(
            user.id,
            user.type
        );

        return response.status(HTTP.OK).json(profile);
    }

    async updateRecruiterDetails(request, response) {
        const { id: userId } = request.loggedUser;

        const user = await UserRepository.findById(userId);

        if (!user) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        await ChangeRepository.addForUser(user.id, request.body, User);

        const profile = await UserRepository.getProfileInformation(
            user.id,
            user.type
        );

        return response.status(HTTP.OK).json(profile);
    }

    async saveReviews(user, reviews = []) {
        for (const review of reviews) {
            if (review.id) {
                const existingReview = user.reviews.find(
                    ({ id }) => id === review.id
                );

                if (existingReview) {
                    await existingReview.update(review);
                }
            } else {
                await user.createReview(review);
            }
        }
    }

    async savePassports(user, passports = []) {
        for (const passport of passports) {
            if (passport.id) {
                const existingPassport = user.passports.find(
                    ({ id }) => id === passport.id
                );

                if (existingPassport) {
                    await existingPassport.update(passport);
                }
            } else {
                await user.createPassport(passport);
            }
        }
    }

    async updateImage(request, response) {
        const { id } = request.loggedUser;
        const { image } = request.files;

        const processedImage = await StoreMediaFromFrontendHandler.handle(
            image,
            id
        );

        if (!processedImage) {
            return response
                .status(HTTP.BAD_REQUEST)
                .json({ error: 'קובץ שגוי לתמונת משתמש' });
        }

        const user = await UserRepository.findById(id);

        if (!user) {
            return response
                .status(HTTP.UNAUTHORIZED)
                .json({ error: 'לא נמצא משתמש' });
        }

        await user.update({
            image_id: processedImage.id
        });

        user.image = processedImage;

        return response.status(HTTP.OK).json(processedImage);
    }

    async bulk(request, response) {
        const allowedActions = ['delete'];
        const { name: action, ids = [] } = request.body;

        if (allowedActions.includes(action) && ids.length) {
            await UserRepository.destroy({
                where: {
                    id: {
                        [Op.in]: ids
                    }
                }
            });
        }

        return response.sendStatus(HTTP.OK);
    }

    async updateUserCredits(request, response) {
        const { id } = request.body;

        const user = await UserRepository.findById(id);

        if (!user) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        await user.update(
            { ...request.body, updated_at: new Date() },
            {
                fields: [
                    'application_credits',
                    'cancellation_credits',
                    'updated_at'
                ]
            }
        );

        return response.status(HTTP.OK).json({});
    }
}

module.exports = UserController;

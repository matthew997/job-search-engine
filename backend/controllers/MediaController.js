const moment = require('moment');
const path = require('path');
const { Op } = require('sequelize');
const HTTP = require('http-status-codes');
const ms = require('ms');

const MediaRepository = require('../repositories/Media');
const StoreMediaHandler = require('../services/StoreMediaHandler');
const DeleteMediaHandler = require('../services/DeleteMediaHandler');
const CVQuestionRepository = require('../repositories/CVQuestion');

const Controller = require('./Controller');

class MediaController extends Controller {
    constructor(videoUploadQueue) {
        super();

        this.videoUploadQueue = videoUploadQueue;

        this.bindMethods();
    }

    async index(request, response) {
        let {
            page = 1,
            perPage = 25,
            name = null,
            type = null,
            date = null
        } = request.query;
        const { loggedUser } = request;
        const conditions = [];
        let where = {};

        if (name) {
            conditions.push({
                name: {
                    [Op.like]: `%${name}%`
                }
            });
        }

        if (type) {
            conditions.push({
                type
            });
        }

        if (date) {
            const separatorsCount = date.split('-').length;

            const timeUnits = ['year', 'month', 'day'];
            const timeUnit = timeUnits[separatorsCount - 1] || 'year';

            const from = moment(date, 'YYYY-MM-DD HH:mm:ss').startOf(timeUnit);
            const to = moment(date, 'YYYY-MM-DD HH:mm:ss').endOf(timeUnit);

            conditions.push({
                created_at: {
                    [Op.between]: [from, to]
                }
            });
        }

        page = parseInt(page);
        perPage = parseInt(perPage);

        const offset = (page - 1) * perPage;
        const limit = perPage;

        if (conditions.length) {
            where = {
                [Op.and]: conditions
            };
        }

        where.user_id = loggedUser.id;

        const media = await MediaRepository.findAndCountAll({
            where,
            offset,
            limit,
            order: [['created_at', 'DESC']],
            include: [{ association: 'user' }]
        });

        return response.status(HTTP.OK).json({ media });
    }

    async store(request, response) {
        const { id: userId } = request.loggedUser;
        const storedMedias = [];
        const { files } = request;

        for (const fileName in files) {
            const file = files[fileName];

            const type = this.getMediaType(file);

            if (type) {
                const {
                    filename,
                    extension,
                    name,
                    date,
                    meta = {}
                } = await StoreMediaHandler.handle(file, type);

                const storedMedia = await MediaRepository.create({
                    name,
                    alt: name,
                    filename,
                    extension,
                    type,
                    size: file.size,
                    date,
                    user_id: userId,
                    meta,
                    created_at: new Date(),
                    updated_at: new Date()
                });

                if (type === 'video') {
                    await this.videoUploadQueue.add(
                        {
                            file: this.getVideoFullPath(
                                storedMedia.date,
                                storedMedia.filename,
                                storedMedia.extension
                            ),
                            update: {
                                model: 'Media',
                                field: 'stream',
                                where: {
                                    id: storedMedia.id
                                }
                            }
                        },
                        {
                            delay: ms('5s')
                        }
                    );
                }

                storedMedias.push(storedMedia);
            }
        }

        return response.status(HTTP.OK).json(storedMedias);
    }

    getVideoFullPath(date, video, extension) {
        return path.join(
            __dirname,
            `./../public/media/videos/${date}/${video}.${extension}`
        );
    }

    async show(request, response) {
        const { id } = request.params;
        const media = await MediaRepository.findById(id);

        if (!media) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        return response.status(HTTP.OK).json(media);
    }

    async update(request, response) {
        const { name, alt } = request.body;
        const media = await MediaRepository.findById(request.params.id);

        if (!media) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        await media.update(
            {
                name,
                alt,
                updated_at: new Date()
            },
            { fields: ['name', 'alt', 'updated_at'] }
        );

        return response.status(HTTP.OK).json(media);
    }

    async destroy(request, response) {
        const { id } = request.params;
        const media = await MediaRepository.findById(id);

        if (!media) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        const isImageUsed = await MediaRepository.checkIfImageIsUsed(id);

        if (isImageUsed) {
            return response
                .status(HTTP.UNPROCESSABLE_ENTITY)
                .json({ error: 'משתמשים בתמונה.' });
        }

        const isVideoUsed = await CVQuestionRepository.findOne({
            where: {
                video_example: id
            }
        });

        if (isVideoUsed) {
            return response
                .status(HTTP.UNPROCESSABLE_ENTITY)
                .json({ error: 'הסרטון נמצא בשימוש.' });
        }

        await DeleteMediaHandler.handle(media);

        await media.destroy({
            where: {
                id
            }
        });

        return response.sendStatus(HTTP.OK);
    }

    getMediaType(file) {
        const [type] = file.mimetype.split('/');
        const allowedTypes = ['image', 'video'];

        if (allowedTypes.includes(type)) {
            return type;
        }

        return null;
    }
}

module.exports = MediaController;

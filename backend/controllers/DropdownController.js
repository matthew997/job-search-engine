const HTTP = require('http-status-codes');

const DropdownRepository = require('../repositories/Dropdown');
const DropdownOptionRepository = require('../repositories/DropdownOption');

const { DropdownOption } = require('../models');

const Controller = require('./Controller');

class DropdownController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async index(request, response) {
        const dropdowns = await DropdownRepository.findAll({
            include: [
                {
                    association: 'options'
                }
            ],
            order: [[{ model: DropdownOption, as: 'options' }, 'order', 'ASC']]
        });

        return response.status(HTTP.OK).json(dropdowns);
    }

    async show(request, response) {
        const { name } = request.params;
        const dropdown = await DropdownRepository.findOneByName(name, {
            include: [
                {
                    association: 'options'
                }
            ],
            order: [[{ model: DropdownOption, as: 'options' }, 'order', 'ASC']]
        });

        if (!dropdown) {
            return response.status(HTTP.NOT_FOUND);
        }

        return response.status(HTTP.OK).json(dropdown);
    }

    async store(request, response) {
        const { name, display_name } = request.body;
        const dropdown = await DropdownRepository.create({
            name,
            display_name
        });

        return response.status(HTTP.OK).json(dropdown);
    }

    async update(request, response) {
        const { name: originalName } = request.params;
        const { name, display_name } = request.body;
        const dropdown = await DropdownRepository.findOneByName(originalName);

        if (!dropdown) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        if (name !== originalName) {
            await DropdownOptionRepository.update(
                {
                    name,
                    display_name
                },
                {
                    where: {
                        parent: originalName
                    }
                }
            );
        }

        await dropdown.update({
            name,
            display_name
        });

        return response.status(HTTP.OK).json(dropdown);
    }

    async destroy(request, response) {
        const { name } = request.params;
        const dropdown = await DropdownRepository.findOneByName(name);

        if (dropdown) {
            await DropdownOptionRepository.destroy({
                where: {
                    parent: name
                }
            });

            await dropdown.destroy();
        }

        return response.sendStatus(HTTP.NO_CONTENT);
    }

    async updateOptions(request, response) {
        const { name } = request.params;
        const { options } = request.body;
        const dropdown = await DropdownRepository.findOneByName(name);

        if (!dropdown) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        await DropdownOptionRepository.replaceOptions(name, options);

        return response.status(HTTP.OK).json(options);
    }
}

module.exports = DropdownController;

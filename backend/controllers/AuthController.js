const crypto = require('crypto');
const moment = require('moment');
const HTTP = require('http-status-codes');
const config = require('../config');
const UserService = require('../services/User');
const { User } = require('../models');
const PhoneService = require('../services/Phone');
const { PasswordReset } = require('../models');
const UserRepository = require('../repositories/User');
const PassportRepository = require('../repositories/Passport');
const UserDetailsRepository = require('../repositories/UserDetails');
const UserLanguageRepository = require('../repositories/UserLanguage');
const UserRegistrationRepository = require('../repositories/UserRegistration');
const UserRegistrationService = require('../services/UserRegistration');
const ReviewRepository = require('../repositories/Review');

const Controller = require('./Controller');

class AuthController extends Controller {
    constructor(MailService) {
        super();

        this.MailService = MailService;
        this.bindMethods();
    }

    async register(request, response) {
        const registerData = request.body;

        registerData.type = User.TYPE_USER;

        if (request.route.path === '/api/auth/register-company') {
            registerData.type = User.TYPE_COMPANY;
        }

        const user = await UserRepository.register(registerData);
        request.session.registeredUserId = user.id;

        await UserRegistrationRepository.create({
            user_id: user.id,
            first_step: true
        });

        return response.status(HTTP.OK).json(user);
    }

    async patchRegistered(request, response) {
        const {
            first_name,
            last_name,
            email,
            birthday,
            marital_status,
            gender,
            hometown,
            phone
        } = request.body;

        const registeredUserId = request.session
            ? request.session.registeredUserId
            : null;
        const user = await UserRepository.findById(registeredUserId);

        if (!user) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        await user.update({
            first_name,
            last_name,
            email,
            birthday,
            marital_status,
            gender,
            hometown,
            phone
        });

        return response.status(HTTP.OK).json(user);
    }

    async login(request, response) {
        const { phone, pin } = request.body;

        const user = await UserService.login(phone, pin);

        if (!user) {
            return response.sendStatus(HTTP.UNAUTHORIZED);
        }

        if (request.session.users && !request.session.users.includes(user.id)) {
            request.session.users.push(user.id);
        } else if (!request.session.users) {
            request.session.users = [user.id];
        }

        await user.update({
            pin: null,
            last_login: new Date()
        });

        request.session.registeredUserId = null;

        return response.status(HTTP.OK).json(user);
    }

    async checkPasswordReset(request, response) {
        const { token } = request.params;

        const passwordReset = await PasswordReset.findOne({
            where: {
                token
            }
        });

        if (!passwordReset || passwordReset.isExpired()) {
            return response.sendStatus(HTTP.FORBIDDEN);
        }

        return response.sendStatus(HTTP.OK);
    }

    async passwordResetLink(request, response) {
        const { email } = request.body;

        const user = await UserRepository.findOne({
            where: {
                email
            }
        });

        if (!user) {
            return response.sendStatus(HTTP.OK);
        }

        const buffer = crypto.randomBytes(32);
        const token = buffer.toString('hex');

        await user.createPasswordReset({
            token,
            expires_at: moment().add(2, 'days'),
            created_at: new Date(),
            updated_at: new Date()
        });

        const link = `${config.app.adminUrl}/reset-password/${token}`;
        const content = `Hello ${user.first_name}! Please reset your password following this link: ${link}`;

        await this.MailService.triggerEvent(
            email,
            'Reset password.',
            content,
            request.app
        );

        return response.sendStatus(HTTP.OK);
    }

    async passwordReset(request, response) {
        const { token } = request.params;
        const { password, password_confirmation } = request.body;
        const errors = [];

        if (password.length < 6) {
            errors.push('Password cannot contain less than 6 characters!');
        }

        if (!/\d/.test(password)) {
            errors.push('Password must contain numbers and letters!');
        }

        if (password !== password_confirmation) {
            errors.push('Password confirmation must be equal to Password');
        }

        if (errors.length) {
            return response.status(HTTP.UNPROCESSABLE_ENTITY).json({ errors });
        }

        const passwordReset = await PasswordReset.findOne({
            where: {
                token
            }
        });

        if (passwordReset.isExpired()) {
            return response.sendStatus(HTTP.FORBIDDEN);
        }

        const user = await passwordReset.getUser();

        if (!user) {
            return response.sendStatus(HTTP.UNPROCESSABLE_ENTITY);
        }

        await user.update({
            password
        });

        await passwordReset.update({
            expires_at: new Date()
        });

        return response.sendStatus(HTTP.OK);
    }

    async logout(request, response, next) {
        if (request.session) {
            request.session.destroy(err => {
                if (err) {
                    next(err);
                }
            });
        }

        return response.status(HTTP.OK).json({});
    }

    async requestPin(request, response) {
        const { phone = '', origin } = request.body;
        const user = await UserService.findByPhone(phone, origin);

        if (!user) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        if (User.TEST_PHONES.includes(phone)) {
            await user.update({
                pin: config.twilio.defaultPin
            });

            return response.sendStatus(HTTP.OK);
        }

        if (PhoneService.canSendPin(user.pin_generated_at)) {
            const error = await UserService.sendPin(user.phone);

            if (error) {
                return response
                    .status(HTTP.UNPROCESSABLE_ENTITY)
                    .json({ error });
            }

            return response.sendStatus(HTTP.OK);
        } else {
            return response.status(HTTP.FORBIDDEN).json({
                error: 'עליך להמתין זמן מה לפני שתייצר את הסיכה שוב.'
            });
        }
    }

    async checkPin(request, response) {
        const { pin = '', phone = '' } = request.body;

        const user = await UserRepository.findOneByPhone(phone);

        if (!user) {
            return response.status(HTTP.NOT_FOUND).json({});
        }

        if (user.pin === pin) {
            await user.update({ pin: null, pin_generated_at: null });

            return response.sendStatus(HTTP.OK);
        }

        return response.status(HTTP.FORBIDDEN).json({});
    }

    async userFinishRegistration(request, response) {
        const { id: user_id } = request.loggedUser;
        const {
            name,
            expiration_date,
            issued_on,
            has_foreign_passport,
            has_american_visa
        } = request.body.passport;
        const {
            education,
            experience,
            experience_job_type,
            service,
            availability,
            languages = []
        } = request.body.more_details;
        const { reviews, foreign_passports } = request.body;

        const user = await UserRepository.findById(user_id);

        if (!user) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        const date = new Date();

        const passport = await PassportRepository.create({
            user_id,
            name,
            expiration_date,
            issued_on,
            has_foreign_passport,
            has_american_visa,
            created_at: date,
            updated_at: date
        });

        const foreignPassports = [];

        if (foreign_passports.length) {
            for (const foreignPassport of request.body.foreign_passports) {
                const passport = await PassportRepository.create({
                    user_id,
                    name: foreignPassport.name,
                    expiration_date: foreignPassport.expiration_date,
                    issued_on: foreignPassport.issued_on,
                    country: foreignPassport.country,
                    created_at: date,
                    updated_at: date
                });

                foreignPassports.push(passport);
            }
        }

        const reviewsArray = [];

        if (reviews.length) {
            for (const review of request.body.reviews) {
                const currentReview = await ReviewRepository.create({
                    user_id,
                    start_date: review.start_date,
                    end_date: review.end_date,
                    rating: review.rating,
                    company: review.company,
                    country: review.country,
                    city: review.city,
                    phone: review.phone,
                    phone_2: review.phone_2,
                    content: review.content,
                    is_recommended: review.is_recommended,
                    created_at: date,
                    updated_at: date
                });

                reviewsArray.push(currentReview);
            }
        }

        await UserLanguageRepository.setUserLanguages(user, languages);

        const moreDetails = await UserDetailsRepository.create({
            user_id,
            education,
            experience,
            experience_job_type,
            service,
            availability,
            created_at: date,
            updated_at: date
        });

        await UserRegistrationService.updateStepByUserId(
            user.id,
            'second_step',
            true
        );

        return response
            .status(HTTP.OK)
            .json({ passport, foreignPassports, moreDetails, reviewsArray });
    }

    async addUserSession(request, response) {
        const { id: userId } = request.params;

        if (request.session && !request.session.users.includes(userId)) {
            request.session.users.push(userId);
        }

        return response.sendStatus(HTTP.OK);
    }
}

module.exports = AuthController;

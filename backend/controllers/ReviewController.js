const HTTP = require('http-status-codes');
const ReviewRepository = require('../repositories/Review');
const Sequelize = require('sequelize');
const { Op } = Sequelize;

const Controller = require('./Controller');

class ReviewController extends Controller {
    constructor() {
        super();

        this.bindMethods();
    }

    async index(request, response) {
        let { page = 1, perPage = 25, status } = request.query;

        const where = {};

        page = parseInt(page);
        perPage = parseInt(perPage);

        const offset = (page - 1) * perPage;
        const limit = perPage;

        if (status === 'approved') {
            where.company_id = {
                [Op.not]: null
            };
        } else {
            where.company_id = {
                [Op.eq]: null
            };
        }

        const reviews = await ReviewRepository.findAndCountAll({
            where,
            offset,
            limit,
            include: [
                {
                    association: 'user',
                    attributes: ['first_name', 'last_name', 'full_name'],
                    required: true
                },
                {
                    association: 'companies'
                }
            ]
        });

        return response.status(HTTP.OK).json(reviews);
    }

    async update(request, response) {
        const { id } = request.params;
        const { company_id } = request.body;

        const review = await ReviewRepository.findById(id);

        if (!review) {
            return response.sendStatus(HTTP.NOT_FOUND);
        }

        await review.update({
            company_id
        });

        return response.status(HTTP.OK).json(review);
    }

    async destroy(request, response) {
        const { id } = request.params;

        const review = await ReviewRepository.findById(id);

        if (review) {
            await review.destroy();
        }

        return response.sendStatus(HTTP.NO_CONTENT);
    }
}

module.exports = ReviewController;

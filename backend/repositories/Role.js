const BaseModelRepository = require('./BaseModelRepository');
const { Role } = require('../models');

class RoleRepository extends BaseModelRepository {
    static get model() {
        return Role;
    }
}

module.exports = RoleRepository;

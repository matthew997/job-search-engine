const BaseModelRepository = require('./BaseModelRepository');
const { UserDetail } = require('../models');

class UserDetailsRepository extends BaseModelRepository {
    static get model() {
        return UserDetail;
    }

    static findOneByUserId(id) {
        return this.findOne({
            where: {
                user_id: id
            }
        });
    }

    static destroy(options = {}) {
        return this.update(
            {
                deleted_at: new Date()
            },
            {
                ...options
            }
        );
    }
}

module.exports = UserDetailsRepository;

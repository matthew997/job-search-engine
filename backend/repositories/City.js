const BaseModelRepository = require('./BaseModelRepository');
const { City } = require('../models');

class CityRepository extends BaseModelRepository {
    static get model() {
        return City;
    }

    static async findByName(name) {
        return this.findOne({
            where: {
                name
            }
        });
    }
}

module.exports = CityRepository;

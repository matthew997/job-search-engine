const BaseModelRepository = require('./BaseModelRepository');
const { Change } = require('../models');

class ChangeRepository extends BaseModelRepository {
    static get model() {
        return Change;
    }

    static findOneByCompanyId(id) {
        return this.findOne({
            where: {
                company_id: id
            }
        });
    }

    static findOneByUserId(id) {
        return this.findOne({
            where: {
                user_id: id
            }
        });
    }

    static addForCompany(id, data, model) {
        const changes = this.getChanges(data, model);

        return this.create({
            company_id: id,
            type: model.name,
            value: changes,
            status: this.model.STATUS_PENDING
        });
    }

    static addForUser(id, data, model) {
        const changes = this.getChanges(data, model);

        return this.create({
            user_id: id,
            type: model.name,
            value: changes,
            status: this.model.STATUS_PENDING
        });
    }

    static getChanges(data, model) {
        const changes = {};

        for (const [key, value] of Object.entries(data)) {
            if (model.EDITABLE_FIELDS.includes(key)) {
                changes[key] = value;
            }
        }

        return changes;
    }
}

module.exports = ChangeRepository;

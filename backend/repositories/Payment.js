const BaseModelRepository = require('./BaseModelRepository');
const { Payment } = require('../models');
const { sequelize } = require('../models');

class PaymentRepository extends BaseModelRepository {
    static get model() {
        return Payment;
    }

    static amount(column, options) {
        return this.model.sum(column, options);
    }
}

module.exports = PaymentRepository;

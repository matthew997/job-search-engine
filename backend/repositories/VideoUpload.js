const BaseModelRepository = require('./BaseModelRepository');
const { VideoUpload } = require('../models');

class VideoUploadRepository extends BaseModelRepository {
    static get model() {
        return VideoUpload;
    }
}

module.exports = VideoUploadRepository;

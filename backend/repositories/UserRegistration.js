const BaseModelRepository = require('./BaseModelRepository');
const { UserRegistration } = require('../models');

class UserRegistrationRepository extends BaseModelRepository {
    static get model() {
        return UserRegistration;
    }

    static findOneByUserId(id) {
        return this.findOne({
            where: {
                user_id: id
            }
        });
    }
}

module.exports = UserRegistrationRepository;

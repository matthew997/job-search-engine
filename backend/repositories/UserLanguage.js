const BaseModelRepository = require('./BaseModelRepository');
const { UserLanguage } = require('../models');

class UserLanguageRepository extends BaseModelRepository {
    static get model() {
        return UserLanguage;
    }

    static async setUserLanguages(user, languages = []) {
        await this.destroy({
            where: {
                user_id: user.id
            }
        });

        const promises = [];

        for (const { name, proficiency } of languages) {
            if (!name || !proficiency) {
                continue;
            }

            const promise = this.create({
                proficiency,
                user_id: user.id,
                language_name: name
            });

            promises.push(promise);
        }

        return Promise.all(promises);
    }
}

module.exports = UserLanguageRepository;

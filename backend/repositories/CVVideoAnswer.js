const BaseModelRepository = require('./BaseModelRepository');
const { CVVideoAnswer } = require('../models');

class CVVideoAnswerRepository extends BaseModelRepository {
    static get model() {
        return CVVideoAnswer;
    }

    static findAllWithQuestion() {
        const attributes = [
            'index',
            'title',
            'info',
            'bullets',
            'video_example'
        ];

        return this.findAll({
            include: [
                {
                    association: 'question',
                    attributes,
                    order: [['index', 'ASC']]
                }
            ]
        });
    }

    static findAllForUser(user_id) {
        const attributes = [
            'index',
            'title',
            'info',
            'bullets',
            'video_example'
        ];

        return this.findAll({
            where: { user_id },
            include: [
                {
                    association: 'question',
                    attributes,
                }
            ],
            order: [['question', 'index', 'ASC']]
        });
    }

    static findOneWithQuestion(id) {
        const attributes = [
            'index',
            'title',
            'info',
            'bullets',
            'video_example'
        ];

        return this.findOne({
            where: { id },
            include: [
                {
                    association: 'question',
                    attributes
                }
            ]
        });
    }
}

module.exports = CVVideoAnswerRepository;

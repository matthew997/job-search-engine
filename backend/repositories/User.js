const bcrypt = require('bcryptjs');
const moment = require('moment');
const sequelize = require('sequelize');

const { Op } = sequelize;
const { Change, User, Role, Sequelize } = require('../models');
const RoleRepository = require('./Role');

const BaseModelRepository = require('./BaseModelRepository');

class UserRepository extends BaseModelRepository {
    static get model() {
        return User;
    }

    static setFreeDays(id, days) {
        return this.updateById(id, {
            free_days: days
        });
    }

    static async extendSubscription(userId) {
        const user = await this.findById(userId);

        return user.extendSubscription();
    }

    static addSubscription(userId) {
        return this.updateById(userId, {
            subscription: moment().add('1', 'month').toDate()
        });
    }

    static async destroy(options = {}) {
        return this.update(
            {
                deleted_at: new Date()
            },
            {
                ...options
            }
        );
    }

    static async register({
        first_name,
        last_name,
        birthday,
        email,
        marital_status,
        gender,
        phone,
        hometown,
        password,
        password_confirmation,
        email_notifications = false,
        type
    }) {
        const createdUser = await this.create({
            first_name,
            last_name,
            birthday,
            phone,
            email,
            marital_status,
            gender,
            hometown,
            password,
            password_confirmation,
            email_notifications: email_notifications ? new Date() : null,
            type,
            created_at: new Date(),
            updated_at: new Date()
        });

        const userRole = await RoleRepository.findOne({
            where: {
                name: Role.USER
            }
        });

        await createdUser.setRoles([userRole]);

        return createdUser;
    }

    static async login(email, password, cms) {
        const where = {
            email
        };

        if (cms) {
            const adminRole = await RoleRepository.findOne({
                where: {
                    name: Role.ADMIN
                }
            });

            where['$roleUser.role_id$'] = {
                [Op.in]: [adminRole.id]
            };
        } else {
            const userRole = await RoleRepository.findOne({
                where: {
                    name: Role.USER
                }
            });

            where['$roleUser.role_id$'] = {
                [Op.in]: [userRole.id]
            };
        }

        const user = await this.findOne({
            where,
            include: [
                {
                    association: 'roleUser'
                }
            ],
            subQuery: false
        });

        if (!user || !user.password) {
            return null;
        }

        return bcrypt.compareSync(password, user.password) ? user : null;
    }

    static async getMe(id, cms = false, email = null) {
        const where = {
            [Op.or]: [
                { id },
                {
                    email
                }
            ]
        };

        if (cms) {
            const adminRole = await RoleRepository.findOne({
                where: {
                    name: Role.ADMIN
                }
            });

            where['$roleUser.role_id$'] = {
                [Op.in]: [adminRole.id]
            };
        } else {
            const userRole = await RoleRepository.findOne({
                where: {
                    name: Role.USER
                }
            });

            where['$roleUser.role_id$'] = {
                [Op.in]: [userRole.id]
            };
        }

        const user = await this.findOne({
            where,
            include: [
                {
                    association: 'roleUser'
                }
            ],
            attributes: User.DISPLAYABLE_FIELDS,
            subQuery: false
        });

        if (!user) {
            return null;
        }

        return user;
    }

    static findOneByPhone(phone) {
        return this.model.findOne({
            where: {
                phone
            }
        });
    }

    static findOneByEmail(email, options = {}) {
        return this.model.findOne({
            where: {
                email
            },
            ...options
        });
    }

    static getProfileInformation(userId, type = 'user') {
        return type === 'user'
            ? this.getUserProfileInformation(userId)
            : this.getRecruiterProfileInformation(userId);
    }

    static async getRecruiterProfileInformation(id) {
        const personalInformation = await this.find(id, {
            include: [
                {
                    required: false,
                    attributes: ['value'],
                    association: 'changes',
                    where: {
                        status: Change.STATUS_PENDING
                    }
                }
            ]
        });

        return { personalInformation };
    }

    static async getUserProfileInformation(id) {
        const user = await this.find(id, {
            include: [
                {
                    association: 'details'
                },
                {
                    association: 'passports'
                },
                {
                    association: 'reviews'
                },
                {
                    association: 'languages',
                    attributes: [
                        ['value', 'name'],
                        [
                            Sequelize.literal(
                                '`languages->UserLanguage`.`proficiency`'
                            ),
                            'proficiency'
                        ]
                    ],
                    through: {
                        attributes: []
                    }
                }
            ]
        });

        let {
            details: moreDetails,
            passports: passportDetails,
            reviews
        } = user;

        const personalInformation = user.toJSON();

        moreDetails = {
            ...moreDetails.toJSON(),
            languages: user.languages
        };

        delete personalInformation.languages;
        delete personalInformation.passports;
        delete personalInformation.details;
        delete personalInformation.reviews;

        return {
            reviews,
            moreDetails,
            passportDetails,
            personalInformation
        };
    }
}

module.exports = UserRepository;

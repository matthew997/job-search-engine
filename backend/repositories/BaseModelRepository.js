const { Op } = require('sequelize');
const { sequelize } = require('../models');

class BaseModelRepository {
    static find(where, options = {}) {
        return typeof where === 'object'
            ? this.findOne(where, options)
            : this.findById(where, options);
    }

    static findById(id, options = {}) {
        return this.model.findOne({
            where: {
                id
            },
            ...options
        });
    }

    static findOne(options = {}) {
        return this.model.findOne({
            ...options
        });
    }

    static findAll(options = {}) {
        return this.model.findAll({
            ...options
        });
    }

    static whereIds(ids = [], options = {}) {
        return this.model.findAll({
            where: {
                id: ids
            },
            ...options
        });
    }

    static findAndCountAll(options = {}) {
        return this.model.findAndCountAll({
            ...options
        });
    }

    static count(options = {}) {
        return this.model.count({
            ...options
        });
    }

    static update(data, options) {
        return this.model.update(
            {
                ...data
            },
            {
                ...options
            }
        );
    }

    static updateById(id, data) {
        return this.model.update(
            { ...data },
            {
                where: {
                    id
                }
            }
        );
    }

    static destroy(options = {}) {
        return this.model.destroy({ ...options });
    }

    static restore(options = {}) {
        return this.model.restore({ ...options });
    }

    static create(data, options) {
        return this.model.create({ ...data }, { ...options });
    }

    static async isSlugAvailable(slug, id = null) {
        const where = {
            slug
        };

        if (id) {
            where.id = {
                [Op.not]: id
            };
        }

        const slugExists = await this.model.findOne({
            where
        });

        return !slugExists;
    }

    static findByIdOrSlug(idOrSlug, options = {}) {
        const uuidRegex = /[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/;
        let where = {
            deleted_at: null
        };

        if (idOrSlug.length === 36 && uuidRegex.test(idOrSlug)) {
            where.id = idOrSlug;
        } else {
            where.slug = idOrSlug;
        }

        if (options.where) {
            where = {
                ...where,
                ...options.where
            };

            delete options.where;
        }

        return this.findOne({
            where,
            ...options
        });
    }

    static bulkCreate(items = []) {
        return this.model.bulkCreate(items);
    }

    static async generateNumericId(transaction) {
        const count = await this.count(
            {},
            {
                transaction,
                lock: true
            }
        );

        return count + 1;
    }

    static random(limit = 1, options = {}) {
        const method = limit === 1 ? 'findOne' : 'findAll';

        return this[method]({
            order: sequelize.random(),
            limit,
            ...options
        });
    }

    static async updateOrCreate(data = {}, where = {}, fields = []) {
        let item;

        const options = {};

        if (fields.length) {
            options.fields = fields;
        }

        if (JSON.stringify(where) !== '{}') {
            item = await this.find({ where });
        }

        if (!item) {
            return this.create(data, options);
        }

        return item.update(data, options);
    }

    static bulkUpdateOrCreate(items) {
        const promises = [];

        for (const item of items) {
            const where = {};

            if (item.id) {
                where.id = item.id;
            }

            promises.push(this.updateOrCreate(item, where));
        }

        return Promise.all(promises);
    }

    static removeIncludeAttributes(include) {
        const temp = JSON.parse(JSON.stringify(include));

        const removeAttributes = item => {
            if (item.attributes) {
                item.attributes = [];
            }

            if (item.include) {
                item.include = this.removeIncludeAttributes(item.include);
            }

            return item;
        };

        return temp.map(item => removeAttributes(item));
    }
}

module.exports = BaseModelRepository;

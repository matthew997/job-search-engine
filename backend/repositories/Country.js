const BaseModelRepository = require('./BaseModelRepository');
const { Country } = require('../models');

class CountryRepository extends BaseModelRepository {
    static get model() {
        return Country;
    }

    static async findByName(name) {
        return this.findOne({
            where: {
                name
            }
        });
    }
}

module.exports = CountryRepository;

const BaseModelRepository = require('./BaseModelRepository');
const { Content } = require('../models');

class ContentRepository extends BaseModelRepository {
    static get model() {
        return Content;
    }

    static findByKey(key) {
        return this.model.findOne({
            where: {
                key
            }
        });
    }
}

module.exports = ContentRepository;

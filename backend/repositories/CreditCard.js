const BaseModelRepository = require('./BaseModelRepository');
const { CreditCard } = require('../models');

class CreditCardRepository extends BaseModelRepository {
    static get model() {
        return CreditCard;
    }

    static async checkIfTokenExists(token) {
        const item = await this.find({
            where: {
                token
            }
        });

        return !!item;
    }

    static async setDefaultCard(card) {
        await this.update(
            {
                default: false
            },
            {
                where: {
                    user_id: card.user_id,
                    deleted_at: null
                }
            }
        );

        await card.update({
            default: true
        });
    }

    static getDefaultCard(userId) {
        return this.findOne({
            where: {
                user_id: userId,
                default: true,
                deleted_at: null
            }
        });
    }

    static destroy(options = {}) {
        return this.update(
            {
                deleted_at: new Date()
            },
            {
                ...options
            }
        );
    }
}

module.exports = CreditCardRepository;

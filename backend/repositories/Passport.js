const BaseModelRepository = require('./BaseModelRepository');
const { Passport } = require('../models');

class PassportRepository extends BaseModelRepository {
    static get model() {
        return Passport;
    }

    static async destroy(options = {}) {
        return this.update(
            {
                deleted_at: new Date()
            },
            {
                ...options
            }
        );
    }
}

module.exports = PassportRepository;

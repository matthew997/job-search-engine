const BaseModelRepository = require('./BaseModelRepository');
const { Application, Country, Sequelize, sequelize } = require('../models');
const { Op } = Sequelize;

class ApplicationRepository extends BaseModelRepository {
    static get model() {
        return Application;
    }

    static getUserApplications(userId) {
        return this.findAll({
            where: {
                user_id: userId
            },
            include: [
                {
                    association: 'company',
                    include: [
                        {
                            attributes: ['id', 'name'],
                            model: Country.unscoped(),
                            as: 'country'
                        },
                        {
                            attributes: ['id', 'name'],
                            association: 'city'
                        }
                    ]
                }
            ]
        });
    }

    static getDetailsForId(id) {
        return this.findById(id, {
            include: [
                {
                    association: 'user',
                    attributes: {
                        exclude: [
                            'pin',
                            'password',
                            'application_credits',
                            'cancellation_credits'
                        ]
                    },
                    include: [
                        {
                            association: 'image'
                        },
                        {
                            association: 'CVVideoAnswers'
                        },
                        {
                            association: 'passports'
                        },
                        {
                            association: 'details'
                        },
                        {
                            association: 'reviews',
                            include: [
                                { association: 'cities' },
                                { association: 'countries' }
                            ]
                        },
                        {
                            association: 'languages',
                            attributes: [
                                ['value', 'name'],
                                [
                                    Sequelize.literal(
                                        '`user->languages->UserLanguage`.`proficiency`'
                                    ),
                                    'proficiency'
                                ]
                            ],
                            through: {
                                attributes: []
                            }
                        }
                    ]
                }
            ]
        });
    }

    static async getAllPerCompany({ where, offset, limit, order }) {
        const applicationsCountSQL = sequelize.dialect.QueryGenerator.selectQuery(
            'user_applications',
            {
                attributes: [
                    [
                        sequelize.fn(
                            'COUNT',
                            sequelize.col('user_applications.id')
                        ),
                        'applications_count'
                    ]
                ],
                where: {
                    user_id: {
                        [Op.eq]: sequelize.col('Application.user_id')
                    }
                },
                group: ['user_id']
            }
        ).slice(0, -1);

        const applications = await this.findAndCountAll({
            distinct: true,
            attributes: {
                include: [
                    [
                        sequelize.literal('(' + applicationsCountSQL + ')'),
                        'applications_count'
                    ]
                ]
            },
            where,
            include: [
                {
                    association: 'user',
                    include: [
                        {
                            association: 'details'
                        },
                        {
                            association: 'passports'
                        }
                    ]
                }
            ],
            order,
            subQuery: false,
            offset,
            limit
        });

        return applications;
    }

    static destroy(options = {}) {
        return this.update(
            {
                deleted_at: new Date()
            },
            {
                ...options
            }
        );
    }
}

module.exports = ApplicationRepository;

const BaseModelRepository = require('./BaseModelRepository');
const { Dropdown } = require('../models');

class DropdownRepository extends BaseModelRepository {
    static get model() {
        return Dropdown;
    }

    static findOneByName(name, options = {}) {
        return this.findOne({
            where: {
                name
            },
            ...options
        });
    }
}

module.exports = DropdownRepository;

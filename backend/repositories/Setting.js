const BaseModelRepository = require('./BaseModelRepository');
const { Setting } = require('../models');
const { Op } = require('sequelize');

class SettingService extends BaseModelRepository {
    static get model() {
        return Setting;
    }

    static async getByKey(key) {
        const setting = await this.findOne({
            where: {
                key
            },
            attributes: ['value']
        });

        if (!setting) {
            return null;
        }

        return setting.value;
    }

    static getPublic() {
        return this.findAll({
            where: {
                key: {
                    [Op.in]: Setting.PUBLIC_KEYS
                }
            },
            attributes: ['key', 'value']
        });
    }

    static getPublicByKey() {
        return this.findOne({
            where: {
                [Op.and]: [
                    {
                        key
                    },
                    {
                        key: {
                            [Op.in]: Setting.PUBLIC_KEYS
                        }
                    }
                ]
            },
            attributes: ['key', 'value']
        });
    }

    static async getTranzilaCredentials() {
        const [terminal, password, transactionPassword] = await Promise.all([
            this.getByKey('tranzila_terminal'),
            this.getByKey('tranzila_password'),
            this.getByKey('tranzila_transaction_password')
        ]);

        return {
            terminal,
            password,
            transactionPassword
        };
    }

    static async getEmailFrom() {
        const [name, address] = await Promise.all([
            this.getByKey('email_from_name'),
            this.getByKey('email_from_address'),
        ]);

        return {
            name,
            address
        }
    }
}

module.exports = SettingService;

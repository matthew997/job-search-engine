const BaseModelRepository = require('./BaseModelRepository');
const { CompanyLeadsFilter } = require('../models');

class CompanyLeadsFilterRepository extends BaseModelRepository {
    static get model() {
        return CompanyLeadsFilter;
    }

    static findOneByCompanyId(id) {
        return this.findOne({
            where: {
                company_id: id
            }
        });
    }
}

module.exports = CompanyLeadsFilterRepository;

const BaseModelRepository = require('./BaseModelRepository');
const { Review } = require('../models');

class ReviewRepository extends BaseModelRepository {
    static get model() {
        return Review;
    }

    static destroy(options = {}) {
        return this.update(
            {
                deleted_at: new Date()
            },
            {
                ...options
            }
        );
    }
}

module.exports = ReviewRepository;

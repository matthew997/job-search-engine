const BaseModelRepository = require('./BaseModelRepository');
const { CVQuestion } = require('../models');

class CVQuestionRepository extends BaseModelRepository {
    static get model() {
        return CVQuestion;
    }

    static async getLastIndex() {
        let index = 1;
        const question = await this.findOne({
            order: [['index', 'desc']]
        });

        if (question) {
            index = question.index;
        }

        return index;
    }
}

module.exports = CVQuestionRepository;

const BaseModelRepository = require('./BaseModelRepository');
const { Page } = require('../models');

class PageRepository extends BaseModelRepository {
    static get model() {
        return Page;
    }
}

module.exports = PageRepository;

const BaseModelRepository = require('./BaseModelRepository');
const { Op } = require('sequelize');

const { Media, Content, Company, Page } = require('../models');

class MediaRepository extends BaseModelRepository {
    static get model() {
        return Media;
    }

    static async checkIfImageIsUsed(id) {
        const models = [
            {
                item: Content,
                imageIdFields: [],
                contentWithImages: ['value']
            },
            {
                item: Page,
                imageIdFields: [],
                contentWithImages: ['content']
            },
            {
                item: Company,
                imageIdFields: ['company_logo', 'promote_image'],
                contentWithImages: []
            }
        ];

        for (const model of models) {
            const where = {
                [Op.or]: []
            };

            for (const imageIdField of model.imageIdFields) {
                where[Op.or].push({
                    [imageIdField]: id
                });
            }

            for (const content of model.contentWithImages) {
                where[Op.or].push({
                    [content]: {
                        [Op.like]: `%${id}%`
                    }
                });
            }

            const item = await model.item.findOne({
                where
            });

            if (item) {
                return true;
            }
        }

        return false;
    }
}

module.exports = MediaRepository;

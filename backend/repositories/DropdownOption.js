const BaseModelRepository = require('./BaseModelRepository');
const { DropdownOption } = require('../models');

class DropdownOptionRepository extends BaseModelRepository {
    static get model() {
        return DropdownOption;
    }

    static async replaceOptions(name, options) {
        await this.destroy({
            where: {
                parent: name
            }
        });

        options = options.map((option, index) => ({
            ...option,
            parent: name,
            order: index + 1
        }));

        return this.bulkCreate(options);
    }

    static getRandomOptionFromParent(parent) {
        return this.random(1, {
            where: {
                parent
            }
        });
    }
}

module.exports = DropdownOptionRepository;

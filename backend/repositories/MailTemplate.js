const BaseModelRepository = require('./BaseModelRepository');
const { MailTemplate } = require('../models');

class MailTemplateRepository extends BaseModelRepository {
    static get model() {
        return MailTemplate;
    }
}

module.exports = MailTemplateRepository;

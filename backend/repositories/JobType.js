const BaseModelRepository = require('./BaseModelRepository');
const { JobType } = require('../models');

class JobTypeRepository extends BaseModelRepository {
    static get model() {
        return JobType;
    }

    static async findByName(name) {
        return this.findOne({
            where: {
                name
            }
        });
    }
}

module.exports = JobTypeRepository;

const BaseModelRepository = require('./BaseModelRepository');

const {
    Change,
    Company,
    Sequelize,
    Application,
    sequelize
} = require('../models');
const CountryRepository = require('./Country');
const CityRepository = require('./City');
const MediaRepository = require('./Media');
const UserRepository = require('../repositories/User');

const { Op } = Sequelize;

class CompanyRepository extends BaseModelRepository {
    static get model() {
        return Company;
    }

    static findOneByName(name) {
        return this.findOne({
            where: {
                name
            }
        });
    }

    static async getLocationFilters() {
        const [countryIds, cityIds] = await Promise.all([
            this.findAll({
                where: {
                    active: true
                },
                attributes: [
                    [Sequelize.literal(`DISTINCT country_id`), 'country_id']
                ]
            }),

            this.findAll({
                where: {
                    active: true
                },
                attributes: [[Sequelize.literal(`DISTINCT city_id`), 'city_id']]
            })
        ]);

        const [countries, cities] = await Promise.all([
            CountryRepository.findAll({
                where: {
                    id: {
                        [Op.in]: countryIds.map(i => i.country_id)
                    }
                }
            }),
            CityRepository.findAll({
                where: {
                    id: {
                        [Op.in]: cityIds.map(i => i.city_id)
                    }
                },
                include: [
                    {
                        association: 'country'
                    }
                ]
            })
        ]);

        return { countries, cities };
    }

    static async findAllForUser(user) {
        const userId = typeof user === 'object' ? user.id : user;
        const includePendingChanges = {
            required: false,
            attributes: ['value'],
            association: 'changes',
            where: {
                status: Change.STATUS_PENDING
            }
        };

        let companies = await this.findAll({
            where: {
                user_id: userId,
                active: true
            },
            include: [
                {
                    association: 'filter',
                    include: [{ ...includePendingChanges }]
                },
                {
                    association: 'promoteImage'
                },
                {
                    association: 'companyLogo'
                },
                { ...includePendingChanges }
            ]
        });

        return Promise.all(
            companies.map(async item => {
                let company = item.toJSON();

                if (company.changes.length) {
                    let promoteImage;
                    let companyLogo;
                    let [changes] = company.changes;

                    const values = changes.value;

                    if (
                        values.promote_image &&
                        company.promote_image !== values.promote_image
                    ) {
                        promoteImage = await MediaRepository.findById(
                            values.promote_image
                        );
                    } else {
                        promoteImage = company.promoteImage;
                    }

                    if (
                        values.company_logo &&
                        company.company_logo !== values.company_logo
                    ) {
                        companyLogo = await MediaRepository.findById(
                            values.company_logo
                        );
                    } else {
                        companyLogo = company.companyLogo;
                    }

                    changes.value.promoteImage = promoteImage;
                    changes.value.companyLogo = companyLogo;
                }

                return company;
            })
        );
    }

    static async findAllAndCountLeads({ where, sortBy, order, offset, limit }) {
        if (sortBy === 'applications_count') {
            sortBy = Sequelize.literal('`applications_count`');
        }

        if (sortBy === 'applications_active_count') {
            sortBy = Sequelize.literal('`applications_active_count`');
        }

        const applicationsCountSQL = sequelize.dialect.QueryGenerator.selectQuery(
            'user_applications',
            {
                attributes: [
                    [
                        sequelize.fn(
                            'COUNT',
                            sequelize.col('user_applications.id')
                        ),
                        'applications_count'
                    ]
                ],
                where: {
                    company_id: {
                        [Op.eq]: sequelize.col('Company.id')
                    }
                }
            }
        ).slice(0, -1);

        const applicationsActiveCountSQL = sequelize.dialect.QueryGenerator.selectQuery(
            'user_applications',
            {
                attributes: [
                    [
                        sequelize.fn(
                            'COUNT',
                            sequelize.col('user_applications.id')
                        ),
                        'applications_active_count'
                    ]
                ],
                where: {
                    company_id: {
                        [Op.eq]: sequelize.col('Company.id')
                    },
                    status: {
                        [Op.in]: [
                            Application.STATUS_PENDING,
                            Application.STATUS_IN_PROGRESS,
                            Application.STATUS_APPLIED,
                            Application.STATUS_FOLLOW_UP
                        ]
                    }
                }
            }
        ).slice(0, -1);

        const [companiesRows, companiesCount] = await Promise.all([
            this.findAll({
                attributes: {
                    include: [
                        [
                            sequelize.literal('(' + applicationsCountSQL + ')'),
                            'applications_count'
                        ],
                        [
                            sequelize.literal(
                                '(' + applicationsActiveCountSQL + ')'
                            ),
                            'applications_active_count'
                        ]
                    ]
                },
                where,
                include: [
                    {
                        association: 'user',
                        attributes: ['last_login']
                    },
                    {
                        association: 'applications',
                        attributes: []
                    },
                    {
                        association: 'jobType'
                    }
                ],
                offset,
                limit,
                distinct: true,
                order: [[sortBy, order]],
                subQuery: false,
                group: ['id']
            }),

            this.count({
                where,
                include: [
                    {
                        association: 'user',
                        attributes: []
                    },
                    {
                        association: 'applications',
                        attributes: []
                    },
                    {
                        association: 'jobType'
                    }
                ],
                distinct: true
            })
        ]);

        return { companiesRows, companiesCount };
    }

    static setAllAsInactive(userId) {
        return this.update(
            {
                active: false
            },
            {
                where: {
                    user_id: userId
                }
            }
        );
    }

    static setAllAsActive(userId) {
        return this.update(
            {
                active: true
            },
            {
                where: {
                    user_id: userId
                }
            }
        );
    }

    static setAsInactive(id) {
        return this.updateById(id, {
            active: false
        });
    }

    static setAsActive(id) {
        return this.updateById(id, {
            active: true
        });
    }

    static setFreeLeads(id, leads) {
        return this.updateById(id, {
            free_leads: leads
        });
    }

    static setOnHomepage(id) {
        return this.updateById(id, {
            is_on_homepage: true
        });
    }

    static removeFromHomepage(id) {
        return this.updateById(id, {
            is_on_homepage: false
        });
    }

    static async filterCompaniesToUser(options, loggedUser) {
        const user = await UserRepository.findById(loggedUser.id, {
            include: [
                {
                    association: 'details'
                },
                {
                    association: 'passports'
                }
            ]
        });

        const { birthday, gender } = user;
        const { availability, experience } = user.details;

        const hasAmericanVisa = user.passports.find(
            passport => !!passport.has_american_visa
        );

        const hasForeignPassport = user.passports.find(
            passport => !!passport.has_foreign_passport
        );

        const age = () => {
            const diffMs = Date.now() - new Date(birthday);
            const ageDate = new Date(diffMs);

            return Math.abs(ageDate.getUTCFullYear() - 1970);
        };

        const isGoldUser = () => {
            const ONE_TO_SIX_MONTHS_EXPERIENCE = '1-6m';

            return !!experience && experience !== ONE_TO_SIX_MONTHS_EXPERIENCE;
        };

        const companiesFilters = {
            has_american_visa: {
                [Op.or]: [Boolean(hasAmericanVisa), null]
            },
            has_foreign_passport: {
                [Op.or]: [Boolean(hasForeignPassport), null]
            },
            sex: {
                [Op.or]: [gender === 'male' ? 'm' : 'f', null]
            },
            availability: {
                [Op.like]: `%${availability}%`
            },
            minimum_age: {
                [Op.lte]: age()
            },
            maximum_age: {
                [Op.gte]: age()
            }
        };

        if (isGoldUser) {
            companiesFilters.gold_users_only = {
                [Op.or]: [isGoldUser(), 0]
            };
        } else {
            companiesFilters.gold_users_only = {
                [Op.or]: [0]
            };
        }

        const companies = await this.findAll({
            where: options,
            include: [
                {
                    association: 'promoteImage'
                },
                {
                    association: 'companyLogo'
                },
                {
                    association: 'country'
                },
                {
                    association: 'city'
                },
                {
                    association: 'filter',
                    where: companiesFilters
                }
            ]
        });

        return companies;
    }
}

module.exports = CompanyRepository;

const { body } = require('express-validator');

const login = [
    body('pin').notEmpty().withMessage('Pin field must have a value').isString()
];

module.exports = login;

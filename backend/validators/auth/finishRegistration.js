const { body } = require('express-validator');

const finishRegistration = [
    body('passport.name', 'Name field is required')
        .notEmpty()
        .isLength({ max: 255 })
        .withMessage('Name should have less than 255 characters'),

    body(
        'passport.issued_on',
        'Passport issued date field is required'
    ).notEmpty(),
    body(
        'passport.expiration_date',
        'Passport expiration date field is required'
    ).notEmpty(),

    body('foreign_passports.*.name', 'Foreign passport name field is required')
        .notEmpty()
        .isLength({ max: 255 })
        .withMessage('Name should have less than 255 characters'),

    body(
        'foreign_passports.*.country',
        'Foreign passport country field is required'
    )
        .notEmpty()
        .isLength({ max: 255 })
        .withMessage('Country field should have less than 255 characters'),

    body(
        'foreign_passports.*.issued_on',
        'Foreign passport issued date field is required'
    ).notEmpty(),
    body(
        'foreign_passports.*.expiration_date',
        'Foreign passport expiration date field is required'
    ).notEmpty(),
    body('more_details.education', 'Education field is required').notEmpty(),
    body(
        'more_details.availability',
        'Available to start field is required'
    ).notEmpty(),

    body(
        'more_details.languages.*.name',
        'Language name field is required'
    ).notEmpty(),
    body(
        'more_details.languages.*.proficiency',
        'Language proficiency field is required'
    ).notEmpty()
];

module.exports = finishRegistration;

const { body } = require('express-validator');

const phone = [
    body('phone')
        .notEmpty()
        .withMessage('Phone field must have a value')
        .customSanitizer(phone => phone.replace(/[^0-9]/g, ''))
        .withMessage('This is not a phone number')
];

module.exports = phone;

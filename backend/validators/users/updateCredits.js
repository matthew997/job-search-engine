const { body } = require('express-validator');

const updateCredits = [
    body('id', 'ID is required').notEmpty(),

    body('application_credits', 'Application credits field is required')
        .notEmpty()
        .withMessage('Applications credits field must have a value'),

    body('cancellation_credits', 'Cancellation credits field is required')
        .notEmpty()
        .withMessage('Cancellation credits field must have a value')
];

module.exports = updateCredits;

const { body } = require('express-validator');
const UserRepository = require('../../repositories/User');

const emailRegExp = /(?:[a-z0-9A-Z!#$%&‘*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&‘*+/=?^_`{|}~-]+)*|“(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*“)@(?:(?:[a-z0-9]|[A-Z]|(?:[a-z0-9-]*[a-z0-9])?\.)+|[A-Z]|[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

const register = [
    body('first_name')
        .notEmpty()
        .withMessage('First name field must have a value')
        .isString(),

    body('last_name')
        .notEmpty()
        .withMessage('Last name field must have a value')
        .isString(),

    body('email')
        .notEmpty()
        .withMessage('Email field must have a value')
        .matches(emailRegExp)
        .withMessage('כתובת המייל שגויה')
        .isEmail()
        .withMessage('כתובת המייל שגויה')
        .normalizeEmail()
        .bail()
        .custom(async (email, { req }) => {
            const registeredUserId = req.session
                ? req.session.registeredUserId
                : null;
            const user = await UserRepository.findOneByEmail(email);

            if (user && registeredUserId !== user.id) {
                return Promise.reject('!המייל כבר קיים במערכת');
            }
        }),

    body('phone').custom(async (phone, { req }) => {
        const registeredUserId = req.session
            ? req.session.registeredUserId
            : null;
        const user = await UserRepository.findOneByPhone(phone);

        if (user && registeredUserId !== user.id) {
            return Promise.reject('מספר הטלפון כבר קיים במערכת!');
        }
    })
];

module.exports = register;

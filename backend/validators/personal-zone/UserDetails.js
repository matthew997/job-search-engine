const { body } = require('express-validator');

const details = [
    body('passports.*.name', 'Name field is required')
        .notEmpty()
        .isLength({ max: 255 })
        .withMessage('Name should have less than 255 characters'),

    body(
        'passports.*.issued_on',
        'Passport issued date field is required'
    ).notEmpty(),
    body(
        'passports.*.expiration_date',
        'Passport expiration date field is required'
    ).notEmpty(),

    body('passports', 'Passport country field is required')
        .custom(passports => {
            for (const { country = null, isForeign = false } of passports) {
                if (isForeign && !country) {
                    return false;
                }
            }

            return true;
        })
        .withMessage('Country field should have less than 255 characters'),

    body('reviews.*.start_date', 'Start date field is required').notEmpty(),
    body('reviews.*.end_date', 'End date field is required').notEmpty(),
    body('reviews.*.rating', 'Rating field is required').notEmpty(),

    body('reviews.*.company', 'Company field is required')
        .notEmpty()
        .isLength({ max: 255 })
        .withMessage('Company should have less than 255 characters'),

    body('reviews.*.phone', 'Company field is required')
        .notEmpty()
        .isLength({ max: 255 })
        .withMessage('Company should have less than 255 characters'),

    body('reviews.*.content', 'Content field is required').notEmpty(),

    body(
        'reviews.*.is_recommended',
        'Is recommended field is required'
    ).notEmpty(),

    body('details.education', 'Education field is required').notEmpty(),
    body(
        'details.availability',
        'Available to start field is required'
    ).notEmpty(),

    body(
        'details.languages.*.name',
        'Language name field is required'
    ).notEmpty(),
    body(
        'details.languages.*.proficiency',
        'Language proficiency field is required'
    ).notEmpty(),

    body('personal.first_name', 'First name field is required')
        .notEmpty()
        .isLength({ max: 255 })
        .withMessage('First name field should have less than 255 characters'),

    body('personal.last_name', 'Last name field is required')
        .notEmpty()
        .isLength({ max: 255 })
        .withMessage('Last name field should have less than 255 characters'),

    body('personal.gender', 'Gender field is required').notEmpty(),
    body('personal.birthday', 'Birthday field is required').notEmpty(),
    body('personal.hometown', 'Hometown name field is required')
        .notEmpty()
        .isLength({ max: 255 })
        .withMessage('Hometown field should have less than 255 characters'),

    body('personal.phone', 'Phone field is required')
        .notEmpty()
        .withMessage('Phone field must have a value')
        .customSanitizer(phone => phone.replace(/[^0-9]/g, ''))
        .withMessage('This is not a phone number')
];

module.exports = details;

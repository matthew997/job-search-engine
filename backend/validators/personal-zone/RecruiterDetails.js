const { body } = require('express-validator');

const details = [
    body('first_name', 'First name field is required')
        .notEmpty()
        .isLength({ max: 255 })
        .withMessage('First name field should have less than 255 characters'),

    body('last_name', 'Last name field is required')
        .notEmpty()
        .isLength({ max: 255 })
        .withMessage('Last name field should have less than 255 characters'),

    body('phone', 'Phone field is required')
        .notEmpty()
        .withMessage('Phone field must have a value')
        .customSanitizer(phone => phone.replace(/[^0-9]/g, ''))
        .withMessage('This is not a phone number')
];

module.exports = details;

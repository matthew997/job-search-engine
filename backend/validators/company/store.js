const { body } = require('express-validator');
const CompanyRepository = require('../../repositories/Company');

const store = [
    body('name')
        .notEmpty()
        .withMessage('Name field must have a value')
        .isString()
        .custom(async name => {
            const company = await CompanyRepository.findOneByName(name);

            if (company) {
                return Promise.reject('Company name already exists');
            }
        }),

    body('city')
        .notEmpty()
        .withMessage('City field must have a value')
        .isString(),

    body('country')
        .notEmpty()
        .withMessage('Country field must have a value')
        .isString(),

    body('company_logo')
        .notEmpty()
        .withMessage('Company logo field must have a value')
        .isString(),

    body('promote_video')
        .notEmpty()
        .withMessage('Promote Video field must have a value')
        .isString(),

    body('facebook_link')
        .notEmpty()
        .withMessage('Facebook Link field must have a value')
        .isString(),

    body('promote_image')
        .notEmpty()
        .withMessage('Promote Image field must have a value')
        .isString(),

    body('type_id')
        .notEmpty()
        .withMessage('Expertise field must have a value')
        .isString(),

    body('about')
        .notEmpty()
        .withMessage('About field must have a value')
        .isString(),

    body('working_environment')
        .notEmpty()
        .withMessage('Working Environment field must have a value')
        .isString(),

    body('office_nature')
        .notEmpty()
        .withMessage('Office Nature field must have a value')
        .isString(),

    body('created_at')
        .notEmpty()
        .withMessage('Created At field must have a value')
        .isString(),

    body('updated_at')
        .notEmpty()
        .withMessage('Updated At field must have a value')
        .isString()
];

module.exports = { store };

const { body } = require('express-validator');
const store = [
    body('question_id')
        .notEmpty()
        .withMessage('Question_ID must be UUID format'),

    body('index', 'Index field is required')
        .notEmpty()
        .isInt()
        .withMessage('Answer index field must be an integer')

    // body('video', 'Video is required!')
    //     .notEmpty()
    //     .withMessage('Video is required!')
    //     .isString()
];

module.exports = store;

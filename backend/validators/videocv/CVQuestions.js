const { body } = require('express-validator');

const store = [
    body('title', 'Title field is required')
        .notEmpty()
        .withMessage('Title field must have a value')
        .isString(),

    body('info', 'Information field is required')
        .notEmpty()
        .withMessage('Information field must have a value')
        .isString(),

    body('bullets', 'Bullets field is required')
        .notEmpty()
        .withMessage('Bullets field must have a value')
        .isArray()
];

module.exports = store;

const moment = require('moment');

const UserRepository = require('../repositories/User');

class PerLeadPayment {
    constructor(queue) {
        this.queue = queue;
    }

    async handle() {
        const users = await this.getUsers();

        for (const user of users) {
            await this.addToQueue(user);
        }
    }

    async getUsers() {
        const currentMonth = moment().startOf('month');
        const users = await UserRepository.findAll({
            where: {
                type: 'company'
            },
            include: [
                {
                    required: false,
                    association: 'creditCards',
                    where: {
                        default: true
                    }
                },
                {
                    attributes: ['id', 'free_leads'],
                    association: 'companies'
                },
                {
                    required: false,
                    association: 'payments',
                    where: {
                        type: 'leads'
                    }
                }
            ]
        });

        return users.filter(user => {
            const paid = user.payments.find(payment =>
                moment(payment.created_at).isAfter(currentMonth)
            );

            return !paid;
        });
    }

    addToQueue(user) {
        return this.queue.add(user);
    }
}

module.exports = PerLeadPayment;

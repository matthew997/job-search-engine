const moment = require('moment');
const { Op } = require('sequelize');

const UserRepository = require('../repositories/User');

class RenewSubscription {
    constructor(queue) {
        this.queue = queue;
    }

    async handle() {
        const users = await this.getUsers();

        for (const user of users) {
            await this.addToQueue(user);
        }
    }

    async getUsers() {
        return UserRepository.findAll({
            where: {
                type: 'company',
                subscription: {
                    [Op.and]: [
                        { [Op.not]: null },
                        { [Op.lte]: moment().toDate() }
                    ]
                }
            },
            include: [
                {
                    required: false,
                    association: 'creditCards',
                    where: {
                        default: true
                    }
                }
            ]
        });
    }

    addToQueue(user) {
        return this.queue.add(user);
    }
}

module.exports = RenewSubscription;

const Layer = require('express/lib/router/layer');
const HTTP = require('http-status-codes');

function wrapErrorMiddleware(fn) {
    return (err, req, res, next) => {
        const ret = fn.call(this, err, req, res, next);

        if (ret && ret.catch) {
            ret.catch(innerErr => next(innerErr));
        }

        return ret;
    };
}

function wrap(fn) {
    return (req, res, next) => {
        const ret = fn.call(this, req, res, next);

        if (ret && ret.catch) {
            ret.catch(err => {
                next(err);
            });
        }

        return ret;
    };
}

Object.defineProperty(Layer.prototype, 'handle', {
    enumerable: true,
    get() {
        return this.__handle;
    },
    set(fn) {
        // Bizarre, but Express checks for 4 args to detect error middleware: https://github.com/expressjs/express/blob/master/lib/router/layer.js
        if (fn.length === 4) {
            fn = wrapErrorMiddleware(fn);
        } else {
            fn = wrap(fn);
        }

        this.__handle = fn;
    }
});

module.exports = app => {
    app.use((err, req, res, next) => {
        if (
            err.name === 'SequelizeValidationError' ||
            err.name === 'SequelizeUniqueConstraintError'
        ) {
            const errors = err.errors.map(e => {
                return { message: e.message, param: e.path };
            });

            return res.status(HTTP.UNPROCESSABLE_ENTITY).json({ errors });
        }

        if (err.message === 'Validation failed') {
            const errors = err.array().map(e => {
                return { message: e.msg, param: e.param };
            });

            return res.status(HTTP.UNPROCESSABLE_ENTITY).json({ errors });
        }

        console.error(err.stack);
        res.status(HTTP.INTERNAL_SERVER_ERROR).send(
            'We messed something up. Sorry!'
        );
    });
};

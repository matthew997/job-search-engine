module.exports = {
    root: true,
    parserOptions: {
        sourceType: 'script'
    },
    extends: [
        // https://github.com/vuejs/eslint-plugin-vue#bulb-rules
        'plugin:vue/recommended',
        // https://github.com/prettier/eslint-config-prettier
        'prettier'
    ],
    rules: {
        'padding-line-between-statements': [
            'error',
            { blankLine: 'always', prev: '*', next: 'return' },
            { blankLine: 'always', prev: '*', next: 'if' },
            { blankLine: 'always', prev: 'if', next: '*' }
        ],
        // Only allow debugger in development
        'no-debugger': process.env.PRE_COMMIT ? 'error' : 'off',
        // Only allow `console.log` in development
        'no-console': process.env.PRE_COMMIT
            ? ['error', { allow: ['warn', 'error'] }]
            : 'off',
        'vue/array-bracket-spacing': 'error',
        'vue/arrow-spacing': 'error',
        'vue/block-spacing': 'error',
        'vue/brace-style': 'error',
        'vue/camelcase': 'error',
        'vue/comma-dangle': ['error', 'never'],
        'vue/component-name-in-template-casing': ['error', 'kebab-case'],
        'vue/dot-location': ['error', 'property'],
        'vue/eqeqeq': 'error',
        'vue/key-spacing': 'error',
        'vue/keyword-spacing': 'error',
        'vue/no-boolean-default': ['error', 'default-false'],
        'vue/no-deprecated-scope-attribute': 'error',
        'vue/object-curly-spacing': ['error', 'always'],
        'vue/space-infix-ops': 'error',
        'vue/space-unary-ops': 'error',
        'vue/v-on-function-call': 'error',
        'vue/html-indent': ['error', 4, { baseIndent: 1 }],
        indent: 'off',

        'vue/attributes-order': [
            'error',
            {
                order: [
                    'DEFINITION',
                    'TWO_WAY_BINDING',
                    'LIST_RENDERING',
                    'CONDITIONALS',
                    'RENDER_MODIFIERS',
                    'GLOBAL',
                    'UNIQUE',
                    'OTHER_ATTR',
                    'OTHER_DIRECTIVES',
                    'EVENTS',
                    'CONTENT'
                ],
                alphabetical: true
            }
        ],
        'vue/order-in-components': [
            'error',
            {
                order: [
                    'el',
                    'name',
                    'parent',
                    'functional',
                    ['delimiters', 'comments'],
                    ['components', 'directives', 'filters'],
                    'extends',
                    'mixins',
                    'inheritAttrs',
                    'model',
                    ['props', 'propsData'],
                    'head',
                    'fetch',
                    'asyncData',
                    'data',
                    'computed',
                    'watch',
                    'LIFECYCLE_HOOKS',
                    'methods',
                    ['template', 'render'],
                    'renderError'
                ]
            }
        ],
        'vue/component-tags-order': [
            'error',
            {
                order: ['template', 'script', 'style']
            }
        ]
    },
    overrides: [
        {
            files: ['src/**/*', 'tests/unit/**/*', 'tests/e2e/**/*'],
            parserOptions: {
                parser: 'babel-eslint',
                sourceType: 'module'
            },
            env: {
                browser: true
            }
        },
        {
            files: ['**/*.unit.js'],
            parserOptions: {
                parser: 'babel-eslint',
                sourceType: 'module'
            },
            env: { jest: true },
            globals: {
                mount: false,
                shallowMount: false,
                shallowMountView: false,
                createComponentMocks: false,
                createModuleStore: false
            }
        }
    ]
};

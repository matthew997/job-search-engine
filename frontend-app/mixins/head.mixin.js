export default {
    head() {
        const title =
            this.meta && this.meta.title
                ? this.meta.title + ' | Chutzlaaretz'
                : 'Chutzlaaretz';

        return {
            title,
            meta: [
                {
                    hid: 'description',
                    name: 'description',
                    content: this.meta && this.meta.description
                }
            ]
        };
    }
};

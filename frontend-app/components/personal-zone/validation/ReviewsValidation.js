import { required, maxLength } from 'vuelidate/lib/validators';
import {
    isValidPhoneNumber,
    isValidPhoneNumberFiledNotRequired
} from '@/helpers/helpers';

export default {
    $each: {
        company: {
            required,
            maxLength: maxLength(255)
        },
        country: {
            required,
            maxLength: maxLength(255)
        },
        city: {
            required,
            maxLength: maxLength(255)
        },
        is_recommended: {
            required
        },
        content: {
            required
        },
        rating: {
            required
        },
        start_date: {
            required
        },
        end_date: {
            required
        },
        phone: {
            required,
            maxLength: maxLength(255),
            isValidPhoneNumber
        },
        phone_2: {
            maxLength: maxLength(255),
            isValidPhoneNumberFiledNotRequired
        }
    }
};

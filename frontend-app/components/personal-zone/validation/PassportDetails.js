import { required, requiredIf, maxLength } from 'vuelidate/lib/validators';

export default {
    $each: {
        name: {
            required,
            maxLength: maxLength(255)
        },
        country: {
            required: requiredIf(({ isForeign = false }) => isForeign),
            maxLength: maxLength(255)
        },
        issued_on: { required },
        expiration_date: { required }
    }
};

import { required, maxLength, numeric, email } from 'vuelidate/lib/validators';

export default {
    phone: {
        required
    },
    first_name: {
        required,
        maxLength: maxLength(255)
    },
    last_name: {
        required,
        maxLength: maxLength(255)
    },
    email: {
        required,
        email
    },
    birthday: {
        required
    },
    gender: {
        required
    },
    hometown: {
        required,
        maxLength: maxLength(255)
    }
};

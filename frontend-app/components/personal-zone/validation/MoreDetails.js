import { required, requiredIf } from 'vuelidate/lib/validators';

export default {
    experience: {
        required: requiredIf(({ hasExperience }) => hasExperience)
    },
    experience_job_type: {
        required: requiredIf(({ hasExperience }) => hasExperience)
    },
    education: { required },

    languages: {
        $each: {
            name: { required },
            proficiency: { required }
        }
    },
    hasExperience: { required },
    availability: { required }
};

export default {
    experience: [
        {
            text: '1-6 חודשים‌ ‌',
            value: '1-6m'
        },
        {
            text: '7-12 חודשים‌',
            value: '7-12m'
        },
        {
            text: '1-3 שנים‌',
            value: '1-3y'
        },
        {
            text: '‌יותר מ-3 שנים‌',
            value: '3y'
        }
    ],

    availability: [
        {
            text: 'מיידי',
            value: 'immediate'
        },
        {
            text: '1-3 חודשים מהיום',
            value: '1-3m'
        },
        {
            text: '3-6 חודשים מהיום',
            value: '3-6m'
        }
    ]
};

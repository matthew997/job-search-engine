export const blobToBase64 = async function(blob) {
    const buffer = Buffer.from(await blob.arrayBuffer());

    return buffer.toString('base64');
};

export const base64toBlob = function(blobString) {
    const byteCharacters = atob(blobString);
    const byteNumbers = new Array(byteCharacters.length);

    for (let i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);

    const blob = new Blob([byteArray], {
        type: 'video/webm'
    });

    return blob;
};

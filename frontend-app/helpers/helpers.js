export const isValidPhoneNumber = function(value) {
    if (!value) {
        return false;
    }

    const valueWithoutSings = value.replace(/_/g, '').replace(/-/g, '');

    return (
        /^\+?05\d([-]{0,1})\d{4}([-]{0,1})\d{3}$/.test(valueWithoutSings) ||
        /^\+?(0[2-4,8-9](-|)\d{7})|^07[1-4,6-9](-|)\d{7}$/.test(
            valueWithoutSings
        ) ||
        /^(?:(?:(\+?972|\(\+?972\)|\+?\(972\))(?:\s|\.|-)?([1-9]\d?))|(0[23489]{1})|(0[57]{1}[0-9]))(?:\s|\.|-)?([^0\D]{1}\d{2}(?:\s|\.|-)?\d{4})$/.test(
            valueWithoutSings
        )
    );
};

export const isValidEmail = function(value) {
    const regExpText = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

    return regExpText.test(value);
};

export const phoneMask = [
    '+',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/
];

export const checkIsNotHebrew = function(value) {
    if (!value) {
        return false;
    }

    const position = value.search(/[\u0590-\u05FF]/);

    if (position >= 0) {
        return false;
    }

    return true;
};

export const isValidPhoneNumberFiledNotRequired = function(value) {
    if (!value) {
        return true;
    }

    const valueWithoutSings = value.replace(/_/g, '').replace(/-/g, '');

    return (
        /^\+?05\d([-]{0,1})\d{4}([-]{0,1})\d{3}$/.test(valueWithoutSings) ||
        /^\+?(0[2-4,8-9](-|)\d{7})|^07[1-4,6-9](-|)\d{7}$/.test(
            valueWithoutSings
        ) ||
        /^(?:(?:(\+?972|\(\+?972\)|\+?\(972\))(?:\s|\.|-)?([1-9]\d?))|(0[23489]{1})|(0[57]{1}[0-9]))(?:\s|\.|-)?([^0\D]{1}\d{2}(?:\s|\.|-)?\d{4})$/.test(
            valueWithoutSings
        )
    );
};

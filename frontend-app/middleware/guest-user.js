export default async function ({ store, redirect }) {

    if (store.state.auth.loggedIn && !store.state.auth.user.registration_completed) {
        if (store.state.auth.user.type === 'user') {
            return redirect('/signup');
        }

        return redirect('/recruiter-sign-up');
    }

    if (store.state.auth.loggedIn) {
        redirect('/');
    }
}

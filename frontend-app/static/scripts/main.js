const $ = require('jquery');

window.jQuery = $;

require('jquery-sticky');
require('jquery-bar-rating');
require('selectric/public/jquery.selectric.min.js');
require('slick-carousel/slick/slick');
require('magnific-popup');
require('@fancyapps/fancybox');
require('./datepicker');
require('./datepicker/translations/hebrew');
require('country-city');

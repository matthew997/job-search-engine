class AnalyticsService {
    constructor(clientInstance) {
        this.client = clientInstance;
    }

    sendGoogleTagManagerEvent(dataLayer) {
        if (dataLayer.constructor !== Object) {
            throw new Error('dataLayer should be an object');
        }

        if (this.client.$gtm) {
            this.client.$gtm.push(dataLayer);
        }
    }
}

export default AnalyticsService;

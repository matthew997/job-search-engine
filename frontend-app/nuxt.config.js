const env = require('dotenv').config();
import enLocale from './assets/locales/en';
import heLocale from './assets/locales/he';
const axios = require('axios');

export default {
    ssr: true,
    /*
     ** Headers of the page
     */
    head: {
        title: 'Chutzlaaretz',
        htmlAttrs: {
            dir: 'rtl'
        },
        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content: ''
            }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
            {
                rel: 'stylesheet',
                href:
                    'https://fonts.googleapis.com/css?family=Rubik:300,400,500,700&display=swap&subset=hebrew'
            }
        ]
    },
    env: {
        appUrl: env.parsed.APP_URL || 'http://localhost:3000',
        apiBaseUrl: env.parsed.API_BASE_URL || 'http://localhost:3001',
        mediaBaseUrl: env.parsed.MEDIA_BASE_URL || 'http://localhost:3001'
    },
    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#fff' },
    /*
     ** Global CSS
     */
    css: [
        '@fancyapps/fancybox/dist/jquery.fancybox.css',
        'slick-carousel/slick/slick.css',
        'selectric/public/selectric.css',
        'jquery-ui-dist/jquery-ui.min.css',
        '@/assets/design/main.scss',
        '@/assets/styles/main.scss',
        'magnific-popup/dist/magnific-popup.css'
    ],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        { src: '~/plugins/axios' },
        { src: '~/plugins/directives/full-image-path', mode: 'client' },
        {
            src: '~/plugins/directives/full-background-image-path',
            mode: 'client'
        },
        { src: '~/static/scripts/main.js', mode: 'client' },
        { src: '~/plugins/services/inject-analytics-service', mode: 'client' },
        { src: '~/plugins/vuelidate.js' }
    ],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        '@nuxtjs/gtm',
        '@nuxtjs/sentry',
        '@nuxtjs/auth',
        '@nuxtjs/toast',
        [
            'nuxt-i18n',
            {
                locales: [process.env.APP_LOCALE || 'he'],
                strategy: 'no_prefix',
                defaultLocale: 'he',

                vueI18n: {
                    fallbackLocale: 'he',
                    messages: {
                        he: heLocale,
                        en: enLocale
                    }
                }
            }
        ]
    ],

    toast: {
        position: 'top-right',
        duration: 2000
    },

    auth: {
        localStorage: {
            prefix: 'auth.'
        },
        cookie: {
            prefix: 'auth.',
            options: {
                path: '/'
            }
        },
        redirect: {
            login: '/login',
            home: '/',
            callback: false
        },
        strategies: {
            local: {
                endpoints: {
                    login: {
                        url: '/auth/login',
                        method: 'post',
                        propertyName: 'id'
                    },
                    logout: { url: '/auth/logout', method: 'post' },
                    user: {
                        url: '/me',
                        method: 'get',
                        propertyName: false,
                        tokenRequired: false,
                        tokenType: false
                    }
                },
                tokenRequired: true,
                tokenType: false
            }
        },
        watchLoggedIn: false
    },
    /*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
    axios: {
        baseURL: env.parsed.API_BASE_URL,
        credentials: true
    },
    sentry: {
        dsn: env.parsed.SENTRY_DSN,
        disabled: env.parsed.SENTRY_DISABLED === 'true'
    },

    gtm: {
        id: async () => {
            const { data } = await axios.get(
                `${env.parsed.API_BASE_URL}/settings/public`
            );

            const gtmKey = data.find(item => item.key === 'google_tag_manager');

            if (gtmKey && gtmKey.value) {
                return gtmKey.value;
            }

            return undefined;
        },
        enabled: true,
        pageTracking: true
    },

    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {},
        extractCSS: true,
        optimization: {
            splitChunks: {
                chunks: 'all',
                automaticNameDelimiter: '.',
                name: undefined,
                cacheGroups: {}
            }
        }
    },

    /*
     **vue configuration
     */
    vue: {
        config: {
            ignoredElements: ['stream']
        }
    }
};

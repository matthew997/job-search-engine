'use strict';

export const defaultState = () => {
    return {};
};

export const state = () => defaultState();

export const getters = {
    getByKey: state => key => state.data[key],
    getData: state => state.data
};

export const actions = {
    async getApplications() {
        const { data } = await this.$axios.get('/applications');

        return data;
    },

    async getApplicationsForRecruiter({}, { companyId, page, id, status }) {
        const { data } = await this.$axios.get('/recruiter/applications', {
            params: {
                companyId,
                page,
                id,
                status
            }
        });

        return data;
    },

    async show({}, id) {
        const { data } = await this.$axios.get(`/applications/${id}`);

        return data;
    },

    async updateNote({}, { id, note }) {
        const { data } = await this.$axios.post(
            `/applications/${id}/update-note`,
            {
                note
            }
        );

        return data;
    },
    async updateStatus({}, { id, status }) {
        const { data } = await this.$axios.post(
            `/applications/${id}/update-status`,
            {
                status
            }
        );

        return data;
    },

    async postApplication({}, company_id) {
        const { data } = await this.$axios.post('/applications', {
            company_id
        });

        return data;
    },

    async cancelApplication({}, company_id) {
        const { data } = await this.$axios.post('/applications/cancel', {
            company_id
        });

        return data;
    }
};

export default {
    state,
    getters,
    actions
};

'use strict';

export const defaultState = () => {
    return {
        cv_answers: []
    };
};

export const state = () => defaultState();

export const getters = {
    getCVAnswers: state => state.cv_answers
};

export const mutations = {
    SET_ANSWER(state, answer) {
        state.cv_answers.push(answer);
    }
};

export const actions = {
    async getCvAnswersPerUser() {
        const data = await this.$axios.get('/cv-video');

        return data;
    },

    async setAnswer({ commit }, newAnswer) {
        if (!getters.getAnswers) {
            const { data } = await this.$axios.post(
                `/cv-video-answers`,
                newAnswer
            );
            commit('SET_ANSWER', data);

            return data;
        }
    },

    async setAnswerFromSafariMobile({ commit }, formData) {
        if (!getters.getAnswers) {
            const { data } = await this.$axios.post(
                `/cv-video-answers`,
                formData
            );
            commit('SET_ANSWER', data);

            return data;
        }
    },

    async updateCvVideo({}, { id, newVideo }) {
        const { data } = await this.$axios.put(
            `/cv-video-answers/${id}`,
            newVideo
        );

        return data;
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};

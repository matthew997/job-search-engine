'use strict';

export const defaultState = () => {
    return {
        settings: null
    };
};

export const state = () => defaultState();

export const getters = {
    getSettings: state => state.settings,
    getSettingByKey: state => key => {
        const setting = state.settings.find(x => x.key === key);

        return setting ? setting.value : null;
    }
};

export const mutations = {
    SET_SETTINGS(state, settings) {
        state.settings = settings;
    }
};

export const actions = {
    async getSettings({ getters, commit }) {
        if (!getters.getSettings) {
            const { data } = await this.$axios.get(`/settings/public`);
            commit('SET_SETTINGS', data);
        }
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};

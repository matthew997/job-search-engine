'use strict';

export const defaultState = () => {
    return {
        user: null,
        userDetails: {},
        isGoldUser: false
    };
};

export const state = () => defaultState();

export const getters = {
    getUser: state => state.user,
    getUserDetails: state => state.userDetails,
    isGoldUser: state => state.isGoldUser
};

export const mutations = {
    SET_USER(state, user) {
        state.user = user;
    },

    UPDATE_USER(state, user) {
        state.user = user;
    },

    SET_USER_DETAILS(state, details) {
        state.userDetails = details;
    },

    SET_IS_GOLD_USER(state) {
        if (state.userDetails.moreDetails) {
            const ONE_TO_SIX_MONTHS_EXPERIENCE = '1-6m';

            const { experience } = state.userDetails.moreDetails;

            state.isGoldUser =
                !!experience && experience !== ONE_TO_SIX_MONTHS_EXPERIENCE;
        }
    },

    SET_GOLD_USER(state, boolValue) {
        state.isGoldUser = boolValue;
    }
};

export const actions = {
    async getData({ commit }, role = null) {
        const { data } = await this.$axios.get(`/me`, { params: { role } });

        commit('SET_USER', data);

        return data;
    },

    async getIsGoldUser({ commit }) {
        const { data } = await this.$axios.get(`/details`);

        commit('SET_USER_DETAILS', data);
        commit('SET_IS_GOLD_USER');

        return data;
    },

    async getProfileInformation() {
        const { data } = await this.$axios.get(`/details`);

        return data;
    },

    async getCompanies() {
        const { data } = await this.$axios.get(`/user/companies`);

        return data;
    },

    async updateUserDetails({}, form) {
        const { data } = await this.$axios.post(`/details/user`, form);

        return data;
    },

    async updateRecruiterDetails({}, form) {
        const { data } = await this.$axios.post(`/details/recruiter`, form);

        return data;
    },

    async updateImage({}, form) {
        const { data } = await this.$axios.put(`/users/image`, form);

        return data;
    },

    async update({ commit }, formData) {
        const { data } = await this.$axios.patch(
            `/users/${formData.id}`,
            formData
        );

        commit('UPDATE_USER', data);

        return data;
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};

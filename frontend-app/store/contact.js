'use strict';

export const defaultState = () => {
    return {};
};

export const state = () => defaultState();

export const getters = {};

export const mutations = {};

export const actions = {
    async postNewsletter({ }, email) {
        const { data } = await this.$axios.post(
            `/mail/newsletter`,
            { email }
        );

        return data;
    },
    async postContactForm({ }, params) {
        const { data } = await this.$axios.post(
            `/mail/contact`,
            params
        );

        return data;
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};

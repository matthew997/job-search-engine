'use strict';

export const defaultState = () => {
    return {
        step: 'RegisterPersonalInfoStep',
        pin: null,
        data: {},
        acceptedTermsByCompany: false,
        userTemporaryData: {}
    };
};

export const state = () => defaultState();

export const getters = {
    getByKey: state => key => {
        return state.data[key];
    },
    getData: state => state.data,
    getCurrentStep: state => state.step,
    getPassportData: state => state.passportData,
    getForeignPassportsData: state => state.passportData,
    getReviewsData: state => state.reviewsData,
    getMoreDetailsData: state => state.moreDetailsData,
    getUserTemporaryData: state => state.userTemporaryData
};

export const mutations = {
    UPDATE_DATA(state, formData) {
        state.data = { ...state.data, ...formData };
    },
    UPDATE_PIN(state, pin) {
        state.pin = pin;
    },
    SET_ID(state, id) {
        state.data.id = id;
    },
    RESET_DATA(state) {
        state.data = {};

        window.localStorage.removeItem('register');
    },
    SET_STEP(state, step) {
        state.step = step;
    },
    UPDATE_PASSPORT_DATA(state, formData) {
        state.passportData = formData;
        state.passportData.user_id = state.data.id;
    },
    UPDATE_FOREIGN_PASSPORTS_DATA(state, formData) {
        state.foreignPassportsData = formData;
    },
    UPDATE_MORE_DETAILS_DATA(state, formData) {
        state.moreDetailsData = formData;
    },
    SET_COMPANY_ACCEPTED_TERMS(state) {
        state.acceptedTermsByCompany = true;
    },
    SET_USER_TEMPORARY_DATA(state, data) {
        state.userTemporaryData = data;
    }
};

export const actions = {
    async setData({ state, commit }, formData) {
        commit('UPDATE_DATA', formData);
    },

    async setPin({ state, commit }, pin) {
        commit('UPDATE_PIN', pin);
    },

    async newUser({ state, commit }) {
        if (state.data.id) {
            const { data } = await this.$axios.patch(
                `/auth/register`,
                state.data
            );

            return data;
        }

        const { data } = await this.$axios.post(`/auth/register`, state.data);
        commit('SET_ID', data.id);

        return data;
    },

    async newCompany({ state, commit }) {
        if (state.data.id) {
            const response = await this.$axios.patch(
                `/auth/register`,
                state.data
            );

            return response.data;
        }

        const response = await this.$axios.post(
            `/auth/register-company`,
            state.data
        );
        commit('SET_ID', response.data.id);

        return response.data;
    },

    async submitPassportData({ state, commit }) {
        const response = await this.$axios.post(
            `/auth/passport-data`,
            state.passportData
        );

        return response;
    },

    async finishRegistration({}, form) {
        const { data } = await this.$axios.post(`/auth/register/finish`, form);

        return data;
    },

    async requestPin({ state }, phone) {
        const response = await this.$axios.post('/auth/request-pin', {
            phone,
            origin: 'user'
        });

        return response;
    },

    async checkPin({ state }) {
        const response = await this.$axios.post('/auth/check-pin', {
            phone: state.data.phone,
            pin: state.pin,
            origin: 'user'
        });

        return response;
    },

    async resetData({ commit }) {
        commit('RESET_DATA');
    },

    async setCompanyAcceptTerms({ state, commit }, userId) {
        if (state.data.id) {
            const { data } = await this.$axios.post(
                `/companies/${state.data.id}/accepted-payment-terms`,
                state.acceptedTermsByCompany
            );

            commit('SET_COMPANY_ACCEPTED_TERMS');

            return data;
        }

        const { data } = await this.$axios.post(
            `/companies/${userId}/accepted-payment-terms`,
            true
        );

        commit('SET_COMPANY_ACCEPTED_TERMS');

        return data;
    },

    setTemporaryUserData({ commit }, data) {
        commit('SET_USER_TEMPORARY_DATA', data);
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};

'use strict';

export const defaultState = () => {
    return {
        countries: []
    };
};

export const state = () => defaultState();

export const getters = {
    all: state => state.countries,
    getCitiesFromCountry: state => country => {
        const found = state.countries.find(item => item.id === country);

        return found ? found.cities : [];
    }
};

export const mutations = {
    SET_COUNTRIES(state, countries) {
        state.countries = countries;
    }
};

export const actions = {
    async index({ commit }) {
        const { data: countries } = await this.$axios.get(`/countries`);

        commit('SET_COUNTRIES', countries);

        return countries;
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};

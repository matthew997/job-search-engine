'use strict';

export const defaultState = () => {
    return {
        content: [],
        contentLoaded: false
    };
};

export const state = () => defaultState();

export const getters = {
    getContent: state => state.content,
    isContentLoaded: state => state.contentLoaded,
    getContentByKey: state => key => {
        const content = state.content.find(item => item.key === key);

        return (content && content.value) || null;
    }
};

export const mutations = {
    SET_CONTENT(state, content) {
        state.content = content;
    },
    ADD_SINGLE_CONTENT_SECTION(state, content) {
        state.content = [...state.content, content];
    },
    SET_CONTENT_LOAD_STATE(state, isContentLoaded) {
        state.contentLoaded = isContentLoaded;
    }
};

export const actions = {
    async index({ commit }) {
        const { data } = await this.$axios.get(`/content`);

        commit('SET_CONTENT', data);
        commit('SET_CONTENT_LOAD_STATE', true);
    },

    async getContent({ getters, commit }, key) {
        if (!getters.getContentByKey(key)) {
            const { data } = await this.$axios.get(`/content/${key}`);

            commit('ADD_SINGLE_CONTENT_SECTION', data);
        }
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};

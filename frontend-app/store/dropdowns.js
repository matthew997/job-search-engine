'use strict';

export const defaultState = () => {
    return {
        dropdowns: []
    };
};

export const state = () => defaultState();

export const getters = {
    getByName: state => (name, optionsOnly = true) => {
        const dropdown = state.dropdowns.find(item => item.name === name);

        if (optionsOnly) {
            return dropdown && dropdown.options;
        }

        return dropdown;
    }
};

export const mutations = {
    SET_DROPDOWNS(state, dropdowns) {
        state.dropdowns = dropdowns;
    },

    SET_DROPDOWN(state, dropdown) {
        state.dropdowns = [...state.dropdowns, dropdown];
    }
};

export const actions = {
    async index({ commit }) {
        const { data: dropdowns } = await this.$axios.get(`/dropdowns`);

        commit('SET_DROPDOWNS', dropdowns);
    },

    async getByName({ getters, commit }, name) {
        if (!getters.getByName(name)) {
            const { data: dropdown } = await this.$axios.get(
                `/dropdowns/${name}`
            );

            commit('SET_DROPDOWN', dropdown);
        }
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};

'use strict';

export const defaultState = () => {
    return {
        creditCards: []
    };
};

export const state = () => defaultState();

export const getters = {
    getCreditCards: state => state.creditCards
};

export const mutations = {
    SET_CREDIT_CARDS(state, creditCards) {
        state.creditCards = creditCards;
    },
    SET_DEFAULT_CARD(state, id) {
        state.creditCards = state.creditCards.map(item => {
            item.default = item.id === id;

            return item;
        });
    },
    DELETE_CARD_BY_INDEX(state, index) {
        state.creditCards.splice(index, 1);
    }
};

export const actions = {
    async fetchSubscriptionPaymentUrl() {
        const { data: url } = await this.$axios.get(
            `/tranzila/subscription-payment-url`
        );

        return url;
    },

    async fetchAddCardPaymentUrl() {
        const { data: url } = await this.$axios.get(
            `/tranzila/add-card-payment-url`
        );

        return url;
    },

    async fetchCreditCards({ commit }) {
        const { data } = await this.$axios.get(`/credit-cards`);

        commit('SET_CREDIT_CARDS', data.creditCards);
    },

    async setDefaultCreditCard({ commit }, id) {
        await this.$axios.post(`/credit-cards/default`, {
            id
        });

        commit('SET_DEFAULT_CARD', id);
    },

    async deleteCreditCard({ commit }, { id, index }) {
        await this.$axios.delete(`/credit-cards/${id}`);

        commit('DELETE_CARD_BY_INDEX', index);
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};

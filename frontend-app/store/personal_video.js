'use strict';

export const defaultState = () => {
    return {
        answers: [],
        isFirst: true,
        isLast: false,
    };
};


export const state = () => defaultState();

export const getters = {
    getAnswersLength: state => state.answers.length || 0,

    getNextAnswer: state => key => {
        if (state.answers === null) {
            return null;
        }

        const index = state.answers.findIndex(x => x.id === key);

        if (~index) {
            if (state.answers[index + 1]) {
                return state.answers[index + 1];
            }

            return state.answers[0];
        }

        return null;
    },

    getIsLast: state => {
        return state.isLast;
    },

    getPreviousAnswer: state => key => {
        if (state.answers === null) {
            return null;
        }

        const index = state.answers.findIndex(x => x.id === key)

        if (~index) {
            if (state.answers[index - 1]) {
                return state.answers[index - 1];
            }

            return state.answers[state.answers.length - 1];
        }

        return null;
    },

    getFirstAnswer: state => state.answers[0]
};

export const mutations = {
    SET_IS_FIRST(state, value) {
        state.isFirst = value
    },

    SET_IS_LAST(state, value) {
        state.isFirst = value
    },


    SET_ANSWERS(state, { answers = [] }) {
        state.answers = [...answers];
    },

    SET_SINGLE_ANSWER(state, { data, index }) {
        state.answers[index] = data
    }
};

export const actions = {
    async setAnswers({ commit }) {
        const { data } = await this.$axios.get('/cv-video');

        commit('SET_ANSWERS', data);
    },

    setIsFirst({ commit }, value) {
        commit('SET_IS_FIRST', value)
    },
    setIsLast({ commit }, value) {
        commit('SET_IS_LAST', value)
    },

    async updateAnswerVideo({ commit }, { id, newVideo }) {
        const { data } = await this.$axios.put(
            `/cv-video-answers/${id}`,
            newVideo
        );

        commit('SET_SINGLE_ANSWER', { data, index: newVideo.index - 1 })

        return data;
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};

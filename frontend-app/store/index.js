export const actions = {
    nuxtServerInit({ dispatch }) {
        return Promise.all([
            dispatch('content/index'),
            dispatch('settings/getSettings'),
            dispatch('dropdowns/index'),
            dispatch('jobtypes/index'),
            dispatch('countries/index')
        ]);
    },
    setApplicationCredits({ commit }, data) {
        commit('SET_LOGGED_USER_APPLICATION_CREDITS', data);
    }
};

export const mutations = {
    SET_LOGGED_USER_APPLICATION_CREDITS(state, data) {
        state.auth.user.application_credits = data;
    }
};

export const getters = {
    loggedUser: state => state.auth.user,
    isCompanyUser: (state, getters) =>
        getters.loggedUser && getters.loggedUser.type === 'company',
    isRegularUser: (state, getters) =>
        getters.loggedUser && getters.loggedUser.type === 'user'
};

'use strict';

export const defaultState = () => {
    return {};
};

export const state = () => defaultState();

export const getters = {};

export const mutations = {};

export const actions = {
    async getQuestions({ getters, commit }) {
        const { data: questions } = await this.$axios.get(`/cv-questions`);

        questions.sort((a, b) => {
            return a['index'] - b['index'];
        });

        return questions;
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};

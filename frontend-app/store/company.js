'use strict';

export const defaultState = () => {
    return {
        companies: null,
        companyData: {
            name: '',
            city: '',
            country: '',
            company_logo: '',
            promote_video: '',
            facebook_link: '',
            instagram_link: '',
            type_id: '',
            promote_image: '',
            about: '',
            working_environment: '',
            office_nature: '',
            created_at: '',
            updated_at: ''
        }
    };
};

export const state = () => defaultState();

export const getters = {
    getNextCompany: state => key => {
        if (state.companies === null) {
            return null;
        }

        const index = state.companies.findIndex(x => x.id === key);

        if (~index) {
            if (state.companies[index + 1]) {
                return state.companies[index + 1];
            }

            return state.companies[0];
        }

        return null;
    },
    getPreviousCompany: state => key => {
        if (state.companies === null) {
            return null;
        }

        const index = state.companies.findIndex(x => x.id === key);

        if (~index) {
            if (state.companies[index - 1]) {
                return state.companies[index - 1];
            }

            return state.companies[state.companies.length - 1];
        }

        return null;
    },
    getData: state => state.companies,
    getCompanyData: state => state.companyData
};

export const mutations = {
    SET_COMPANIES(state, data) {
        state.companies = data;
    }
};

export const actions = {
    async submitNewCompany({ state }, formData) {
        try {
            const { data } = await this.$axios.post('/companies', formData);

            return data;
        } catch (error) {
            console.error(error);
        }
    },

    async updateFilter({ state }, { id, data: filter }) {
        try {
            const { data } = await this.$axios.post(
                `/companies/${id}/filter`,
                filter
            );

            return data;
        } catch (error) {
            console.error(error);
        }
    },

    async getFilteredCompanies(
        { commit },
        { country, city, jobType, isOnHomepage, jobsPage }
    ) {
        const params = {
            country,
            city,
            jobType,
            isOnHomepage,
            jobsPage
        };

        const { data } = await this.$axios.get(`/companies`, { params });
        commit('SET_COMPANIES', data);

        return data;
    },

    async getFilters() {
        const { data } = await this.$axios.get(`/companies/filters`);

        return data;
    },

    async getCompanyBySlug({}, slug) {
        try {
            const { data } = await this.$axios.get(`/companies/${slug}`);

            return data;
        } catch (error) {
            console.error(error);
        }
    },

    async getCompanyBySlugLocked({}, slug) {
        try {
            const { data } = await this.$axios.get(`/companies/${slug}/locked`);

            return data;
        } catch (error) {
            console.error(error);
        }
    },

    async getApplications({}, { id, params = {} }) {
        const { data } = await this.$axios.get(
            `/companies/${id}/applications`,
            {
                params
            }
        );

        return data;
    },

    async update({}, { id, company }) {
        const { data } = await this.$axios.patch(`/companies/${id}`, company);

        return data;
    },

    async delete({}, id) {
        const { data } = await this.$axios.delete(`/companies/${id}`);

        return data;
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};

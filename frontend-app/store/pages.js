'use strict';

export const defaultState = () => {
    return {};
};

export const state = () => defaultState();

export const getters = {};

export const mutations = {};

export const actions = {
    async getBySlug({ }, slug) {
        const { data } = await this.$axios.get(`/pages/${slug}`);

        return data;
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};

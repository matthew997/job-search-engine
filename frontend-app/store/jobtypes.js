'use strict';

export const defaultState = () => {
    return {
        types: []
    };
};

export const state = () => defaultState();

export const getters = {
    all: state => state.types
};

export const mutations = {
    SET_TYPES(state, types) {
        state.types = types;
    }
};

export const actions = {
    async index({ commit }) {
        const { data: types } = await this.$axios.get(`/job-types`);

        commit('SET_TYPES', types);
    },

    async homepage({ }) {
        const params = {
            homepage: true
        };

        const { data } = await this.$axios.get(`/job-types`, { params });

        return data;
    },
};

export default {
    state,
    getters,
    mutations,
    actions
};

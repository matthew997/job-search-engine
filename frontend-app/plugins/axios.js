import cookie from 'cookie';

export default function({ $axios, req, res, redirect }) {
    $axios.onError(error => {
        if (process.server) {
            if (error.response && error.response.status === 401) {
                return redirect('/login');
            }
        }
    });

    $axios.onRequest(config => {
        let CLUID;

        if (process.server && req.headers.cookie) {
            const cookies = cookie.parse(req.headers.cookie);

            CLUID = cookies['auth._token.local'];

            $axios.defaults.headers.Cookie = req.headers.cookie;
        } else if (process.client) {
            CLUID = localStorage['auth._token.local'] || '';
        }

        if (CLUID) {
            config.headers.CLUID = CLUID;
        }
    });
}

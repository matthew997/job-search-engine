/* eslint-disable */
import Vue from 'vue';
import config from '../../config';

const FullBackgroundImagePath = {
    async bind(el, binding, vnode) {
        if (!binding.value) {
            return null;
        }

        if (binding.arg === 'assets') {
            el.style.backgroundImage = `url(${require(`@/assets/images/${binding.value}`)})`;

            return;
        }

        if (!binding.value.id) {
            const url = `${config.mediaBaseUrl}/media/images/no-image.png`;
            el.style.backgroundImage = `url(${url})`;

            return;
        }

        const isSafari = /^((?!chrome|android).)*safari/i.test(
            navigator.userAgent
        );
        const isWebSupported = !isSafari;
        const url = `${config.mediaBaseUrl}/media/${binding.value.type}s/${
            binding.value.date
        }/${binding.value.filename}.${
            isWebSupported ? 'webp' : binding.value.extension
        }`;

        el.style.backgroundImage = `url(${url})`;
    }
};

Vue.directive('full-background-image-path', FullBackgroundImagePath);

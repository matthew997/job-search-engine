/* eslint-disable */
import Vue from 'vue';
import config from '../../config';

const FullImagePath = {
    bind(el, binding, vnode) {
        if (!binding.value) {
            return null;
        }

        if (binding.arg === 'assets') {
            let fileName = binding.value;

            el.src = require(`@/assets/images/${fileName}`);

            fileName = fileName.split('.').slice(0, -1)[0];

            el.alt = fileName;

            return;
        }

        function handleIntersectForApi(entries, observer) {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    createSourceSet(binding.value, el);
                    observer.unobserve(el);
                }
            });
        }

        function createObserver(handler) {
            const options = {
                root: null,
                rootMargin: '100px',
                threshold: '0'
            };
            const observer = new IntersectionObserver(handler, options);
            observer.observe(el);
        }

        if (window.IntersectionObserver && binding.arg !== 'no-observe') {
            createObserver(handleIntersectForApi);
        } else {
            createSourceSet(binding.value, el);
        }
    }
};

Vue.directive('full-image-path', FullImagePath);

function createSourceSet(image, element) {
    if (!image || !image.id || !image.meta) {
        const newImage = document.createElement('img');

        newImage.src = `${config.mediaBaseUrl}/media/images/no-image.png`;
        newImage.alt = 'no image';

        element.appendChild(newImage);

        return null;
    }

    const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    const isWebSupported = !isSafari;

    const breakPointsCopy = [...image.meta.breakpoints];
    breakPointsCopy.sort((a, b) => a - b);

    let sourceWebp = document.createElement('source');
    sourceWebp.type = 'image/webp';

    let sourceJpg = document.createElement('source');
    sourceJpg.type = `image/${image.extension}`;

    breakPointsCopy.forEach(breakpoint => {
        sourceWebp.srcset += `${config.mediaBaseUrl}/media/images/${image.date}/${image.filename}_${breakpoint}.webp ${breakpoint}w,`;
        sourceWebp.sizes += `(max-width: ${breakpoint}px) ${breakpoint}px,`;
        sourceJpg.srcset += `${config.mediaBaseUrl}/media/images/${image.date}/${image.filename}_${breakpoint}.${image.extension} ${breakpoint}w,`;
        sourceJpg.sizes += `(max-width: ${breakpoint}px) ${breakpoint}px,`;
    });

    if (image.size && image.extension !== 'gif') {
        sourceWebp.srcset += `${config.mediaBaseUrl}/media/images/${image.date}/${image.filename}.webp ${image.meta.width}w,`;
        sourceWebp.sizes += `${image.meta.width}px,`;
        sourceJpg.srcset += `${config.mediaBaseUrl}/media/images/${image.date}/${image.filename}.${image.extension} ${image.meta.width}w,`;
        sourceJpg.sizes += `${image.meta.width}px,`;
    }

    element.appendChild(sourceWebp);
    element.appendChild(sourceJpg);

    const newImage = document.createElement('img');

    if (image.extension === 'gif') {
        newImage.src = `${config.mediaBaseUrl}/media/images/${image.date}/${image.filename}.gif`;
    } else {
        newImage.src = `${config.mediaBaseUrl}/media/images/${image.date}/${
            image.filename
        }.${isWebSupported ? 'webp' : image.extension}`;
    }

    newImage.alt = image.alt;

    element.appendChild(newImage);
}

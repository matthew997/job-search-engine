import AnalyticsService from '@/services/analytics.service';

export default ({ app }, inject) => {
    inject('analytics', new AnalyticsService(app));
};
